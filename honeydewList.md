**ideas:**
* "build space", the concept that will determine what can and cannot go into a compound
  * ie, buildings and stalactites and caves add build space, rooms and defenses deplete build space

**features:**
* need to have relations among nobles
* need to have a isDead flag on nobles for when they're dead
* magical items, probably need a creation process during house creation
* city creation
* actual gameplay, lol
* houses owning businesses as a way to bring in cash
* city has houseless rogues, non-noble families, non-drow inhabitants, etc.
* nobles being born
* difference indications on changes each week for each house and the city in the simulator
* track intel as static data in a separate table
* food stores per house, probably need businesses first
* mercenary house?


**enhancements:**
* ages during house creation are probably a bit too random
* pretty ui, kekw
* split out house create code from HouseService and HouseController
* once finalize house happens, go to HouseView
* communicate and check for required fields on house creation
* random city should not have duplicate house names
* lock down controller methods
* remove extra RestRepository stuffs


**notes**
* Intel: to gain intel on a stat or house feature:
  * DC = prestige of target - deception (of target) + investigation (of you)
* Need fight mechanics
  * tactic - stealth, aggressive, balanced, defensive, ranged, magic, disengage, etc
  * damage - hp? multiple attacks per round?
  * individual vs group fights? different mechanics?
* Marketplace:
  * outside availability - outside vendors have stock they bring in
  * offer to buy - like ebay, place your offer w/a max price, end of cycle the product goes to the highest bidders with prestige (or something) for tie breaker
  * offer to sell - how much and min price
  * attempt to steal - could be an Operation
  * different markets
    * open market for publicly tradeable goods
    * black market for other goods (ie selling rothe w/o contract)
    * black market offers are not visibile by default
* Influence
  * measured per POI
  * effected by troop presence, money invested, other factors, shrinks over time naturally
  * different POIs might have different ways to gain influence
  * influence is zero-sum, when one house gains, another loses
  * influence would effect success of operations and other actions in a POI
* Random POIs
  * don't have to be preconfigured or can be duplicates
  * (goblin camp, orc camp, gnome mine, monster lairs)
  * other ideas: Svnirfneblin city, Beholder den, Bulette den, Derro hive, Duerger city, Fire Giant lair, Rothe pasture, Mycanoid grove, Illithid colony, Umber Hulk lair, Gem mine, Troll cave, Shadow dragon lair, Purple worm lair, Minotaur labyrinth, Hook Horror den