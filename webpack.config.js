module.exports = {
    devtool: 'source-map',
    entry: __dirname + '/src/main/resources/static/main.js',
    cache: false,
    output: {
        path: __dirname + '/build/resources/main/static',
        publicPath: '/',
        filename: 'drow-city.js'
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
                presets: ['@babel/preset-env', '@babel/preset-react']
            }
        },
        {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
        }]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};