package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.entity.Individual
import com.divisiblebyzero.drowcity.entity.Message
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Subject

import javax.transaction.Transactional

@Transactional
@SpringBootTest(classes = DrowCityApplication)
class MessageRepositorySpec extends Specification {
    @Subject
    @Autowired
    MessageRepository sut

    @Autowired
    IndividualRepository individualRepository

    Individual one
    Individual two

    def setup() {
        one = new Individual(name: 'One')
        two = new Individual(name: 'Two')
        individualRepository.saveAll([one, two])
        Message read = new Message(from: one, to: two, readByTo: false, message: 'unread')
        Message unread = new Message(from: one, to: two, readByTo: true, message: 'read')
        sut.saveAll([read, unread])
    }

    def 'findByTo'() {
        when:
        List<Message> result = sut.findByTo(two)

        then:
        result.size() == 2
    }

    def 'findByToAndReadByToTrue'() {
        when:
        List<Message> result = sut.findByToAndReadByToTrue(two)

        then:
        result.size() == 1
    }
}
