package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.entity.Player
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import javax.transaction.Transactional

import static com.divisiblebyzero.drowcity.enums.OrganizationType.business
import static com.divisiblebyzero.drowcity.enums.OrganizationType.house

@Transactional
@SpringBootTest(classes = DrowCityApplication)
class OrganizationRepositorySpec extends Specification {
    @Autowired
    OrganizationRepository sut

    @Autowired
    PlayerRepository playerRepository

    Player player1
    Player player2

    def setup() {
        player1 = new Player(firstName: 'Player', lastName: 'One')
        player2 = new Player(firstName: 'Player', lastName: 'Two')
        playerRepository.saveAll([player1, player2])
    }

    def 'findByPlayerAndType'() {
        setup:
        List<Organization> given = [
                new Organization(player: player1, type: house),
                new Organization(player: player1, type: business),
                new Organization(player: player2, type: house),
                new Organization(player: player2, type: house),
                new Organization(player: player2, type: business),
                new Organization(player: player2, type: business),
        ]
        sut.saveAll(given)

        when:
        List<Organization> player1Houses = sut.findByPlayerAndType(player1, house)
        List<Organization> player1Businesses = sut.findByPlayerAndType(player1, business)
        List<Organization> player2Houses = sut.findByPlayerAndType(player2, house)
        List<Organization> player2Businesses = sut.findByPlayerAndType(player2, business)

        then:
        player1Houses.size() == 1
        player1Businesses.size() == 1
        player2Houses.size() == 2
        player2Businesses.size() == 2
    }
}
