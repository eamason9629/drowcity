package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.entity.PotentialOperation
import com.divisiblebyzero.drowcity.repository.PotentialOperationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = DrowCityApplication)
class PotentialOperationInitializerSpec extends Specification {
    @Autowired
    PotentialOperationInitializer sut

    @Autowired
    PotentialOperationRepository potentialOperationRepository

    def 'creates appropriate test data'() {
        given:
        List<PotentialOperation> results = potentialOperationRepository.findAll().toList()

        expect:
        sut
        results.size() == PotentialOperationInitializer.OPERATION_DATA.size()
        results.each {PotentialOperation it ->
            assert it.name != null
            assert it.description != null
            assert it.requiredArchetypes != null
            assert it.requiredSkill != null
            assert it.requiredSkillLevel >= 0
            assert it.difficulty >= 0
            assert it.workerGroupsAllowed != null
            assert it.archetypesAllowed != null
            assert it.timetable > 0
        }

    }
}
