package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.entity.City
import com.divisiblebyzero.drowcity.repository.CityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import static com.divisiblebyzero.drowcity.testData.CityDataInitializer.TEST_CITY_NAME

@SpringBootTest(classes = DrowCityApplication)
class CityDataInitializerSpec extends Specification {
    @Autowired
    CityRepository cityRepository

    def 'initializer ran and set data up appropriately'() {
        when:
        List<City> results = cityRepository.findAll().toList()
        City city = results.first()

        then:
        results.size() == 1
        city.week > 0
        city.name == TEST_CITY_NAME
        city.gameMaster != null
    }
}
