package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.entity.Message
import com.divisiblebyzero.drowcity.repository.MessageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = DrowCityApplication)
class MessageDataInitializerSpec extends Specification {
    @Autowired
    MessageDataInitializer sut

    @Autowired
    MessageRepository messageRepository

    def 'initializer ran and set data up appropriately'() {
        when:
        List<Message> results = messageRepository.findAll().toList()

        then:
        sut != null
        results.size() == 100
        results.each { Message it ->
            assert it.from != null
            assert it.to != null
            assert it.subject != null
            assert it.message != null
            assert !it.sentAnonymously
            assert !it.sentPublicly
            assert !it.readByGM
            assert !it.readByTo
        }
    }
}
