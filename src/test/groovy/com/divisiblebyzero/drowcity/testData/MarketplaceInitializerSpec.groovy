package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.entity.Marketplace
import com.divisiblebyzero.drowcity.repository.MarketplaceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import javax.transaction.Transactional

@SpringBootTest(classes = DrowCityApplication)
class MarketplaceInitializerSpec extends Specification {
    @Autowired
    MarketplaceInitializer sut

    @Autowired
    MarketplaceRepository marketplaceRepository

    @Transactional
    def 'creates the data we expect'() {
        given:
        List<Marketplace> result = marketplaceRepository.findAll().toList()

        expect:
        sut
        result.size() == MarketplaceInitializer.MARKETS_DATA.size()
        result.each { Marketplace it ->
            assert it.name != null
            assert it.description != null
            assert it.location != null
            assert it.allowedAssetTypes != null
            assert it.allowedAssetTypes.size() > 0
        }
    }
}
