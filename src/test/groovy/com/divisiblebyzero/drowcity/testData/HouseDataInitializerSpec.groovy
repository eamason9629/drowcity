package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.entity.*
import com.divisiblebyzero.drowcity.enums.AssetType
import com.divisiblebyzero.drowcity.enums.ConsumptionModel
import com.divisiblebyzero.drowcity.enums.Loyalty
import com.divisiblebyzero.drowcity.enums.OrganizationType
import com.divisiblebyzero.drowcity.repository.CityRepository
import com.divisiblebyzero.drowcity.repository.OrganizationRepository
import com.divisiblebyzero.drowcity.repository.PlayerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import javax.transaction.Transactional

@SpringBootTest(classes = DrowCityApplication)
class HouseDataInitializerSpec extends Specification {
    @Autowired
    CityRepository cityRepository

    @Autowired
    PlayerRepository playerRepository

    @Autowired
    OrganizationRepository organizationRepository

    @Transactional
    def 'initializer ran and set data up appropriately'() {
        when:
        City city = cityRepository.findByName(CityDataInitializer.TEST_CITY_NAME)
        Player player = playerRepository.findByUsername(PlayerDataInitializer.ADMIN_USERNAME)
        List<Organization> results = organizationRepository.findByType(OrganizationType.house)

        then:
        results.size() == 12
        results.each { Organization it ->
            assert it.player == player
            assert it.type == OrganizationType.house
            assert it.city == city
            assert it.name != null
            assert it.assets.size() == AssetType.values().size()
            it.assets.each { Asset asset ->
                assert asset.type != null
                assert asset.lines.size() == 1
            }
            assert it.workerGroups.size() == 5
            it.workerGroups.each { WorkerGroup wg ->
                assert wg.workerType != null
                assert wg.consumptionModel == ConsumptionModel.normal
                assert wg.employmentType != null
                assert wg.lineItems.size() == 1
            }
            assert it.members.size() == 5
            it.members.each { OrganizationalMembershipRelationship omr ->
                assert omr.from != null
                assert omr.to != null
                assert omr.status != null
                assert omr.loyalty == Loyalty.loyal
            }
        }
    }
}
