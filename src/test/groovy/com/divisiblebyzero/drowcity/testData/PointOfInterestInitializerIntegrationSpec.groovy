package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.entity.PointOfInterest
import com.divisiblebyzero.drowcity.repository.PointOfInterestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import static com.divisiblebyzero.drowcity.testData.PointOfInterestInitializer.POI_DATA

@SpringBootTest(classes = DrowCityApplication)
class PointOfInterestInitializerIntegrationSpec extends Specification {
    @Autowired
    PointOfInterestInitializer sut

    @Autowired
    PointOfInterestRepository pointOfInterestRepository

    def 'initializer ran and set data up appropriately'() {
        when:
        List<PointOfInterest> results = pointOfInterestRepository.findAll().toList()

        then:
        sut != null
        results.size() == POI_DATA.size()
        results.each { PointOfInterest poi ->
            assert poi.name != null
            assert poi.discoverability > 0
            assert poi.description != null
            assert poi.region != null
            assert poi.llothsFavor != 0
            assert poi.prestige != 0
            assert poi.arcana != 0
            assert poi.defense != 0
            assert poi.deception != 0
            assert poi.insight != 0
            assert poi.investigation != 0
            assert poi.religion != 0
            assert poi.stealth != 0
            assert poi.survival != 0
        }
    }
}
