package com.divisiblebyzero.drowcity.testData


import spock.lang.Specification

import static com.divisiblebyzero.drowcity.testData.PointOfInterestInitializer.POI_DATA

class PointOfInterestInitializerSpec extends Specification {
    def 'test data is formatted correctly'() {
        expect:
        POI_DATA.each {
            assert it.loot instanceof List : "$it.name's loot isn't a List"
        }
    }
}
