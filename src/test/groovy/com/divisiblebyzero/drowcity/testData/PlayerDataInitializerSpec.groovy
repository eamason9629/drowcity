package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.repository.PlayerRepository
import com.divisiblebyzero.drowcity.repository.UserRoleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import static com.divisiblebyzero.drowcity.testData.PlayerDataInitializer.TEST_PLAYERS
import static com.divisiblebyzero.drowcity.testData.PlayerDataInitializer.USER_ROLES

@SpringBootTest(classes = DrowCityApplication)
class PlayerDataInitializerSpec extends Specification {
    @Autowired
    PlayerRepository playerRepository

    @Autowired
    UserRoleRepository userRoleRepository

    def 'data initializer ran and created data'() {
        expect:
        playerRepository.findAll().size() == TEST_PLAYERS.size()
        userRoleRepository.findAll().size() == USER_ROLES.size() + TEST_PLAYERS.size()
        playerRepository.findByUsername(PlayerDataInitializer.ADMIN_USERNAME)
    }
}
