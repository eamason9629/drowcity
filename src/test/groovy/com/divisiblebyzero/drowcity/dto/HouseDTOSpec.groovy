package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.*
import com.divisiblebyzero.drowcity.enums.*
import com.divisiblebyzero.drowcity.util.NameGenerator
import spock.lang.Specification

class HouseDTOSpec extends Specification {
    def 'create'() {
        setup:
        Organization given = new Organization(
                id: UUID.randomUUID(),
                name: NameGenerator.houseName(),
                type: OrganizationType.house,
                city: new City(name: NameGenerator.cityName(), week: NameGenerator.random(100)),
                workerGroups: [
                        new WorkerGroup(id: UUID.randomUUID(), lineItems: [new WorkerGroupLineItem(week: 0, change: NameGenerator.random(10), type: WorkerGroupLineItemType.hire)], workerType: WorkerType.soldier, employmentType: EmploymentType.paid, consumptionModel: ConsumptionModel.normal),
                        new WorkerGroup(id: UUID.randomUUID(), lineItems: [new WorkerGroupLineItem(week: 0, change: NameGenerator.random(10), type: WorkerGroupLineItemType.hire)], workerType: WorkerType.staff, employmentType: EmploymentType.paid, consumptionModel: ConsumptionModel.normal)
                ],
                members: [
                        new OrganizationalMembershipRelationship(to: new Individual(name: NameGenerator.femaleName(), archetype: Archetype.matron, gender: Gender.female), status: OrganizationalRelationshipStatus.matron),
                        new OrganizationalMembershipRelationship(to: new Individual(name: NameGenerator.maleName(), archetype: Archetype.headWizard, gender: Gender.male), status: OrganizationalRelationshipStatus.noble),
                ],
                assets: [
                        new Asset(id: UUID.randomUUID(), type: AssetType.gold, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                        new Asset(id: UUID.randomUUID(), type: AssetType.rothe, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                        new Asset(id: UUID.randomUUID(), type: AssetType.mushrooms, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                        new Asset(id: UUID.randomUUID(), type: AssetType.fish, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                        new Asset(id: UUID.randomUUID(), type: AssetType.moss, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                        new Asset(id: UUID.randomUUID(), type: AssetType.water, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                        new Asset(id: UUID.randomUUID(), type: AssetType.weapons, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                        new Asset(id: UUID.randomUUID(), type: AssetType.armor, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                        new Asset(id: UUID.randomUUID(), type: AssetType.lizards, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                        new Asset(id: UUID.randomUUID(), type: AssetType.potions, lines: [new AssetLineItem(week: 0, change: NameGenerator.random(10), type: AssetLineItemType.found)]),
                ]
        )

        when:
        HouseDTO result = HouseDTO.create(given)

        then:
        result.id == given.id.toString()
        result.name == given.name
        result.cityName == given.city.name
        result.week == given.city.week
        result.matron != null
        result.matron.id == given.members.first().to.id.toString()
        result.matron.name == given.members.first().to.name
        result.members.size() == 1
        result.members.first().id == given.members.last().to.id.toString()
        result.members.first().name == given.members.last().to.name
        result.workerGroups.each {
            assert it.id != null
            assert it.workerType != null
            assert it.employmentType != null
            assert it.consumptionModel != null
            assert it.currentCount > 0
            assert it.lastChangeAmount > 0
        }
        result.assets.size() == given.assets.size()
        result.assets.each {
            assert it.id != null
            assert it.type != null
            assert it.currentCount > 0
            assert it.lastChange > 0
        }
    }
}
