package com.divisiblebyzero.drowcity.util

import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class NameGeneratorSpec extends Specification {
    def 'lists contain unique names'() {
        setup:
        Map cities = NameGenerator.CITY_NAMES.groupBy { it }.findAll { it.value.size() > 1 }
        Map houses = NameGenerator.HOUSE_NAMES.groupBy { it }.findAll { it.value.size() > 1 }
        Map males = NameGenerator.MALE_NAMES.groupBy { it }.findAll { it.value.size() > 1 }
        Map females = NameGenerator.FEMALE_NAMES.groupBy { it }.findAll { it.value.size() > 1 }

        expect:
        cities == [:]
        houses == [:]
        males == [:]
        females == [:]
    }

    def 'cityName - index #given'() {
        setup:
        GroovySpy(global: true, NameGenerator)
        String expected = NameGenerator.CITY_NAMES[given - 1]

        when:
        String result = NameGenerator.cityName()

        then:
        result == expected
        1 * NameGenerator.random(NameGenerator.CITY_NAMES.size()) >> given

        where:
        given << (1..NameGenerator.CITY_NAMES.size())
    }

    def 'houseName - index #given'() {
        setup:
        GroovySpy(global: true, NameGenerator)
        String expected = NameGenerator.HOUSE_NAMES[given - 1]

        when:
        String result = NameGenerator.houseName()

        then:
        result == expected
        1 * NameGenerator.random(NameGenerator.HOUSE_NAMES.size()) >> given

        where:
        given << (1..NameGenerator.HOUSE_NAMES.size())
    }

    def 'maleName - index #given'() {
        setup:
        GroovySpy(global: true, NameGenerator)
        String expected = NameGenerator.MALE_NAMES[given - 1]

        when:
        String result = NameGenerator.maleName()

        then:
        result == expected
        1 * NameGenerator.random(NameGenerator.MALE_NAMES.size()) >> given

        where:
        given << (1..NameGenerator.MALE_NAMES.size())
    }

    def 'femaleName - index #given'() {
        setup:
        GroovySpy(global: true, NameGenerator)
        String expected = NameGenerator.FEMALE_NAMES[given - 1]

        when:
        String result = NameGenerator.femaleName()

        then:
        result == expected
        1 * NameGenerator.random(NameGenerator.FEMALE_NAMES.size()) >> given

        where:
        given << (1..NameGenerator.FEMALE_NAMES.size())
    }
}
