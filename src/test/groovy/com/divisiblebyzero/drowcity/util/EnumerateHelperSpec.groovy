package com.divisiblebyzero.drowcity.util

import com.divisiblebyzero.drowcity.dto.EnumDTO
import com.divisiblebyzero.drowcity.enums.EmploymentType
import com.divisiblebyzero.drowcity.enums.Gender
import spock.lang.Specification
import spock.lang.Unroll

class EnumerateHelperSpec extends Specification {
    @Unroll
    def 'createFrom - #given.simpleName -> #expected'() {
        when:
        EnumDTO result = EnumerateHelper.createFrom(given)

        then:
        result == expected

        where:
        given | expected
        Gender | new EnumDTO(
                    name: Gender.class.simpleName, values: [
                        male: [name: Gender.male.name(), prettyName: Gender.male.prettyName, description: Gender.male.description, creationCost: Gender.male.creationCost] as Map,
                        female: [name: Gender.female.name(), prettyName: Gender.female.prettyName, description: Gender.female.description, creationCost: Gender.female.creationCost] as Map
                    ]
                )
        EmploymentType | new EnumDTO(
                            name: EmploymentType.class.simpleName, values: [
                                forced: [name: EmploymentType.forced.name(), prettyName: EmploymentType.forced.prettyName, description: EmploymentType.forced.description],
                                paid: [name: EmploymentType.paid.name(), prettyName: EmploymentType.paid.prettyName, description: EmploymentType.paid.description],
                                volunteer: [name: EmploymentType.volunteer.name(), prettyName: EmploymentType.volunteer.prettyName, description: EmploymentType.volunteer.description]
                            ]
                        )
    }
}
