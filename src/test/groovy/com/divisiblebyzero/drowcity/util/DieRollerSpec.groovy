package com.divisiblebyzero.drowcity.util

import com.divisiblebyzero.drowcity.enums.DieType
import spock.lang.Specification
import spock.lang.Unroll

class DieRollerSpec extends Specification {
    @Unroll
    def '#numDice#dieType + #modifier = [#min, #max]'() {
        when:
        List<Integer> result = (1..10).collect { DieRoller.rollDice(numDice, dieType, modifier) }

        then:
        result.each { Integer it ->
            assert it >= min
            assert it <= max
        }

        where:
        numDice | dieType      | modifier | min | max
        0       | DieType.d1   | 0        | 0   | 0
        0       | DieType.d1   | 5        | 5   | 5
        0       | DieType.d6   | 0        | 0   | 0
        0       | DieType.d6   | 5        | 5   | 5
        0       | DieType.d20  | 0        | 0   | 0
        0       | DieType.d20  | 5        | 5   | 5
        1       | DieType.d1   | 0        | 1   | 1
        1       | DieType.d1   | 1        | 2   | 2
        1       | DieType.d1   | -1       | 0   | 0
        2       | DieType.d1   | 0        | 2   | 2
        1       | DieType.d2   | 0        | 1   | 2
        1       | DieType.d3   | 0        | 1   | 3
        1       | DieType.d4   | 0        | 1   | 4
        1       | DieType.d6   | 0        | 1   | 6
        1       | DieType.d8   | 0        | 1   | 8
        1       | DieType.d10  | 0        | 1   | 10
        1       | DieType.d12  | 0        | 1   | 12
        1       | DieType.d18  | 0        | 1   | 18
        1       | DieType.d20  | 0        | 1   | 20
        20      | DieType.d20  | 0        | 20  | 400
        1       | DieType.d30  | 0        | 1   | 30
        1       | DieType.d50  | 0        | 1   | 50
        1       | DieType.d100 | 0        | 1   | 100
    }

    @Unroll
    def 'coinFlip - #given -> #expected'() {
        setup:
        GroovySpy(global: true, DieRoller)

        when:
        boolean result = DieRoller.coinFlip()

        then:
        result == expected
        1 * DieRoller.rollDice(1, DieType.d2) >> given

        where:
        given | expected
        1     | true
        2     | false
    }

    @Unroll
    def 'pickOne - #given'() {
        when:
        def result = DieRoller.pickOne(given)

        then:
        result
        given.contains(result)

        where:
        given << [
                [1], [1,2], [1, 2, 3]
        ]
    }
}
