package com.divisiblebyzero.drowcity.enums

import com.divisiblebyzero.drowcity.util.DieRoller
import spock.lang.Specification
import spock.lang.Unroll

class GenderSpec extends Specification {
    @Unroll
    def 'random - #roll -> #expected.name()'() {
        setup:
        GroovySpy(global: true, DieRoller)

        when:
        Gender result = Gender.random()

        then:
        result == expected
        1 * DieRoller.rollDice(1, DieType.d100) >> { roll }

        where:
        roll | expected
        5    | Gender.male
        10   | Gender.male
        15   | Gender.male
        20   | Gender.male
        25   | Gender.male
        30   | Gender.male
        35   | Gender.male
        40   | Gender.male
        45   | Gender.male
        50   | Gender.male
        55   | Gender.male
        60   | Gender.male
        65   | Gender.male
        70   | Gender.male
        74   | Gender.male
        75   | Gender.male
        76   | Gender.female
        80   | Gender.female
        85   | Gender.female
        89   | Gender.female
        90   | Gender.female
        91   | Gender.female
        95   | Gender.female
        100  | Gender.female
    }
}
