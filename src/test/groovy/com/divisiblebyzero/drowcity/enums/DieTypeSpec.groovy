package com.divisiblebyzero.drowcity.enums

import spock.lang.Specification
import spock.lang.Unroll

class DieTypeSpec extends Specification {
    @Unroll
    def 'maxValue - #given -> #expected'() {
        expect:
        given.maxValue() == expected

        where:
        given        | expected
        DieType.d1   | 1
        DieType.d2   | 2
        DieType.d3   | 3
        DieType.d4   | 4
        DieType.d6   | 6
        DieType.d8   | 8
        DieType.d10  | 10
        DieType.d12  | 12
        DieType.d18  | 18
        DieType.d20  | 20
        DieType.d30  | 30
        DieType.d50  | 50
        DieType.d100 | 100
    }
}
