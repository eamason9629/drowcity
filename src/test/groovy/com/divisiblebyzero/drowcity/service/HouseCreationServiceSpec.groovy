package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.dto.HouseCreationProcessDTO
import com.divisiblebyzero.drowcity.dto.NewHouseDTO
import com.divisiblebyzero.drowcity.enums.FamilyTrait
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class HouseCreationServiceSpec extends Specification {
    @Subject
    HouseCreationService sut = new HouseCreationService()

    @Unroll
    def 'calculateRemainingPoints -> #given => #expected'() {
        setup:
        HouseCreationProcessDTO process = new HouseCreationProcessDTO(newHouse: given, nobles: [])

        when:
        HouseCreationProcessDTO result = sut.calculateRemainingPoints(process)

        then:
        result.remainingPoints == expected

        where:
        expected | given
        10000    | new NewHouseDTO()
        9999     | new NewHouseDTO(gold: 1000)
        9998     | new NewHouseDTO(gold: 2000)
        11000    | new NewHouseDTO(familyTrait: FamilyTrait.heretical)
        9900     | new NewHouseDTO(familyTrait: FamilyTrait.infernalTies)
        9900     | new NewHouseDTO(rotheFarmBid: 100)
        9800     | new NewHouseDTO(adamantiumMineBid: 200)
        9700     | new NewHouseDTO(arenaBid: 300)
        9600     | new NewHouseDTO(fishmongeringBid: 400)
        9500     | new NewHouseDTO(mossFarmingBid: 500)
        9400     | new NewHouseDTO(mushroomFarmingBid: 600)
        9300     | new NewHouseDTO(waterSupplyBid: 700)
        9200     | new NewHouseDTO(garbageDumpBid: 800)
        9100     | new NewHouseDTO(mortuaryBid: 900)
        9000     | new NewHouseDTO(lizardFarmingBid: 1000)
        9900     | new NewHouseDTO(soldierCount: 100)
        9900     | new NewHouseDTO(priestessCount: 4)
        9800     | new NewHouseDTO(wizardCount: 4)
        9900     | new NewHouseDTO(staffCount: 500)
        9998     | new NewHouseDTO(orcCount: 2)
        9999     | new NewHouseDTO(goblinCount: 5)
        9998     | new NewHouseDTO(orcCount: 2)
        9998     | new NewHouseDTO(koboldCount: 20)
        9988     | new NewHouseDTO(duergarCount: 6)
        9985     | new NewHouseDTO(dwarfCount: 3)
        9980     | new NewHouseDTO(gnomeCount: 2)
        9950     | new NewHouseDTO(minotaurCount: 2)
        9750     | new NewHouseDTO(beholderCount: 1)
        9000     | new NewHouseDTO(illithidCount: 2)
        9996     | new NewHouseDTO(kuoToaCount: 2)
        9994     | new NewHouseDTO(bugbearCount: 2)
        9980     | new NewHouseDTO(lamiaCount: 2)
        9600     | new NewHouseDTO(nightmareCount: 2)
        9993     | new NewHouseDTO(zombieCount: 7)
        9999     | new NewHouseDTO(skeletonCount: 2)
        9300     | new NewHouseDTO(driderCount: 7)
    }
}
