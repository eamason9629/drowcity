package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.config.DrowCityApplication
import com.divisiblebyzero.drowcity.entity.Asset
import com.divisiblebyzero.drowcity.enums.AssetLineItemType
import com.divisiblebyzero.drowcity.enums.AssetType
import com.divisiblebyzero.drowcity.repository.AssetRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Unroll

import javax.transaction.Transactional

@SpringBootTest(classes = DrowCityApplication)
class AssetServiceSpec extends Specification {
    @Autowired
    AssetService sut

    @Autowired
    AssetRepository assetRepository

    @Transactional
    def 'generate and save new asset - defaults'() {
        when:
        Asset returned = sut.generateAndSaveNewAsset()
        Asset result = assetRepository.findById(returned.id).get()

        then:
        result
        result.id
        result.type == AssetType.gold
        result.lines.size() == 1
        result.lines.first().id
        result.lines.first().week == 0
        result.lines.first().change == 0
        result.lines.first().type == AssetLineItemType.found
    }

    @Unroll
    @Transactional
    def 'generate and save new asset - #week + #type + #lineItemType + #initialValue'() {
        when:
        Asset returned = sut.generateAndSaveNewAsset(week, type, lineItemType, initialValue)
        Asset result = assetRepository.findById(returned.id).get()

        then:
        result
        result.id
        result.type == type
        result.lines.size() == 1
        result.lines.first().id
        result.lines.first().week == week
        result.lines.first().change == initialValue
        result.lines.first().type == lineItemType

        where:
        week | type                | lineItemType                | initialValue
        0    | AssetType.gold      | AssetLineItemType.found     | 0
        1    | AssetType.potions   | AssetLineItemType.purchased | 5
        2    | AssetType.lizards   | AssetLineItemType.consumed  | 10
        3    | AssetType.weapons   | AssetLineItemType.crafted   | 15
        4    | AssetType.armor     | AssetLineItemType.lost      | 20
        5    | AssetType.rothe     | AssetLineItemType.liberated | 25
        6    | AssetType.mushrooms | AssetLineItemType.stolen    | 30
    }
}
