package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.entity.Loot
import com.divisiblebyzero.drowcity.entity.PointOfInterest
import com.divisiblebyzero.drowcity.enums.AssetType
import com.divisiblebyzero.drowcity.enums.Region
import com.divisiblebyzero.drowcity.service.LootService
import com.divisiblebyzero.drowcity.service.PointOfInterestService
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.transaction.Transactional

@Component
class PointOfInterestInitializer implements InitializingBean {
    @Autowired
    CityDataInitializer cityDataInitializer

    @Autowired
    PointOfInterestService pointOfInterestService

    @Autowired
    LootService lootService

    final static List<Map> POI_DATA = [
            [name: 'Garandal Plateau', discoverability: 100, description: "The most ostentatious part of Dur'Garandal, Garandal Plateau is home to four of the top five houses and the absolute richest commoner families.", region: Region.city, isHabitable: true, llothsFavor: 50, prestige: 25, arcana: 1, defense: 5, deception: -5, insight: 5, investigation: -5, religion: 5, stealth: -10, survival: -5, loot: []],
            [name: 'The Rift', discoverability: 100, description: "A deep valley at the west end of Dur'Garandal, it is the center of all black market trade in the city.", region: Region.city, isHabitable: true, llothsFavor: 10, prestige: -25, arcana: -5, defense: -5, deception: 10, insight: 10, investigation: 5, religion: -5, stealth: 10, survival: 5, loot: []],
            [name: 'Lakeside', discoverability: 100, description: 'Situated on the eastern shores of Lake Grabdar, across the Malacrada River from the Academy.', region: Region.city, isHabitable: true, llothsFavor: 15, prestige: 15, arcana: 1, defense: 5, deception: -5, insight: 5, investigation: 5, religion: 5, stealth: -5, survival: 5, loot: []],
            [name: 'Derro Flats', discoverability: 100, description: "The Derro Flats are the commercial center of Dur'Garandal. The marketplace, the arena, and one of the top eight houses are located here.", region: Region.city, isHabitable: true, llothsFavor: 10, prestige: 5, arcana: -1, defense: -5, deception: -5, insight: 10, investigation: 10, religion: -5, stealth: -5, survival: -5, loot: []],
            [name: 'The Squalor', discoverability: 100, description: "Only the lowest of the low live in this dismal part of Dur'Garandal.", region: Region.city, isHabitable: true, llothsFavor: -5, prestige: -30, arcana: -5, defense: -5, deception: 5, insight: -5, investigation: -5, religion: 5, stealth: 10, survival: 15, loot: []],
            [name: 'Proastio', discoverability: 100, description: "This area is located just in the 'shadow' of the Garandal Plateau. It is a highly respectable neighborhood, housing one of the top eight houses of the city.", region: Region.city, isHabitable: true, llothsFavor: 20, prestige: 20, arcana: 5, defense: 5, deception: -5, insight: 5, investigation: 5, religion: 5, stealth: -5, survival: -5, loot: []],
            [name: 'Gakko Peninsula', discoverability: 100, description: "Gakko Peninsula is where the Academy of Dur'Garandal is located.", region: Region.city, isHabitable: false, llothsFavor: 100, prestige: 50, arcana: 30, defense: 25, deception: -15, insight: 15, investigation: 15, religion: 25, stealth: 10, survival: 5, loot: []],
            [name: 'Carnak Island', discoverability: 100, description: "Carnak Island is located in roughly the center of Lake Grabdar and houses the city's captive Rothe population.", region: Region.city, isHabitable: false, llothsFavor: 5, prestige: 5, arcana: -5, defense: 25, deception: -10, insight: -5, investigation: -10, religion: -5, stealth: -10, survival: 15, loot: [[type: AssetType.rothe, quantity: 500]]],
            [name: 'Fleva', discoverability: 75, description: "Fleva is the location of the only known source of Adamantium within Dur'Garandal's sphere of influence.", region: Region.northernFrontier, isHabitable: false, llothsFavor: 5, prestige: 5, arcana: 10, defense: -5, deception: 5, insight: -5, investigation: -5, religion: 5, stealth: 5, survival: 5, loot: [[type: AssetType.adamantium, quantity: 500]]],
            [name: 'Malacrada Falls', discoverability: 100, description: "The Malacrada Falls are located on the north end of Dur'Garandal and are the source of the Malacrada River.", region: Region.city, isHabitable: false, llothsFavor: 5, prestige: 5, arcana: 5, defense: -5, deception: -5, insight: 5, investigation: -5, religion: -5, stealth: -5, survival: 10, loot: [[type: AssetType.water, quantity: 500]]],
            [name: 'Upper Malacrada River', discoverability: 100, description: 'The section of the river between the falls and Lake Grabdar.', region: Region.city, isHabitable: false, llothsFavor: 5, prestige: 5, arcana: 5, defense: -5, deception: -5, insight: 5, investigation: -5, religion: 5, stealth: -5, survival: 5, loot: []],
            [name: 'Lake Grabdar', discoverability: 100, description: 'The river widens at the north end of the lake and comes together again at the Gakko Peninsula on the south end of the lake.', region: Region.city, isHabitable: false, llothsFavor: 5, prestige: 5, arcana: 5, defense: -5, deception: -5, insight: 5, investigation: -5, religion: 5, stealth: -5, survival: 5, loot: [[type: AssetType.fish, quantity: 500]]],
            [name: 'Lower Malacrada River', discoverability: 100, description: 'The southern end of the Malacrada River before it goes beneath the group again at the southern end of the city.', region: Region.city, isHabitable: false, llothsFavor: 5, prestige: 5, arcana: 5, defense: -5, deception: -5, insight: 5, investigation: -5, religion: 5, stealth: -5, survival: 5, loot: []],
            [name: 'Savra', discoverability: 100, description: 'The lizard ranch located in the northeastern part of the city.', region: Region.city, isHabitable: false, llothsFavor: 5, prestige: 10, arcana: -5, defense: 5, deception: -5, insight: 5, investigation: 5, religion: 5, stealth: -10, survival: 5, loot: [[type: AssetType.lizards, quantity: 500]]],
            [name: 'Vryo', discoverability: 100, description: 'The series of small caves located in the southeastern part of the city. All moss production for the city occurs here.', region: Region.city, isHabitable: false, llothsFavor: 5, prestige: 10, arcana: 10, defense: 5, deception: -5, insight: 5, investigation: -5, religion: 5, stealth: -10, survival: 10, loot: [[type: AssetType.moss, quantity: 500]]],
            [name: 'Manitari', discoverability: 100, description: 'The almost swampy area of the city in the southern reaches. The large grove of mushrooms is located here.', region: Region.city, isHabitable: false, llothsFavor: 5, prestige: 10, arcana: 10, defense: -5, deception: 5, insight: 5, investigation: -5, religion: 5, stealth: -5, survival: 10, loot: [[type: AssetType.mushrooms, quantity: 500]]],
            [name: 'Nekrotomeio', discoverability: 100, description: "A group of several large caves located at the western end of the city. The caves have been converted into a catacombs to house the city's dead.", region: Region.city, isHabitable: false, llothsFavor: 10, prestige: 10, arcana: 10, defense: 15, deception: 5, insight: -5, investigation: 10, religion: 15, stealth: -10, survival: -5, loot: []],
            [name: 'The Arena', discoverability: 100, description: 'The city arena used for communal worship, sporting events, public executions, and other gatherings.', region: Region.city, isHabitable: true, llothsFavor: 10, prestige: 20, arcana: -5, defense: 5, deception: -15, insight: 15, investigation: 5, religion: 5, stealth: -15, survival: 5, loot: []],
            [name: 'Ooze Pits', discoverability: 75, description: "Several natural pits that have been filled with various oozes and slimes to consume the city's filth and garbage.", region: Region.easternFrontier, isHabitable: false, llothsFavor: 5, prestige: -10, arcana: 5, defense: -5, deception: 5, insight: 5, investigation: 5, religion: -5, stealth: -5, survival: 5, loot: []],
]

    @Transactional
    PointOfInterest generatePOI(Map data) {
        List<Map> loot = data.remove('loot') as List<Map>
        assert loot instanceof List
        PointOfInterest result = new PointOfInterest(data)
        result.loot = []
        pointOfInterestService.save(result)
        loot.each {
            result.loot.add(lootService.save(new Loot(it)))
        }
        data.put('loot', loot)
        pointOfInterestService.save(result)
    }

    @Override
    @Transactional
    void afterPropertiesSet() throws Exception {
        //unused, but this bean has to go afterwards, so...
        assert cityDataInitializer
        POI_DATA.each {
            generatePOI(it)
        }
    }
}
