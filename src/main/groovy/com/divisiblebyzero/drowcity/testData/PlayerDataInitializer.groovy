package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.entity.UserRole
import com.divisiblebyzero.drowcity.enums.UserRoleTypes
import com.divisiblebyzero.drowcity.service.PlayerService
import com.divisiblebyzero.drowcity.service.UserRoleService
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.transaction.Transactional

@Component
class PlayerDataInitializer implements InitializingBean {
    @Autowired
    PlayerService playerService

    @Autowired
    UserRoleService userRoleService

    final static ADMIN_USERNAME = 'erik.mason'

    final static List<Player> TEST_PLAYERS = [
            new Player(email: 'emas@rocketmail.com', username: ADMIN_USERNAME, password: 'blah', firstName: 'Erik', lastName: 'Mason'),
            new Player(email: 'test1@test.com', username: 'bob.newhart', password: 'blah', firstName: 'Bob', lastName: 'Newhart'),
            new Player(email: 'test2@test.com', username: 'mob.newhart', password: 'blah', firstName: 'Mob', lastName: 'Newhart'),
            new Player(email: 'test3@test.com', username: 'rob.newhart', password: 'blah', firstName: 'Rob', lastName: 'Newhart'),
            new Player(email: 'test4@test.com', username: 'dob.newhart', password: 'blah', firstName: 'Dob', lastName: 'Newhart'),
            new Player(email: 'test5@test.com', username: 'fob.newhart', password: 'blah', firstName: 'Fob', lastName: 'Newhart'),
            new Player(email: 'test6@test.com', username: 'cob.newhart', password: 'blah', firstName: 'Cob', lastName: 'Newhart'),
            new Player(email: 'test7@test.com', username: 'nob.newhart', password: 'blah', firstName: 'Nob', lastName: 'Newhart'),
            new Player(email: 'test8@test.com', username: 'sob.newhart', password: 'blah', firstName: 'Sob', lastName: 'Newhart'),
    ]

    final static List<UserRole> USER_ROLES = [
        new UserRole(username: ADMIN_USERNAME, role: UserRoleTypes.userAdmin)
    ]

    @Override
    @Transactional
    void afterPropertiesSet() throws Exception {
        TEST_PLAYERS.each { Player player ->
            playerService.createPlayer(player)
        }
        USER_ROLES.each { UserRole it ->
            userRoleService.createRole(it)
        }
    }
}
