package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.entity.*
import com.divisiblebyzero.drowcity.enums.*
import com.divisiblebyzero.drowcity.service.*
import com.divisiblebyzero.drowcity.util.NameGenerator
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.transaction.Transactional

import static com.divisiblebyzero.drowcity.enums.AssetLineItemType.found
import static com.divisiblebyzero.drowcity.enums.ConsumptionModel.normal

@Component
class HouseDataInitializer implements InitializingBean {
    @Autowired
    CityDataInitializer cityDataInitializer

    @Autowired
    CityService cityService

    @Autowired
    HouseService houseService

    @Autowired
    AssetService assetService

    @Autowired
    AssetLineItemService assetLineItemService

    @Autowired
    WorkerGroupService workerGroupService

    @Autowired
    WorkerGroupLineItemService workerGroupLineItemService

    @Autowired
    IndividualService individualService

    @Autowired
    OrganizationalMembershipRelationshipService organizationalMembershipRelationshipService

    WorkerGroup generateWorkerGroup(Organization employer, WorkerType workerType, EmploymentType employmentType) {
        WorkerGroup result = new WorkerGroup(employer: employer, workerType: workerType, employmentType: employmentType, consumptionModel: normal, lineItems: [])
        workerGroupService.save(result)
        result.lineItems.add(new WorkerGroupLineItem(workerGroup: result, week: 0, change: NameGenerator.random(10), type: WorkerGroupLineItemType.hire))
        workerGroupLineItemService.saveAll(result.lineItems)
        workerGroupService.save(result)
    }

    OrganizationalMembershipRelationship generateNoble(Organization house, OrganizationalRelationshipStatus status, Archetype archetype, Gender gender) {
        Individual noble = new Individual(city: house.city, name: NameGenerator.nameForGender(gender), gender: gender, archetype: archetype, relationships: [])
        individualService.save(noble)
        OrganizationalMembershipRelationship nobleRel = new OrganizationalMembershipRelationship(from: house, to: noble, status: status, loyalty: Loyalty.loyal)
        organizationalMembershipRelationshipService.save(nobleRel)
    }

    @Override
    @Transactional
    void afterPropertiesSet() throws Exception {
        //unused, but this bean has to go afterwards, so...
        assert cityDataInitializer
        City city = cityService.findCityByName(CityDataInitializer.TEST_CITY_NAME)
        (0..11).each {
            Organization house = new Organization(city: city, player: city.gameMaster, name: NameGenerator.houseName(), type: OrganizationType.house, members: [], workerGroups: [], compounds: [])
            houseService.save(house)
            house.assets = AssetType.values().collect {AssetType assetType ->
                assetService.generateAndSaveNewAsset(0, assetType, found, NameGenerator.random(10))
            }
            houseService.save(house)
            house.workerGroups.add(generateWorkerGroup(house, WorkerType.values()[NameGenerator.random(WorkerType.values().size()) - 1], EmploymentType.values()[NameGenerator.random(EmploymentType.values().size()) - 1]))
            house.workerGroups.add(generateWorkerGroup(house, WorkerType.values()[NameGenerator.random(WorkerType.values().size()) - 1], EmploymentType.values()[NameGenerator.random(EmploymentType.values().size()) - 1]))
            house.workerGroups.add(generateWorkerGroup(house, WorkerType.values()[NameGenerator.random(WorkerType.values().size()) - 1], EmploymentType.values()[NameGenerator.random(EmploymentType.values().size()) - 1]))
            house.workerGroups.add(generateWorkerGroup(house, WorkerType.values()[NameGenerator.random(WorkerType.values().size()) - 1], EmploymentType.values()[NameGenerator.random(EmploymentType.values().size()) - 1]))
            house.workerGroups.add(generateWorkerGroup(house, WorkerType.values()[NameGenerator.random(WorkerType.values().size()) - 1], EmploymentType.values()[NameGenerator.random(EmploymentType.values().size()) - 1]))
            houseService.save(house)
            house.members.add(generateNoble(house, OrganizationalRelationshipStatus.matron, Archetype.matron, Gender.female))
            house.members.add(generateNoble(house, OrganizationalRelationshipStatus.headPriestess, Archetype.headPriestess, Gender.female))
            house.members.add(generateNoble(house, OrganizationalRelationshipStatus.headWizard, Archetype.headWizard, Gender.male))
            house.members.add(generateNoble(house, OrganizationalRelationshipStatus.weaponMaster, Archetype.weaponMaster, Gender.male))
            house.members.add(generateNoble(house, OrganizationalRelationshipStatus.spyMaster, Archetype.spyMaster, Gender.male))
            organizationalMembershipRelationshipService.saveAll(house.members)
            houseService.save(house)
        }
    }
}
