package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.entity.Marketplace
import com.divisiblebyzero.drowcity.entity.PointOfInterest
import com.divisiblebyzero.drowcity.enums.AssetType
import com.divisiblebyzero.drowcity.service.MarketplaceService
import com.divisiblebyzero.drowcity.service.PointOfInterestService
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.transaction.Transactional

@Component
class MarketplaceInitializer implements InitializingBean {
    @Autowired
    PointOfInterestInitializer pointOfInterestInitializer

    @Autowired
    PointOfInterestService pointOfInterestService

    @Autowired
    MarketplaceService marketplaceService

    final static List<Map> MARKETS_DATA = [
            [name: 'The Rift Marketplace', description: 'The black market located in The Rift.', location: 'The Rift', allowedAssetTypes: AssetType.values() - AssetType.gold, isBlackMarket: true],
            [name: "Dur'Garandal Marketplace", description: 'The main marketplace of the city.', location: 'Derro Flats', allowedAssetTypes: AssetType.values() - AssetType.gold, isBlackMarket: false],
    ]

    @Override
    @Transactional
    void afterPropertiesSet() throws Exception {
        //unused, but this bean has to go afterwards, so...
        assert pointOfInterestInitializer
        MARKETS_DATA.each { Map it ->
            PointOfInterest poi = pointOfInterestService.findByName(it.location)
            Marketplace newMarketplace = new Marketplace(name: it.name, description: it.description, location: poi, allowedAssetTypes: it.allowedAssetTypes, isBlackMarket: it.isBlackMarket)
            marketplaceService.save(newMarketplace)
        }
    }
}
