package com.divisiblebyzero.drowcity.testData


import com.divisiblebyzero.drowcity.entity.City
import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.service.CityService
import com.divisiblebyzero.drowcity.service.PlayerService
import com.divisiblebyzero.drowcity.util.NameGenerator
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.transaction.Transactional

@Component
class CityDataInitializer implements InitializingBean {
    @Autowired
    PlayerDataInitializer playerDataInitializer

    @Autowired
    CityService cityService

    @Autowired
    PlayerService playerService

    final static String TEST_CITY_NAME = "Dur'garandal"

    @Override
    @Transactional
    void afterPropertiesSet() throws Exception {
        //unused, but this bean has to go afterwards, so...
        assert playerDataInitializer
        Player gameMaster = playerService.findPlayerForUsername(PlayerDataInitializer.ADMIN_USERNAME)
        City newCity = new City(
                gameMaster: gameMaster,
                name: TEST_CITY_NAME,
                week: NameGenerator.random(300)
        )
        cityService.save(newCity)
    }
}
