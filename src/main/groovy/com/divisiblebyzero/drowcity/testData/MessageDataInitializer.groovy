package com.divisiblebyzero.drowcity.testData

import com.divisiblebyzero.drowcity.entity.Individual
import com.divisiblebyzero.drowcity.entity.Message
import com.divisiblebyzero.drowcity.service.IndividualService
import com.divisiblebyzero.drowcity.service.MessageService
import com.divisiblebyzero.drowcity.util.NameGenerator
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.transaction.Transactional

@Component
class MessageDataInitializer implements InitializingBean {
    @Autowired
    HouseDataInitializer houseDataInitializer

    @Autowired
    MessageService messageService

    @Autowired
    IndividualService individualService

    Individual pickAFrom() {
        List<Individual> individuals = individualService.findAll()
        individuals.get(NameGenerator.random(individuals.size()) - 1)
    }

    Individual pickATo(Individual from) {
        List<Individual> individuals = individualService.findAll()
        Individual result = individuals.get(NameGenerator.random(individuals.size()) - 1)
        while(result == from) {
            result = individuals.get(NameGenerator.random(individuals.size()) - 1)
        }
        return result
    }

    @Override
    @Transactional
    void afterPropertiesSet() throws Exception {
        //unused, but this bean has to go afterwards, so...
        assert houseDataInitializer
        (0..99).each {
            Message message = new Message(from: pickAFrom(), week: NameGenerator.random(50), sentAnonymously: false, sentPublicly: false, subject: "Test subject #$it", message: "Test message #$it")
            message.to = pickATo(message.from)
            messageService.sendMessage(message)
        }
    }
}
