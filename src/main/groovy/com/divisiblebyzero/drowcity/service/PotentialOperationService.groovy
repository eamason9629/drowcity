package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.PotentialOperation
import com.divisiblebyzero.drowcity.repository.PotentialOperationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PotentialOperationService {
    @Autowired
    PotentialOperationRepository potentialOperationRepository

    PotentialOperation save(PotentialOperation potentialOperation) {
        potentialOperationRepository.save(potentialOperation)
    }
}
