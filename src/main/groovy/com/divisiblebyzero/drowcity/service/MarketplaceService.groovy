package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.Marketplace
import com.divisiblebyzero.drowcity.repository.MarketplaceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MarketplaceService {
    @Autowired
    MarketplaceRepository marketplaceRepository

    Marketplace save(Marketplace marketplace) {
        marketplaceRepository.save(marketplace)
    }
}
