package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.dto.HouseCreationCompoundFeatureDTO
import com.divisiblebyzero.drowcity.dto.HouseCreationProcessDTO
import com.divisiblebyzero.drowcity.dto.NewHouseDTO
import com.divisiblebyzero.drowcity.dto.NewIndividualDTO
import com.divisiblebyzero.drowcity.enums.Archetype
import org.springframework.stereotype.Service

import static com.divisiblebyzero.drowcity.enums.WorkerType.*

@Service
class HouseCreationService {
    final static int STARTING_POINTS = 10000
    final static int GOLD_RATIO = 1000

    List<String> generateRecommendations(HouseCreationProcessDTO processData) {
        List<String> result = []
        Map<Archetype, Integer> nobleCounts = [:]
        processData.nobles.each { NewIndividualDTO it ->
            //TODO
        }
        return result
    }

    HouseCreationProcessDTO calculateRemainingPoints(HouseCreationProcessDTO processData) {
        NewHouseDTO newHouse = processData.newHouse
        processData.remainingPoints = STARTING_POINTS
        processData.remainingPoints -= newHouse.gold / GOLD_RATIO
        processData.remainingPoints -= newHouse.familyTrait.creationCost
        processData.remainingPoints -= newHouse.rotheFarmBid
        processData.remainingPoints -= newHouse.adamantiumMineBid
        processData.remainingPoints -= newHouse.arenaBid
        processData.remainingPoints -= newHouse.fishmongeringBid
        processData.remainingPoints -= newHouse.mossFarmingBid
        processData.remainingPoints -= newHouse.mushroomFarmingBid
        processData.remainingPoints -= newHouse.waterSupplyBid
        processData.remainingPoints -= newHouse.garbageDumpBid
        processData.remainingPoints -= newHouse.mortuaryBid
        processData.remainingPoints -= newHouse.lizardFarmingBid
        processData.remainingPoints -= newHouse.soldierCount * soldier.creationCost
        processData.remainingPoints -= newHouse.priestessCount * priestess.creationCost
        processData.remainingPoints -= newHouse.wizardCount * wizard.creationCost
        processData.remainingPoints -= newHouse.staffCount * staff.creationCost
        processData.remainingPoints -= newHouse.orcCount * orc.creationCost
        processData.remainingPoints -= newHouse.goblinCount * goblin.creationCost
        processData.remainingPoints -= newHouse.koboldCount * kobold.creationCost
        processData.remainingPoints -= newHouse.duergarCount * duergar.creationCost
        processData.remainingPoints -= newHouse.dwarfCount * dwarf.creationCost
        processData.remainingPoints -= newHouse.gnomeCount * gnome.creationCost
        processData.remainingPoints -= newHouse.minotaurCount * minotaur.creationCost
        processData.remainingPoints -= newHouse.beholderCount * beholder.creationCost
        processData.remainingPoints -= newHouse.illithidCount * illithid.creationCost
        processData.remainingPoints -= newHouse.kuoToaCount * kuoToa.creationCost
        processData.remainingPoints -= newHouse.bugbearCount * bugbear.creationCost
        processData.remainingPoints -= newHouse.lamiaCount * lamia.creationCost
        processData.remainingPoints -= newHouse.nightmareCount * nightmare.creationCost
        processData.remainingPoints -= newHouse.zombieCount * zombie.creationCost
        processData.remainingPoints -= newHouse.skeletonCount * skeleton.creationCost
        processData.remainingPoints -= newHouse.driderCount * drider.creationCost
        processData.compoundFeatures.each { HouseCreationCompoundFeatureDTO it ->
            processData.remainingPoints -= it.quantity * it.compoundFeature.creationCost
        }
        processData.nobles.each { NewIndividualDTO it ->
            processData.remainingPoints -= it.gender.creationCost
            processData.remainingPoints -= it.archetype.creationCost
            processData.remainingPoints -= (it.llothsFavor / 10) * it.llothsFavor + it.llothsFavor
            processData.remainingPoints -= (it.arcana / 10) * it.arcana + it.arcana
            processData.remainingPoints -= (it.attack / 10) * it.attack + it.attack
            processData.remainingPoints -= (it.defense / 10) * it.defense + it.defense
            processData.remainingPoints -= (it.deception / 10) * it.deception + it.deception
            processData.remainingPoints -= (it.insight / 10) * it.insight + it.insight
            processData.remainingPoints -= (it.investigation / 10) * it.investigation + it.investigation
            processData.remainingPoints -= (it.religion / 10) * it.religion + it.religion
            processData.remainingPoints -= (it.stealth / 10) * it.stealth + it.stealth
            processData.remainingPoints -= (it.survival / 10) * it.survival + it.survival
        }
        return processData
    }

    HouseCreationProcessDTO createHouse() {
        return new HouseCreationProcessDTO(remainingPoints: STARTING_POINTS)
    }
}
