package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.dto.CityDTO
import com.divisiblebyzero.drowcity.entity.City
import com.divisiblebyzero.drowcity.repository.CityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CityService {
    @Autowired
    CityRepository cityRepository

    City findCityById(UUID uuid) {
        cityRepository.findById(uuid).get()
    }

    City createCity(CityDTO cityDTO) {
        City newCity = new City(name: cityDTO.name)
        cityRepository.save(newCity)
    }

    List<City> listCities() {
        cityRepository.findAll().toList()
    }

    City save(City city) {
        cityRepository.save(city)
    }

    City findCityByName(String name) {
        cityRepository.findByName(name)
    }
}
