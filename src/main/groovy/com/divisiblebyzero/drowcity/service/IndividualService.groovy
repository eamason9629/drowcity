package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.Individual
import com.divisiblebyzero.drowcity.repository.IndividualRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class IndividualService {
    @Autowired
    IndividualRepository individualRepository

    List<Individual> findAll() {
        individualRepository.findAll().toList()
    }

    Individual findById(UUID uuid) {
        individualRepository.findById(uuid).get()
    }

    Individual save(Individual individual) {
        individualRepository.save(individual)
    }
}
