package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.AssetLineItem
import com.divisiblebyzero.drowcity.repository.AssetLineItemRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AssetLineItemService {
    @Autowired
    AssetLineItemRepository assetLineItemRepository

    List<AssetLineItem> saveAll(List<AssetLineItem> lineItems) {
        assetLineItemRepository.saveAll(lineItems).toList()
    }

    AssetLineItem save(AssetLineItem assetLineItem) {
        assetLineItemRepository.save(assetLineItem)
    }
}
