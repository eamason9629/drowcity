package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.WorkerGroupLineItem
import com.divisiblebyzero.drowcity.repository.WorkerGroupLineItemRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class WorkerGroupLineItemService {
    @Autowired
    WorkerGroupLineItemRepository workerGroupLineItemRepository

    List<WorkerGroupLineItem> saveAll(List<WorkerGroupLineItem> items) {
        workerGroupLineItemRepository.saveAll(items).toList()
    }
}
