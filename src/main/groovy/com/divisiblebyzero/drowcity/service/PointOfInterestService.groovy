package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.PointOfInterest
import com.divisiblebyzero.drowcity.repository.PointOfInterestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PointOfInterestService {
    @Autowired
    PointOfInterestRepository pointOfInterestRepository

    PointOfInterest findByName(String name) {
        pointOfInterestRepository.findByName(name)
    }

    PointOfInterest save(PointOfInterest poi) {
        pointOfInterestRepository.save(poi)
    }

    List<PointOfInterest> saveAll(List<PointOfInterest> pointOfInterests) {
        pointOfInterestRepository.saveAll(pointOfInterests).toList()
    }
}
