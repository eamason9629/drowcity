package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.OrganizationalMembershipRelationship
import com.divisiblebyzero.drowcity.repository.OrganizationalMembershipRelationshipRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class OrganizationalMembershipRelationshipService {
    @Autowired
    OrganizationalMembershipRelationshipRepository organizationalMembershipRelationshipRepository

    OrganizationalMembershipRelationship save(OrganizationalMembershipRelationship relationship) {
        organizationalMembershipRelationshipRepository.save(relationship)
    }

    List<OrganizationalMembershipRelationship> saveAll(List<OrganizationalMembershipRelationship> relationships) {
        organizationalMembershipRelationshipRepository.saveAll(relationships).toList()
    }
}
