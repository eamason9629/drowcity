package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.Individual
import com.divisiblebyzero.drowcity.entity.Message
import com.divisiblebyzero.drowcity.repository.MessageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.transaction.Transactional

@Service
class MessageService {
    @Autowired
    MessageRepository messageRepository

    Message findMessageById(UUID uuid) {
        messageRepository.findById(uuid).get()
    }

    @Transactional
    Message sendMessage(Message message) {
        messageRepository.save(message)
    }

    List<Message> findUnreadMessagesTo(Individual individual) {
        messageRepository.findByToAndReadByToTrue(individual)
    }

    List<Message> findMessagesTo(Individual individual) {
        messageRepository.findByTo(individual)
    }
}
