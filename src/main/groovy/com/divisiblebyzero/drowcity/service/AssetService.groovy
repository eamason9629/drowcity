package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.Asset
import com.divisiblebyzero.drowcity.entity.AssetLineItem
import com.divisiblebyzero.drowcity.enums.AssetLineItemType
import com.divisiblebyzero.drowcity.enums.AssetType
import com.divisiblebyzero.drowcity.repository.AssetRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.transaction.Transactional

@Service
class AssetService {
    @Autowired
    AssetRepository assetRepository

    @Autowired
    AssetLineItemService assetLineItemService

    List<Asset> saveAll(List<Asset> assets) {
        assetRepository.saveAll(assets).toList()
    }

    Asset save(Asset asset) {
        assetRepository.save(asset)
    }

    @Transactional
    Asset generateAndSaveNewAsset(int week = 0, AssetType type = AssetType.gold, AssetLineItemType lineItemType = AssetLineItemType.found, int initialValue = 0) {
        Asset newAsset = new Asset(type: type, lines: [])
        save(newAsset)
        newAsset.lines.add(new AssetLineItem(week: week, change: initialValue, type: lineItemType, asset: newAsset))
        assetLineItemService.saveAll(newAsset.lines)
        save(newAsset)
    }

    Asset findById(UUID uuid) {
        assetRepository.findById(uuid).get()
    }
}
