package com.divisiblebyzero.drowcity.service


import com.divisiblebyzero.drowcity.repository.CityRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Slf4j
@Service
class RandomCityService {
    @Autowired
    CityRepository cityRepository

//    @Autowired
//    HouseService houseService
//
//    @Autowired
//    HouseRepository houseRepository
//
//    @Autowired
//    PlayerCharacterRepository playerCharacterRepository
//
//    @Autowired
//    CharacterToCharacterRelationshipService characterRelationshipService
//
//    @Autowired
//    HouseToHouseRelationshipService houseRelationshipService
//
//    @Transactional
//    City randomCity() {
//        City result = cityRepository.save(new City(name: NameGenerator.cityName(), drowCommoners: rollDice(1200, DieType.d6, 3000)))
//        int numHouses = rollDice(10, DieType.d6, 10)    //20-70 houses
//        (1..numHouses).each {
//            result.houses += randomHouse(result)
//        }
//        houseRelationshipService.buildRelationships(result)
//        characterRelationshipService.buildRelationships(result)
//        return cityRepository.save(result)
//    }
//
//    @Transactional
//    House randomHouse(City city) {
//        House result = new House(city: city)
//        result.name = NameGenerator.houseName()
//        result.llothsFavor = rollDice(10, DieType.d8, -20)
//        result.ageInWeeks = 0
//        result.gold = rollDice(100, DieType.d20, 2200)
//        result.npcHouse = true
//        result.soldiers = rollDice(10, DieType.d20, 100)
//        result = houseRepository.save(result)
//        result.matron = randomCharacter(result)
//        result.matron.gender = Gender.female
//        result.matron.archetype = Archetype.matron
//        result.headWizard = coinFlip() ? randomCharacter(result) : null
//        result.headWizard?.gender = Gender.male
//        result.headWizard?.archetype = Archetype.headWizard
//        result.weaponMaster = coinFlip() ? randomCharacter(result) : null
//        result.weaponMaster?.gender = Gender.male
//        result.weaponMaster?.archetype = Archetype.weaponMaster
//        result.spyMaster = coinFlip() ? randomCharacter(result) : null
//        result.spyMaster?.gender = Gender.male
//        result.spyMaster?.archetype = Archetype.spyMaster
//        (0..rollDice(1, DieType.d8, -1)).each {
//            result.nobles += randomCharacter(result)
//        }
//        return houseRepository.save(result)
//    }
//
//    PlayerCharacter randomCharacter(House house) {
//        PlayerCharacter result = new PlayerCharacter(house: house)
//        result.gender = Gender.random()
//        result.name = result.gender == Gender.male ? NameGenerator.maleName() : NameGenerator.femaleName()
//        result.ageInWeeks = rollDice(5, DieType.d20, -4) * 52
//        result.llothsFavor = rollDice(1, DieType.d8) * 4
//        return playerCharacterRepository.save(result)
//    }
}
