package com.divisiblebyzero.drowcity.service


import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.entity.UserRole
import com.divisiblebyzero.drowcity.enums.UserRoleTypes
import com.divisiblebyzero.drowcity.repository.PlayerRepository
import com.divisiblebyzero.drowcity.repository.UserRoleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

import javax.transaction.Transactional

@Service
class PlayerService {
    @Autowired
    BCryptPasswordEncoder passwordEncoder

    @Autowired
    PlayerRepository playerRepository

    @Autowired
    UserRoleRepository userRoleRepository

    @Transactional
    Player createPlayer(Player player) {
        String encodedPassword = passwordEncoder.encode(player.password)
        player.password = encodedPassword
        userRoleRepository.save(new UserRole(username: player.username, role: UserRoleTypes.generic))
        playerRepository.save(player)
    }

    Player findPlayerForUsername(String username) {
        playerRepository.findByUsername(username)
    }

    Player save(Player player) {
        playerRepository.save(player)
    }
}
