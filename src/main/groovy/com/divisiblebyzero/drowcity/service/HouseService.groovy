package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.enums.OrganizationType
import com.divisiblebyzero.drowcity.repository.OrganizationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class HouseService {
    @Autowired
    OrganizationRepository organizationRepository

    List<Organization> findHousesForPlayer(Player player) {
        organizationRepository.findByPlayerAndType(player, OrganizationType.house)
    }

    Organization save(Organization organization) {
        assert organization.type == OrganizationType.house
        organizationRepository.save(organization)
    }

    Organization findHouseById(UUID id) {
        organizationRepository.findById(id).get()
    }
}
