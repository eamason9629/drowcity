package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.entity.UserRole
import com.divisiblebyzero.drowcity.repository.UserRoleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserRoleService {
    @Autowired
    UserRoleRepository userRoleRepository

    List<UserRole> findRolesForPlayer(Player player) {
        userRoleRepository.findAllByUsername(player?.username)
    }

    UserRole createRole(UserRole userRole) {
        userRoleRepository.save(userRole)
    }
}
