package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.City
import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.repository.OrganizationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class OrganizationService {
    @Autowired
    OrganizationRepository organizationRepository

    Organization findById(UUID id) {
        organizationRepository.findById(id).get()
    }

    List<Organization> findForCity(City city) {
        organizationRepository.findByCity(city)
    }
}
