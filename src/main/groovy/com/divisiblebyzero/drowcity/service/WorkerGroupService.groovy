package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.WorkerGroup
import com.divisiblebyzero.drowcity.repository.WorkerGroupRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class WorkerGroupService {
    @Autowired
    WorkerGroupRepository workerGroupRepository

    WorkerGroup save(WorkerGroup group) {
        workerGroupRepository.save(group)
    }

    WorkerGroup findById(UUID uuid) {
        workerGroupRepository.findById(uuid).get()
    }
}
