package com.divisiblebyzero.drowcity.service

import com.divisiblebyzero.drowcity.entity.Loot
import com.divisiblebyzero.drowcity.repository.LootRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class LootService {
    @Autowired
    LootRepository lootRepository

    Loot save(Loot loot) {
        lootRepository.save(loot)
    }

    List<Loot> saveAll(List<Loot> loot) {
        lootRepository.saveAll(loot).toList()
    }
}
