package com.divisiblebyzero.drowcity.util

import com.divisiblebyzero.drowcity.enums.Gender

class NameGenerator {
    final static List<String> CITY_NAMES = [
            'Menzoberranzan', 'Abaethaggar', 'Abburth', 'Baereghel', "Charrvhel'raugaust", 'Chalssin', 'Ched Nasad', 'Cheth Rrhinn', "Dyon G'ennivalz", 'Erelhei-Cinlu', 'Erundlyn', 'Faneadar', 'Guallidurth', 'Haundrauth', 'Ithilaughym', 'Karsuluthiyl', "Lith My'athar", 'Llecerellyn', 'Llurth Drier', "Luihaulen'tar", 'Maerimydra', 'Orlytlar', 'Rilauven', 'Sschindylryn', 'Sshamath', 'Sshanntynlan', 'Szithlin', "T'lindhet", 'Telnarquel', 'Tyrybblyn', 'Uluitur', 'Undraeth', "Undrek'Thoz", 'Ungethal', 'Ust Natha', "V'elddrinnsshar", 'Waethe Hlammachar', 'Yuethindrynn'
    ]

    static String cityName() {
        CITY_NAMES[random(CITY_NAMES.size()) - 1]
    }

    final static List<String> HOUSE_NAMES = [
            'Agrach Dyrr', 'Auvryndar', 'Baenre', "Barrison Del'Armgo", "Bron'tej", 'Celofraie', 'Chumavh', 'Despana', 'DeVir', 'Dhuurniv', "Do'Urden", "Druu'giir", 'Duskryn', "Elec'thil", 'Faen Tlabbar', 'Fey-Branche', 'Freth', 'Godeep', "H'kar", 'Horlbar', 'Aleval', 'Arabani', 'Arkhenneld', 'Claddath', 'Everhate', 'Helviiryn', 'Hlaund', 'Maerret', "Rilynt'tar", "Hun'ett", 'Hunzrin', "Jae'llat", 'Jaelre', 'Jaezred Chaulssin', 'Kenafin', 'Maevret', "Masq'il'yr", 'Melarn', 'Millithor', 'Mizzrym', "Mlin'thobbyn", 'Nasadra', 'Nurbonnis', 'Oblodra', 'Ousstyl', 'Pharn', "S'sril", 'Shobalar', "Srune'Lett", 'Symryvvin', "Syr'thaerl", "T'orgh", "T'sarran", "Tanor'Thal", "Teken'duis", 'Teyachumet', 'Thaeyalla', "Tuin'Tarl", 'Vandree', "X'larraz'et'soj", 'Xorlarrin', 'Yauntyrr', 'Zauvirr', 'Zolond', 'Asbodela', 'Balartyr', 'Bluirren', 'Catanzaro', "Chueth'duis", 'Darnruel', "Dlaen' Del'Amatar", 'Fael Olyphar', "Hael'Irin", "H'tithet", "Illith'vir", 'Ilueph', 'Ilystryph', "Klor'Ibar", 'Llarabbar', 'Miliskeera', 'Neereath', "Ol'il'isk", 'Ouol', 'Ryrrl', 'Shun Tahaladar', "Ssh'starm", 'Thadalix', 'Tirin', "Tuek'tharm", 'Ulutar', 'Urundlet', 'Vahadarr', "Waeth del'tar", "Yulaun'tlar", "Yune'duis"
    ]

    static String houseName() {
        HOUSE_NAMES[random(HOUSE_NAMES.size()) - 1]
    }

    final static List<String> MALE_NAMES = [
            'Alton', 'Balok', 'Baragh', 'Belaern', 'Belgos', 'Bemril', "Berg'inyon", 'Bhintel', 'Brorn', 'Bruherd', 'Caelkoth', 'Callimar', 'Chakos', 'Chaszmyr', 'Coranzen', 'Dantrag', 'Dhuunyl', 'Dinin', 'Dresmorlin', 'Dro', 'Duagloth', 'Durdyn', 'Elamshin', 'Elendar', 'Elkantar', 'Filraen', 'Ghaundan', 'Ghaundar', 'Guldor', 'Gwylyss', 'Hadrogh', "Hatch'net", 'Honemmeth', 'Houndaer', 'Ildan', 'Ilmryn', 'Ilphrin', 'Imbros', 'Irennan', 'Istolil', 'Istorvir', 'Iymril', 'Jaezred', 'Jalynfein', 'Jeggred', 'Jevan', 'Jhaamdath', 'Jhaldrym', 'Jivvin', 'Jyslin', "K'yorl", 'Kalannar', 'Kethan', 'Kluthruel', 'Kophyn', 'Krenaste', 'Krondorl', 'Kyorlin', 'Lesaonar', 'Lirdnolu', 'Llaulmyn', 'Malaggar', 'Micarlin', 'Minolin', 'Molvayas', 'Morennel', 'Nadal', 'Nalfein', 'Narissorin', 'Narlros', 'Nilonim', 'Nimruil', "Numrini'th", 'Nyloth', 'Nym', 'Omareth', 'Orgoloth', 'Ornaryn', 'Pharaun', 'Pharius', 'Quave', 'Quendar', 'Quenthel', 'Quevven', 'Ranaghar', 'Relonor', 'Riklaunim', 'Rinnill', 'Ristel', 'Ruathym', 'Ryld', 'Ryltar', 'Sabal', 'Selakiir', 'Seldszar', 'Sengo', 'Solaufein', 'Sorn', 'Syrdar', 'Szordrin', 'Taldinyon', 'Tarlyn', 'Tazennin', 'Tebryn', 'Tolokoph', 'Torrellan', 'Trelgath', 'Tsabrak', 'Urlryn', 'Valas', 'Veldrin', 'Velkyn', 'Vhurdaer', 'Vhurindrar', 'Vielyn', 'Vlondril', 'Vorn', 'Vuzlyn', 'Welverin', 'Xarann', 'Xundus', 'Yazston', 'Yuimmar', 'Zaknafein', 'Zeerith', 'Zyn'
    ]

    static String maleName() {
        MALE_NAMES[random(MALE_NAMES.size()) - 1]
    }

    final static List<String> FEMALE_NAMES = [
            'Ahlysaaria', 'Akordia', 'Alaunirra', 'Alystin', 'Amalica','Angaste','Anluryn','Ardulace','Aunrae','Balaena','Baltana','Bautha','Belarbreena','Beszrima','Brigantyna','Briza','Brorna','Burryna','Byrtyn','Cazna','Chadra','Chadzina','Chalithra','Chandara','Chardalyn','Charinida','Charlindra','Chenzira','Chessintra','Dhaunae','Dilynrae','Drada','Drisinil','Eclavdra','Elerra','Elvanshalee','Elvraema','Erakasyne','Ereldra','Faeryl','Felyndiira','Filfaere',"G'eldriia ",'Gaussra','Ghilanna','Greyanna','Gurina','Haelra','Halisstra','Ilharess','Ilivarrra','Ilmra','Imrae','Jaelryn','Jezzara','Jhaelryna','Jhaelrynna','Jhalass','Jhangara','Jhanniss','Jhulae','Khaless','Kiaran','Laele','Larynda','LiNeerlay','Lledrith','Llolfaen','Lualyrr','Lythrana','Malice','Maya','Menzoberra',"Mez'Barris",'Micarlin',"Miz'ri",'Mizzrym','Myrymma','Narcelia','Nathrae','Nedylene','Nendra','Nizana','Nulliira','Olorae','Pellanistra','Phaere','Phyrra','Qilue','Quarra','Rauva','Rilrae','Sabrae','Saradreza','Sassandra','Schezalle','Shimyra','ShriNeerune','Shulvallriel','Shurdriira','Shurraenil','Shyntlara','SiNafay','Sindyrrith','Solenzara','Ssapriina',"T'risstree",'Talabrina','Talice','Tallrene','Thalra','Thirza','Thraele','Triel','Ulitree','Ulviirala','Umrae','Urlryn','Urmelena','Vhondryl','Viconia','Vierna','Vornalla','Waerva','Wuyondra','Xalyth','Xullrae','Xune','Yasrena','Yvonnel',"Z'ress",'Zarra','Zebeyana','Zeerith','Zelpassa','Zendalure','Zesstra','Zilvra'
    ]

    static String femaleName() {
        FEMALE_NAMES[random(FEMALE_NAMES.size()) - 1]
    }

    static String nameForGender(Gender gender) {
        gender == Gender.female ? femaleName() : maleName()
    }

    static random(int max) {
        (int) (Math.random() * max) + 1
    }
}
