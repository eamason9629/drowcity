package com.divisiblebyzero.drowcity.util

import com.divisiblebyzero.drowcity.enums.DieType

class DieRoller {
    static int rollDice(int numberOfDice, DieType dieType, int modifier = 0) {
        if(numberOfDice == 0) { return modifier }
        int result = modifier
        (1..numberOfDice).each { result += (int) (Math.random() * dieType.maxValue()) + 1 }
        return result
    }

    static boolean coinFlip() {
        rollDice(1, DieType.d2) == 1
    }

    static Object pickOne(List list) {
        int index = (int) (Math.random() * list.size())
        return list[index]
    }
}
