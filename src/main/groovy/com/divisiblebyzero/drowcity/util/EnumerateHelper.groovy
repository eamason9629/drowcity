package com.divisiblebyzero.drowcity.util

import com.divisiblebyzero.drowcity.dto.EnumDTO

abstract class EnumerateHelper {
    static EnumDTO createFrom(Class enumerate) {
        EnumDTO result = new EnumDTO(name: enumerate.simpleName)
        enumerate.values().each { it->
            Map value = [name: it.name(), prettyName: it.prettyName, description: it.description]
            if(it.hasProperty('creationCost')) {
                value.creationCost = it.creationCost
            }
            result.values[it.name() as String] = value
        }
        return result
    }
}
