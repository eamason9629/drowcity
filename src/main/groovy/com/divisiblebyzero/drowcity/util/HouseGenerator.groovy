package com.divisiblebyzero.drowcity.util

import com.divisiblebyzero.drowcity.entity.City
import com.divisiblebyzero.drowcity.entity.Individual
import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.entity.OrganizationalMembershipRelationship
import com.divisiblebyzero.drowcity.enums.Archetype
import com.divisiblebyzero.drowcity.enums.Gender
import com.divisiblebyzero.drowcity.enums.OrganizationType
import com.divisiblebyzero.drowcity.enums.OrganizationalRelationshipStatus

import static com.divisiblebyzero.drowcity.enums.Archetype.*

abstract class HouseGenerator {
    static Organization generateHouse(int pointValue, City city) {
        //What goes into a new house?
        //name = random
        //player is the same as city owner player
        //type == House
        //members are the nobles
        //  * 5 nobles at 10,000 xp
        //  * 1 noble added for every additional 10,000 xp?
        //workerGroups will also vary by pointValues
        //compounds... spend 1/3 points on compound? might have to see what the players did
        //gold, maybe keep the same number of gold as point value
        //food will be based on how many workers there are
        //weapons...same
        //lizards, only the top eight would have them probably
        //potions, none to start
        //llothsFavor = maybe random or calculated based on the other features of the house and family
        //prestige, arcana, attack, etc, are based on features and stuff
        Organization result = new Organization(city: city, player: city.gameMaster, type: OrganizationType.house)
        long numNobles = 4 + Math.round(pointValue / 10000)
        numNobles.times {
            List<Archetype> archetypes = [matron, headPriestess, weaponMaster, spyMaster, headWizard]
            Archetype chosenArchetype
            if (it > archetypes.size()) {
                List<Archetype> possibilities = [highPriestess, priestess, acolyte, journeymanWizard, wizard, neophyte, championWarrior, soldier, warrior, conscript, blackguard, scoundrel, hooligan, lowLife, child]
                Collections.shuffle(possibilities)
                chosenArchetype = possibilities.first()
            } else {
                chosenArchetype = archetypes[it - 1]
            }
            result.members.add(createNoble(result, chosenArchetype))
        }
        return result
    }

    static OrganizationalMembershipRelationship createNoble(Organization house, Archetype archetype) {
        OrganizationalMembershipRelationship relationship = new OrganizationalMembershipRelationship(from: house, to: createIndividual(archetype), status: OrganizationalRelationshipStatus.member)

        return relationship
    }

    final static Map<String, Map<String, Integer>> BASE_STATS = [
            matron                 : [llothsFavor: 100, ageInWeeks: 400 * 52, prestige: 50, arcana: 40, attack: 35, defense: 40, deception: 30, insight: 40, investigation: 30, religion: 100, stealth: 25, survival: 50],
            arachTinilithMatron    : [llothsFavor: 100, ageInWeeks: 300 * 52, prestige: 100, arcana: 40, attack: 30, defense: 35, deception: 30, insight: 40, investigation: 30, religion: 90, stealth: 20, survival: 40],
            academyMistress        : [llothsFavor: 75, ageInWeeks: 150 * 52, prestige: 50, arcana: 35, attack: 30, defense: 30, deception: 30, insight: 35, investigation: 25, religion: 80, stealth: 20, survival: 30],
            headPriestess          : [llothsFavor: 50, ageInWeeks: 150 * 52, prestige: 30, arcana: 30, attack: 30, defense: 30, deception: 25, insight: 30, investigation: 25, religion: 70, stealth: 15, survival: 30],
            highPriestess          : [llothsFavor: 30, ageInWeeks: 100 * 52, prestige: 15, arcana: 20, attack: 25, defense: 25, deception: 20, insight: 25, investigation: 20, religion: 60, stealth: 10, survival: 20],
            priestess              : [llothsFavor: 10, ageInWeeks: 70 * 52, prestige: 10, arcana: 10, attack: 20, defense: 20, deception: 15, insight: 20, investigation: 15, religion: 50, stealth: 5, survival: 10],
            acolyte                : [llothsFavor: 5, ageInWeeks: 40 * 52, prestige: 5, arcana: 0, attack: 10, defense: 10, deception: 10, insight: 10, investigation: 10, religion: 40, stealth: 0, survival: 0],
            archmage               : [llothsFavor: 40, ageInWeeks: 600 * 52, prestige: 75, arcana: 100, attack: 30, defense: 50, deception: 25, insight: 30, investigation: 40, religion: 50, stealth: 20, survival: 30],
            sorcereHeadMaster      : [llothsFavor: 30, ageInWeeks: 500 * 52, prestige: 75, arcana: 100, attack: 30, defense: 50, deception: 25, insight: 30, investigation: 40, religion: 50, stealth: 20, survival: 25],
            sorcereMaster          : [llothsFavor: 20, ageInWeeks: 300 * 52, prestige: 50, arcana: 90, attack: 25, defense: 40, deception: 20, insight: 25, investigation: 35, religion: 40, stealth: 15, survival: 20],
            sorcereProfessor       : [llothsFavor: 10, ageInWeeks: 250 * 52, prestige: 40, arcana: 80, attack: 20, defense: 30, deception: 15, insight: 20, investigation: 30, religion: 30, stealth: 10, survival: 15],
            headWizard             : [llothsFavor: 0, ageInWeeks: 200 * 52, prestige: 20, arcana: 70, attack: 20, defense: 30, deception: 15, insight: 20, investigation: 25, religion: 25, stealth: 10, survival: 10],
            journeymanWizard       : [llothsFavor: -5, ageInWeeks: 150 * 52, prestige: 10, arcana: 50, attack: 15, defense: 20, deception: 10, insight: 15, investigation: 20, religion: 20, stealth: 5, survival: 5],
            wizard                 : [llothsFavor: -10, ageInWeeks: 100 * 52, prestige: 5, arcana: 30, attack: 10, defense: 10, deception: 10, insight: 10, investigation: 15, religion: 15, stealth: 5, survival: 0],
            apprenticeWizard       : [llothsFavor: -15, ageInWeeks: 80 * 52, prestige: 0, arcana: 20, attack: 5, defense: 5, deception: 10, insight: 10, investigation: 10, religion: 10, stealth: 0, survival: 0],
            neophyte               : [llothsFavor: -20, ageInWeeks: 60 * 52, prestige: 0, arcana: 10, attack: 0, defense: 0, deception: 5, insight: 5, investigation: 5, religion: 5, stealth: 0, survival: 0],
            warMaster              : [llothsFavor: 50, ageInWeeks: 300 * 52, prestige: 100, arcana: 30, attack: 100, defense: 100, deception: 40, insight: 20, investigation: 30, religion: 10, stealth: 50, survival: 70],
            meleeMagthereHeadMaster: [llothsFavor: 40, ageInWeeks: 300 * 52, prestige: 90, arcana: 25, attack: 100, defense: 100, deception: 40, insight: 20, investigation: 25, religion: 10, stealth: 50, survival: 70],
            meleeMagthereMaster    : [llothsFavor: 30, ageInWeeks: 150 * 52, prestige: 70, arcana: 20, attack: 90, defense: 90, deception: 35, insight: 15, investigation: 20, religion: 10, stealth: 45, survival: 60],
            meleeMagthereInstructor: [llothsFavor: 20, ageInWeeks: 100 * 52, prestige: 50, arcana: 15, attack: 70, defense: 70, deception: 30, insight: 10, investigation: 15, religion: 10, stealth: 40, survival: 50],
            weaponMaster           : [llothsFavor: 20, ageInWeeks: 150 * 52, prestige: 50, arcana: 15, attack: 70, defense: 70, deception: 30, insight: 15, investigation: 20, religion: 10, stealth: 45, survival: 40],
            championWarrior        : [llothsFavor: 10, ageInWeeks: 125 * 52, prestige: 30, arcana: 10, attack: 60, defense: 60, deception: 25, insight: 10, investigation: 15, religion: 5, stealth: 40, survival: 30],
            soldier                : [llothsFavor: 0, ageInWeeks: 100 * 52, prestige: 10, arcana: 5, attack: 50, defense: 50, deception: 20, insight: 5, investigation: 10, religion: 5, stealth: 35, survival: 20],
            warrior                : [llothsFavor: -5, ageInWeeks: 60 * 52, prestige: 5, arcana: 5, attack: 40, defense: 40, deception: 15, insight: 5, investigation: 5, religion: 5, stealth: 30, survival: 10],
            conscript              : [llothsFavor: -10, ageInWeeks: 40 * 52, prestige: 0, arcana: 0, attack: 30, defense: 30, deception: 10, insight: 0, investigation: 0, religion: 0, stealth: 25, survival: 5],
            inquisitor             : [llothsFavor: 75, ageInWeeks: 300 * 52, prestige: 90, arcana: 30, attack: 80, defense: 90, deception: 0, insight: 30, investigation: 100, religion: 20, stealth: 100, survival: 60],
            spyMaster              : [llothsFavor: 65, ageInWeeks: 150 * 52, prestige: 40, arcana: 25, attack: 70, defense: 80, deception: 0, insight: 30, investigation: 90, religion: 15, stealth: 90, survival: 50],
            blackguard             : [llothsFavor: 55, ageInWeeks: 125 * 52, prestige: 20, arcana: 20, attack: 60, defense: 70, deception: 0, insight: 25, investigation: 80, religion: 10, stealth: 80, survival: 40],
            scoundrel              : [llothsFavor: 45, ageInWeeks: 100 * 52, prestige: 10, arcana: 10, attack: 50, defense: 60, deception: 0, insight: 20, investigation: 70, religion: 10, stealth: 70, survival: 30],
            hooligan               : [llothsFavor: 35, ageInWeeks: 60 * 52, prestige: 0, arcana: 5, attack: 40, defense: 50, deception: 0, insight: 15, investigation: 60, religion: 5, stealth: 60, survival: 20],
            lowLife                : [llothsFavor: 25, ageInWeeks: 40 * 52, prestige: 0, arcana: 0, attack: 30, defense: 40, deception: 0, insight: 10, investigation: 50, religion: 0, stealth: 50, survival: 10],
            child                  : [llothsFavor: 0, ageInWeeks: 20 * 52, prestige: 0, arcana: 0, attack: 0, defense: 0, deception: 0, insight: 0, investigation: 0, religion: 0, stealth: 0, survival: 0],
    ]

    static Individual createIndividual(Archetype archetype) {
        Individual individual = new Individual(archetype: archetype)
        if (archetype in [matron, headPriestess, highPriestess, priestess, acolyte]) {
            individual.gender = Gender.female
        } else if (archetype == child) {
            individual.gender = Gender.random()
        } else {
            individual.gender = Gender.male
        }

        return individual
    }
}
