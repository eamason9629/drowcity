package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.OperationStatus
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'operation')
class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne
    @JoinColumn(name = 'potentialOperationId', insertable = false, updatable = false)
    PotentialOperation potentialOperation

    @ManyToOne
    @JoinColumn(name = 'organizationId', insertable = false, updatable = false)
    Organization organization

    @ManyToOne
    @JoinColumn(name = 'operationLeadId', insertable = false, updatable = false)
    Individual operationLead

    //needs many to many table?
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    List<Individual> operationsTeam

    @OneToMany(mappedBy = 'operation', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<WorkerOperationAssignment> workerAssignments

    @Column(name = 'startWeek')
    int startWeek

    @Column(name = 'progress')
    int progress

    @Column(name = 'status')
    OperationStatus status

    //list of events caused by operation
    //a final result event
}
