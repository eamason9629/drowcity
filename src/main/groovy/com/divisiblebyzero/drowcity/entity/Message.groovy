package com.divisiblebyzero.drowcity.entity

import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'message')
class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'week')
    int week

    @JoinColumn(name = 'from_id')
    @ManyToOne(fetch = FetchType.LAZY)
    Individual from

    @JoinColumn(name = 'to_id')
    @ManyToOne(fetch = FetchType.LAZY)
    Individual to

    @Column(name = 'sent_anonymously')
    boolean sentAnonymously

    @Column(name = 'sent_publicly')
    boolean sentPublicly

    @Column(name = 'read_by_to')
    boolean readByTo

    @Column(name = 'read_by_gm')
    boolean readByGM

    @Column(name = 'subject')
    String subject

    @Column(name = 'message')
    String message
}
