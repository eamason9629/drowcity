package com.divisiblebyzero.drowcity.entity


import com.divisiblebyzero.drowcity.enums.WorkAssignmentType
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'workerPOIAssignment')
class WorkerPOIAssignment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne(cascade = CascadeType.MERGE)
    OrganizationToPointOfInterestRelationship poi

    @Enumerated(EnumType.STRING)
    @Column(name = 'workAssignmentType')
    WorkAssignmentType workAssignmentType

    @ManyToOne(cascade = CascadeType.MERGE)
    WorkerGroup workerGroup

    @Column(name = 'quantity')
    int quantity
}
