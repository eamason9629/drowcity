package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.ConsumptionModel
import com.divisiblebyzero.drowcity.enums.EmploymentType
import com.divisiblebyzero.drowcity.enums.WorkerType
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'worker_group')
class WorkerGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne
    @JoinColumn(name = 'employer_id')
    Organization employer

    @Column(name = 'worker_type')
    @Enumerated(EnumType.STRING)
    WorkerType workerType

    @Column(name = 'employment_type')
    @Enumerated(EnumType.STRING)
    EmploymentType employmentType

    @Column(name = 'consumption_model')
    @Enumerated(EnumType.STRING)
    ConsumptionModel consumptionModel

    @OrderBy('week DESC')
    @OneToMany(mappedBy = 'workerGroup', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<WorkerGroupLineItem> lineItems
}
