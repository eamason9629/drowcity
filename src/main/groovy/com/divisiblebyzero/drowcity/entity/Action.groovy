package com.divisiblebyzero.drowcity.entity


import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*

@Entity
@EqualsAndHashCode
@Table(name = 'action')
@ToString(excludes = ['progress'])
class Action {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @OrderBy('week DESC')
    @OneToMany(mappedBy = 'action', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<ActionProgress> progress
}
