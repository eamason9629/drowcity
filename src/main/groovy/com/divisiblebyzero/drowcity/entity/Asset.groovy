package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.AssetType
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*

@Entity
@EqualsAndHashCode
@Table(name = 'asset')
@ToString(excludes = ['lines'])
class Asset {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'type')
    @Enumerated(EnumType.STRING)
    AssetType type

    @OrderBy('week DESC')
    @OneToMany(mappedBy = 'asset', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<AssetLineItem> lines
}
