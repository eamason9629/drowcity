package com.divisiblebyzero.drowcity.entity

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*

@Entity
@ToString
@EqualsAndHashCode
@Table(name = 'actionProgress')
class ActionProgress {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne
    @JoinColumn(name = 'action_id')
    Action action

    @Column(name = 'week')
    int week
}
