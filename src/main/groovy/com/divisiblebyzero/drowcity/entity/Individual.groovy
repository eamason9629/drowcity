package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.Archetype
import com.divisiblebyzero.drowcity.enums.Gender
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*

@Entity
@ToString
@Table(name = 'individual')
@EqualsAndHashCode(excludes = ['relationships'])
class Individual extends AbstractEntity {
    @Column(name = 'gender')
    @Enumerated(EnumType.STRING)
    Gender gender

    @Column(name = 'archetype')
    @Enumerated(EnumType.STRING)
    Archetype archetype

    @OneToMany(mappedBy = 'from', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<IndividualRelationship> relationships
}
