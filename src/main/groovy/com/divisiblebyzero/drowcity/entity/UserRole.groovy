package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.UserRoleTypes
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'user_role')
class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'username')
    String username

    @Column(name = 'role')
    @Enumerated(EnumType.STRING)
    UserRoleTypes role
}
