package com.divisiblebyzero.drowcity.entity


import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'org_poi_rel')
class OrganizationToPointOfInterestRelationship {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = 'from_id', updatable = false)
    Organization from

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = 'to_id', updatable = false)
    PointOfInterest to

    @Column(name = 'influence')
    int influence

    @OneToMany(mappedBy = 'poi', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<WorkerPOIAssignment> assignments
}
