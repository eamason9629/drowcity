package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.WorkerGroupLineItemType
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'worker_group_line_item')
class WorkerGroupLineItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne
    @JoinColumn(name = 'worker_group_id')
    WorkerGroup workerGroup

    @Column(name = 'week')
    int week

    @Column(name = 'change')
    int change

    @Column(name = 'type')
    @Enumerated(EnumType.STRING)
    WorkerGroupLineItemType type
}
