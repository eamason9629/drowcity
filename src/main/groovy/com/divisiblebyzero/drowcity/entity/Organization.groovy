package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.OrganizationType
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*

@Entity
@Table(name = 'organization')
@ToString(excludes = ['members', 'workerGroups', 'compounds', 'player'])
@EqualsAndHashCode(excludes = ['members', 'workerGroups', 'compounds', 'player'])
class Organization extends AbstractEntity {
    @JoinColumn(name = 'player_id')
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    Player player

    @Column(name = 'type')
    @Enumerated(EnumType.STRING)
    OrganizationType type

    @OneToMany(mappedBy = 'from', cascade = CascadeType.ALL, orphanRemoval = true)
    List<OrganizationalMembershipRelationship> members

    @OneToMany(mappedBy = 'employer', cascade = CascadeType.ALL, orphanRemoval = true)
    List<WorkerGroup> workerGroups

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Compound> compounds

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Asset> assets
}
