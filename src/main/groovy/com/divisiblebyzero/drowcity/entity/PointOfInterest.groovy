package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.Region
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'pointOfInterest')
class PointOfInterest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'name')
    String name

    @Column(name = 'discoverability')
    int discoverability

    @Column(name = 'description')
    String description

    @Column(name = 'region')
    @Enumerated(EnumType.STRING)
    Region region

    @Column(name = 'isHabitable')
    boolean isHabitable

    @Column(name = 'llothsFavor')
    int llothsFavor

    @Column(name = 'prestige')
    int prestige

    @Column(name = 'arcana')
    int arcana

    @Column(name = 'defense')
    int defense

    @Column(name = 'deception')
    int deception

    @Column(name = 'insight')
    int insight

    @Column(name = 'investigation')
    int investigation

    @Column(name = 'religion')
    int religion

    @Column(name = 'stealth')
    int stealth

    @Column(name = 'survival')
    int survival

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Loot> loot
}
