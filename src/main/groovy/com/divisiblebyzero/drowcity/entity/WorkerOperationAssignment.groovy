package com.divisiblebyzero.drowcity.entity

import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'workerOperationAssignment')
class WorkerOperationAssignment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne(cascade = CascadeType.MERGE)
    Operation operation

    @ManyToOne(cascade = CascadeType.MERGE)
    WorkerGroup workerGroup

    @Column(name = 'quantity')
    int quantity
}
