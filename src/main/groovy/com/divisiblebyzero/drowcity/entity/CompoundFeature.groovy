package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.CompoundFeatureType
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'compound_feature')
class CompoundFeature {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne
    @JoinColumn(name = 'compound_id')
    Compound compound

    @Column(name = 'name')
    String name

    @Column(name = 'description')
    String description

    @Column(name = 'type')
    @Enumerated(EnumType.STRING)
    CompoundFeatureType type

    @Column(name = 'lloths_favor')
    int llothsFavor

    @Column(name = 'build_time_in_weeks')
    int remainingBuildTimeInWeeks

    @Column(name = 'prestige')
    int prestige

    @Column(name = 'arcana')
    int arcana

    @Column(name = 'attack')
    int attack

    @Column(name = 'defense')
    int defense

    @Column(name = 'deception')
    int deception

    @Column(name = 'insight')
    int insight

    @Column(name = 'investigation')
    int investigation

    @Column(name = 'religion')
    int religion

    @Column(name = 'stealth')
    int stealth

    @Column(name = 'survival')
    int survival
}
