package com.divisiblebyzero.drowcity.entity

import javax.persistence.*

@MappedSuperclass
abstract class AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne
    @JoinColumn(name = 'city_id')
    City city

    @Column(name = 'lloths_favor')
    int llothsFavor

    @Column(name = 'age_in_weeks')
    int ageInWeeks

    @Column(name = 'name')
    String name

    @Column(name = 'prestige')
    int prestige

    @Column(name = 'arcana')
    int arcana

    @Column(name = 'attack')
    int attack

    @Column(name = 'defense')
    int defense

    @Column(name = 'deception')
    int deception

    @Column(name = 'insight')
    int insight

    @Column(name = 'investigation')
    int investigation

    @Column(name = 'religion')
    int religion

    @Column(name = 'stealth')
    int stealth

    @Column(name = 'survival')
    int survival

//    List<Equipment> equipment
}
