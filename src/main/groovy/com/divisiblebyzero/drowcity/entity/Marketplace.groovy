package com.divisiblebyzero.drowcity.entity


import com.divisiblebyzero.drowcity.enums.AssetType
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'marketplace')
class Marketplace {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'name')
    String name

    @Column(name = 'description')
    String description

    @ManyToOne
    @JoinColumn(name = 'poi_id')
    PointOfInterest location

    @Enumerated(EnumType.STRING)
    @Column(name = 'allowedAssetTypes')
    @ElementCollection(targetClass = AssetType)
    @JoinTable(name = 'allowedAssetTypes', joinColumns = @JoinColumn(name = 'assetTypeID'))
    List<AssetType> allowedAssetTypes

    @Column(name = 'isBlackMarket')
    boolean isBlackMarket

    @OneToMany(mappedBy = 'marketplace', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<MarketplaceOffer> offers
}
