package com.divisiblebyzero.drowcity.entity

import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'city')
class City {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @JoinColumn(name = 'game_master_id')
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    Player gameMaster

    @Column(name = 'name', unique = true)
    String name

    @Column(name = 'week')
    int week

    @OneToMany(mappedBy = 'city', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<Organization> organizations

    @OneToMany(mappedBy = 'city', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<Compound> compounds
}
