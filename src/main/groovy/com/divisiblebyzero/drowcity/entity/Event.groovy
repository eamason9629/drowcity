package com.divisiblebyzero.drowcity.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = 'event')
class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'week')
    int week

    @Column(name = 'name')
    String name

    @Column(name = 'description')
    String description

    @ManyToOne
    PointOfInterest location

    //list or description of discoveries
    //list or description of intel gained
    //references to Asset/WorkerGroup line items

}
