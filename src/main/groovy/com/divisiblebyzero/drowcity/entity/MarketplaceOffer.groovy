package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.AssetType
import groovy.transform.Canonical

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Canonical
@Table(name = 'marketplaceOffer')
class MarketplaceOffer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'asset')
    AssetType asset

    @Column(name = 'quantity')
    int quantity

    @Column(name = 'minPrice')
    int minPrice

    @Column(name = 'weekMade')
    int weekMade

    @ManyToOne
    @JoinColumn(name = 'marketplace_id')
    Marketplace marketplace

    @ManyToOne
    @JoinColumn(name = 'seller_id')
    Organization seller

    @Column(name = 'isBlackMarketOffer')
    boolean isBlackMarketOffer

    @Column(name = 'deceptionScore')
    int deceptionScore
}
