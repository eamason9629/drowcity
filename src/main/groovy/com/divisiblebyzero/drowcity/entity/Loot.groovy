package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.AssetType
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'asset')
class Loot {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'type')
    @Enumerated(EnumType.STRING)
    AssetType type

    @Column(name = 'quantity')
    int quantity
}
