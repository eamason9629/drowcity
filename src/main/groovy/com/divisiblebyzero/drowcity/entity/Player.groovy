package com.divisiblebyzero.drowcity.entity

import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'player')
class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'username', unique = true)
    String username

    @Column(name = 'email', unique = true)
    String email

    @Column(name = 'password')
    String password

    @Column(name = 'first_name')
    String firstName

    @Column(name = 'last_name')
    String lastName

    @Column(name = 'enabled')
    boolean enabled = true

    @Column(name = 'lastLogin')
    String lastLogin

    @OneToMany(mappedBy = 'player', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<Organization> organizations
}
