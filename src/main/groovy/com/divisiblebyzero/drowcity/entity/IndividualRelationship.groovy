package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.IndividualRelationType
import com.divisiblebyzero.drowcity.enums.IndividualToIndividualStatus
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'individual_rel')
class IndividualRelationship {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne
    @JoinColumn(name = 'from_id', insertable = false, updatable = false)
    Individual from

    @ManyToOne
    @JoinColumn(name = 'to_id', insertable = false, updatable = false)
    Individual to

    @Column(name = 'type')
    @Enumerated(EnumType.STRING)
    IndividualRelationType type

    @Column(name = 'status')
    @Enumerated(EnumType.STRING)
    IndividualToIndividualStatus status
}
