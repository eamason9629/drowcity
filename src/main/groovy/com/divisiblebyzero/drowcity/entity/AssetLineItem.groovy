package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.AssetLineItemType
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'asset_line_item')
class AssetLineItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne
    @JoinColumn(name = 'asset_id')
    Asset asset

    @Column(name = 'week')
    int week

    @Column(name = 'change')
    int change

    @Column(name = 'type')
    @Enumerated(EnumType.STRING)
    AssetLineItemType type
}
