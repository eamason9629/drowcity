package com.divisiblebyzero.drowcity.entity

import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'compound')
class Compound {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne
    @JoinColumn(name = 'city_id')
    City city

    @ManyToOne
    @JoinColumn(name = 'owner_id')
    Organization owner

    @Column(name = 'name')
    String name

    @OneToMany(mappedBy = 'compound', cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    List<CompoundFeature> compoundFeatures
}
