package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.Archetype
import com.divisiblebyzero.drowcity.enums.WorkerType
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'potentialOperation')
class PotentialOperation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @Column(name = 'name')
    String name

    @Column(name = 'description')
    String description

    @Enumerated(EnumType.STRING)
    @Column(name = 'requiredArchetypes')
    @ElementCollection(targetClass = Archetype)
    @JoinTable(name = 'requiredArchetypes', joinColumns = @JoinColumn(name = 'archetypeID'))
    List<Archetype> requiredArchetypes

    @Column(name = 'requiredSkill')
    String requiredSkill

    @Column(name = 'requiredSkillLevel')
    int requiredSkillLevel

    @Column(name = 'difficulty')
    int difficulty

    @Enumerated(EnumType.STRING)
    @Column(name = 'workerGroupsAllowed')
    @ElementCollection(targetClass = WorkerType)
    @JoinTable(name = 'workerGroupsAllowed', joinColumns = @JoinColumn(name = 'workerTypeID'))
    List<WorkerType> workerGroupsAllowed

    @Enumerated(EnumType.STRING)
    @Column(name = 'archetypesAllowed')
    @ElementCollection(targetClass = Archetype)
    @JoinTable(name = 'archetypesAllowed', joinColumns = @JoinColumn(name = 'archetypesID'))
    List<Archetype> archetypesAllowed

    @Column(name = 'isPausable')
    boolean isPausable

    @Column(name = 'isPerpetual')
    boolean isPerpetual

    @Column(name = 'timetable')
    int timetable
}
