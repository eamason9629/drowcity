package com.divisiblebyzero.drowcity.entity

import com.divisiblebyzero.drowcity.enums.Loyalty
import com.divisiblebyzero.drowcity.enums.OrganizationalRelationshipStatus
import groovy.transform.Canonical

import javax.persistence.*

@Entity
@Canonical
@Table(name = 'org_individual_rel')
class OrganizationalMembershipRelationship {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = 'from_id', updatable = false)
    Organization from

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = 'to_id', updatable = false)
    Individual to

    @Column(name = 'status')
    @Enumerated(EnumType.STRING)
    OrganizationalRelationshipStatus status

    @Column(name = 'loyalty')
    @Enumerated(EnumType.STRING)
    Loyalty loyalty = Loyalty.indifferent
}
