package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.entity.OrganizationalMembershipRelationship
import com.divisiblebyzero.drowcity.enums.OrganizationType
import com.divisiblebyzero.drowcity.enums.OrganizationalRelationshipStatus
import org.aspectj.weaver.patterns.OrTypePattern

import static com.divisiblebyzero.drowcity.enums.OrganizationalRelationshipStatus.matron

class HouseDashboardDTO {
    String id
    String name
    String cityName
    String matronName
    int week
    //List<WeeklyReportSummaryDTO> reports
    List<MessageDTO> messages
    List<AssetSummaryDTO> assets
    List<StaffSummaryDTO> staff
    List<NobleSummaryDTO> nobles
    //List<OperationSummaryDTO> operations
    //List<EventSummaryDTO> events

    static HouseDashboardDTO create(Organization organization) {
        assert organization.type == OrganizationType.house, "HouseDashboardDTO given a $organization.type in the create method."
        new HouseDashboardDTO(
                id: organization.id,
                name: organization.name,
                cityName: organization.city.name,
                matronName: organization.members.find { it.status == matron }?.to?.name,
                week: organization.city.week,
                assets: AssetSummaryDTO.create(organization.assets),
                staff: StaffSummaryDTO.create(organization.workerGroups),
                nobles: NobleSummaryDTO.create(organization.members)
        )
    }

    static List<HouseDashboardDTO> create(List<Organization> organizations) {
        organizations.findAll { Organization it -> it.type == OrganizationType.house }.collect {Organization it ->
            create(it)
        }
    }
}
