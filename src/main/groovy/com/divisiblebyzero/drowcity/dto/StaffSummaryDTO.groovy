package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.WorkerGroup
import com.divisiblebyzero.drowcity.entity.WorkerGroupLineItem

class StaffSummaryDTO {
    String id
    String workerType
    String employmentType
    String consumptionModel
    int currentCount
    int lastChangeAmount

    static StaffSummaryDTO create(WorkerGroup group) {
        int curCount = 0
        group.lineItems.each { WorkerGroupLineItem it ->
            curCount += it.change
        }
        new StaffSummaryDTO(
                id: group.id.toString(),
                workerType: group.workerType.prettyName,
                employmentType: group.employmentType.prettyName,
                consumptionModel: group.consumptionModel.prettyName,
                currentCount: curCount,
                lastChangeAmount: group.lineItems.last().change
        )
    }

    static List<StaffSummaryDTO> create(List<WorkerGroup> workerGroups) {
        workerGroups.collect { WorkerGroup it ->
            create(it)
        }
    }
}
