package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.Player
import groovy.transform.Canonical

@Canonical
class PlayerDTO {
    UUID id
    String username
    String firstName
    String lastName
    boolean enabled

    static PlayerDTO create(Player player) {
        return new PlayerDTO(id: player.id, username: player.username, firstName: player.firstName, lastName: player.lastName, enabled: player.enabled)
    }
}
