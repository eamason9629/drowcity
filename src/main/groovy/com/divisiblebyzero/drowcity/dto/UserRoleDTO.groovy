package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.UserRole

class UserRoleDTO {
    UUID id
    String username
    String role

    static UserRoleDTO create(UserRole role) {
        return new UserRoleDTO(id: role.id, username: role.username, role: role.role)
    }

    static List<UserRoleDTO> create(List<UserRole> roles) {
        roles.collect { UserRole it ->
            create(it)
        }
    }
}
