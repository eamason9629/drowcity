package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.OrganizationalMembershipRelationship
import groovy.transform.Canonical

@Canonical
class NobleSummaryDTO {
    String id
    String name
    String status
    String gender

    static NobleSummaryDTO create(OrganizationalMembershipRelationship relationship) {
        new NobleSummaryDTO(
                id: relationship.to.id.toString(),
                name: relationship.to.name,
                status: relationship.status.prettyName,
                gender: relationship.to.gender.prettyName
        )
    }

    static List<NobleSummaryDTO> create(List<OrganizationalMembershipRelationship> membershipRelationships) {
        membershipRelationships.collect { OrganizationalMembershipRelationship it ->
            create(it)
        }
    }
}
