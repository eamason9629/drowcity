package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.WorkerGroup

class WorkerGroupDTO {
    String id
    String employerName
    String workerType
    String employmentType
    String consumptionModel
    int currentCount
    List<WorkerGroupLineItemDTO> lineItems

    static WorkerGroupDTO create(WorkerGroup group) {
        int currentCount = 0
        group.lineItems.each {
            currentCount += it.change
        }
        new WorkerGroupDTO(
                id: group.id.toString(),
                employerName: group.employer.name,
                workerType: group.workerType.prettyName,
                employmentType: group.employmentType.prettyName,
                consumptionModel: group.consumptionModel.prettyName,
                currentCount: currentCount,
                lineItems: WorkerGroupLineItemDTO.create(group.lineItems)
        )
    }
}
