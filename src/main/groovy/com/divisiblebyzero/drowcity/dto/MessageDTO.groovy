package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.Individual
import com.divisiblebyzero.drowcity.entity.Message

class MessageDTO {
    String id
    int week
    String from
    String fromName
    String to
    String toName
    boolean sentAnonymously
    boolean sentPublicly
    boolean readByTo
    String subject
    String message

    static MessageDTO create(Message theMessage) {
        return new MessageDTO(
                id: theMessage.id.toString(),
                week: theMessage.week,
                from: theMessage.from.id.toString(),
                fromName: theMessage.from.name,
                to: theMessage.to.id.toString(),
                toName: theMessage.to.name,
                sentAnonymously: theMessage.sentAnonymously,
                sentPublicly: theMessage.sentPublicly,
                readByTo: theMessage.readByTo,
                subject: theMessage.subject,
                message: theMessage.message
        )
    }

    static List<MessageDTO> create(List<Message> messages) {
        messages.collect { Message it ->
            create(it)
        }
    }

    static Message create(MessageDTO messageDTO, Individual from, Individual to) {
        return new Message(
                from: from,
                to: to,
                sentAnonymously: messageDTO.sentAnonymously,
                sentPublicly: messageDTO.sentPublicly,
                subject: messageDTO.subject,
                message: messageDTO.message
        )
    }
}
