package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.Player

class UserDashboardDTO {
    String username
    String email
    String firstName
    String lastName
    String lastLogin
    List<SimpleHouseDTO> houses

    static UserDashboardDTO create(Player player) {
        new UserDashboardDTO(
                username: player.username,
                email: player.email,
                firstName: player.firstName,
                lastName: player.lastName,
                lastLogin: player.lastLogin,
                houses: SimpleHouseDTO.create(player.organizations)
        )
    }
}
