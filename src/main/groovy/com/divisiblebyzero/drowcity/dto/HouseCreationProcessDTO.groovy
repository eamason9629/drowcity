package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.enums.*
import com.divisiblebyzero.drowcity.util.NameGenerator

import static com.divisiblebyzero.drowcity.util.EnumerateHelper.createFrom

class HouseCreationProcessDTO {
    float remainingPoints
    NewHouseDTO newHouse = new NewHouseDTO()
    List<HouseCreationCompoundFeatureDTO> compoundFeatures = []
    List<String> recommendations = []
    List<NewIndividualDTO> nobles = [
            new NewIndividualDTO(name: NameGenerator.femaleName(), gender: Gender.female, archetype: Archetype.matron, relationshipToMatron: IndividualRelationType.none)
    ]
    Map<String, EnumDTO> enums = [
            (Archetype.simpleName)             : createFrom(Archetype),
            (CompoundFeatureType.simpleName)   : createFrom(CompoundFeatureType),
            (FamilyTrait.simpleName)           : createFrom(FamilyTrait),
            (Gender.simpleName)                : createFrom(Gender),
            (IndividualRelationType.simpleName): createFrom(IndividualRelationType),
    ]
}
