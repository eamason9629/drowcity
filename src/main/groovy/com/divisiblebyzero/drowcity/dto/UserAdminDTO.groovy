package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.enums.UserRoleTypes

class UserAdminDTO {
    List<PlayerAndRolesDTO> players = []
    String[] allRoles = UserRoleTypes.values()
}
