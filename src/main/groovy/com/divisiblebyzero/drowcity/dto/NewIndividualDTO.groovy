package com.divisiblebyzero.drowcity.dto


import com.divisiblebyzero.drowcity.enums.Archetype
import com.divisiblebyzero.drowcity.enums.Gender
import com.divisiblebyzero.drowcity.enums.IndividualRelationType
import groovy.transform.Canonical

@Canonical
class NewIndividualDTO {
    String name
    Gender gender
    Archetype archetype
    IndividualRelationType relationshipToMatron

    int llothsFavor
    int arcana
    int attack
    int defense
    int deception
    int insight
    int investigation
    int religion
    int stealth
    int survival
}
