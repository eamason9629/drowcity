package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.entity.UserRole
import com.divisiblebyzero.drowcity.enums.UserRoleTypes

class PlayerAndRolesDTO extends PlayerDTO {
    Map<String, Boolean> roles = [:]

    static Map<String, Boolean> mapFromRoles(List<UserRole> roles) {
        Map result = [:]
        UserRoleTypes.values().each { UserRoleTypes roleType ->
            result[roleType.toString()] = roles.find { UserRole role ->
                role.role == roleType
            } ? true : false
        }
        return result
    }

    static PlayerAndRolesDTO create(Player player, List<UserRole> roles) {
        new PlayerAndRolesDTO(
                id: player.id,
                username: player.username,
                firstName: player.firstName,
                lastName: player.lastName,
                enabled: player.enabled,
                roles: mapFromRoles(roles)
        )
    }
}
