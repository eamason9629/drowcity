package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.Asset
import com.divisiblebyzero.drowcity.entity.AssetLineItem

class AssetSummaryDTO {
    String id
    int currentCount
    String type
    String typePrettyName
    int lastChange

    static List<AssetSummaryDTO> create(List<Asset> assets) {
        assets.collect { create(it)}
    }

    static AssetSummaryDTO create(Asset asset) {
        int curCount = 0
        asset.lines.each { AssetLineItem it ->
            curCount += it.change
        }
        AssetSummaryDTO result = new AssetSummaryDTO(
                id: asset.id.toString(),
                currentCount: curCount,
                type: asset.type.toString(),
                typePrettyName: asset.type.prettyName
        )
        if(asset.lines.size() > 0) {
            result.lastChange = asset.lines.last().change
        }
        return result
    }
}
