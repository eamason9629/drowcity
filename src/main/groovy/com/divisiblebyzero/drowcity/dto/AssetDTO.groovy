package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.Asset

class AssetDTO {
    String id
    String ownerName
    String type
    int currentCount
    List<AssetLineItemDTO> lines

    static AssetDTO create(Asset asset) {
        int currentCount = 0
        asset.lines.each {
            currentCount += it.change
        }
        new AssetDTO(
                id: asset.id.toString(),
                currentCount: currentCount,
                type: asset.type.toString(),
                lines: AssetLineItemDTO.create(asset.lines)
        )
    }
}
