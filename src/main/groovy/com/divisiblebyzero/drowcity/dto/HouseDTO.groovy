package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.entity.OrganizationalMembershipRelationship
import com.divisiblebyzero.drowcity.enums.OrganizationType
import com.divisiblebyzero.drowcity.enums.OrganizationalRelationshipStatus

class HouseDTO {
    String id
    String name
    String cityName
    int week
    List<MessageDTO> messages

    NobleSummaryDTO matron
    List<StaffSummaryDTO> workerGroups
    List<NobleSummaryDTO> members
    List<AssetSummaryDTO> assets

    static HouseDTO create(Organization organization) {
        assert organization.type == OrganizationType.house
        NobleSummaryDTO matron = NobleSummaryDTO.create(organization.members.find { OrganizationalMembershipRelationship it ->
            it.status == OrganizationalRelationshipStatus.matron
        })
        new HouseDTO(
                id: organization.id.toString(),
                name: organization.name,
                cityName: organization.city.name,
                week: organization.city.week,
                matron: matron,
                workerGroups: StaffSummaryDTO.create(organization.workerGroups),
                members: NobleSummaryDTO.create(organization.members).findAll {
                    return it.status != OrganizationalRelationshipStatus.matron.prettyName
                },
                assets: AssetSummaryDTO.create(organization.assets),
        )
    }

    static List<HouseDTO> create(List<Organization> organizations) {
        organizations.findAll { Organization it -> it.type == OrganizationType.house }.collect {Organization it ->
            create(it)
        }
    }
}
