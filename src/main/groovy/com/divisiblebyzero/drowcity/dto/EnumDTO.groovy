package com.divisiblebyzero.drowcity.dto

import groovy.transform.Canonical

@Canonical
class EnumDTO {
    String name
    Map<String, Map<String, Object>> values = [:]
}