package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.WorkerGroupLineItem

class WorkerGroupLineItemDTO {
    String id
    String workerGroup
    int week
    int change
    String type

    static WorkerGroupLineItemDTO create(WorkerGroupLineItem lineItem) {
        new WorkerGroupLineItemDTO(
                id: lineItem.id.toString(),
                workerGroup: lineItem.workerGroup.workerType.prettyName,
                week: lineItem.week,
                change: lineItem.change,
                type: lineItem.type.prettyName
        )
    }

    static List<WorkerGroupLineItemDTO> create(List<WorkerGroupLineItem> lineItems) {
        lineItems.collect { WorkerGroupLineItem it ->
            create(it)
        }
    }
}
