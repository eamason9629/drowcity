package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.enums.FamilyTrait
import com.divisiblebyzero.drowcity.util.NameGenerator
import groovy.transform.Canonical

@Canonical
class NewHouseDTO {
    String name = NameGenerator.houseName()
    FamilyTrait familyTrait = FamilyTrait.none
    int gold = 0

    int rotheFarmBid = 0
    int adamantiumMineBid = 0
    int waterSupplyBid = 0
    int fishmongeringBid = 0
    int lizardFarmingBid = 0
    int mossFarmingBid = 0
    int mushroomFarmingBid = 0
    int mortuaryBid = 0
    int garbageDumpBid = 0
    int arenaBid = 0

    int soldierCount = 0
    int priestessCount = 0
    int wizardCount = 0
    int staffCount = 0
    int orcCount = 0
    int goblinCount = 0
    int koboldCount = 0
    int duergarCount = 0
    int dwarfCount = 0
    int gnomeCount = 0
    int minotaurCount = 0
    int beholderCount = 0
    int illithidCount = 0
    int kuoToaCount = 0
    int bugbearCount = 0
    int lamiaCount = 0
    int nightmareCount = 0
    int zombieCount = 0
    int skeletonCount = 0
    int driderCount = 0
}
