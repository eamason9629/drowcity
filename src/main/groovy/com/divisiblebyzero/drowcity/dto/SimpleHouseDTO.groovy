package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.entity.OrganizationalMembershipRelationship
import com.divisiblebyzero.drowcity.enums.OrganizationType
import com.divisiblebyzero.drowcity.enums.OrganizationalRelationshipStatus
import groovy.transform.Canonical

@Canonical
class SimpleHouseDTO {
    String id
    String name
    String cityName
    String matronName
    int week

    static SimpleHouseDTO create(Organization organization) {
        new SimpleHouseDTO(
                id: organization.id.toString(),
                name: organization.name,
                cityName: organization.city.name,
                matronName: organization.members.find {
                    it.status == OrganizationalRelationshipStatus.matron
                }?.to?.name,
                week: organization.city.week
        )
    }

    static List<SimpleHouseDTO> create(List<Organization> organizations) {
        organizations.findAll { Organization it -> it.type == OrganizationType.house }.collect {Organization it ->
            create(it)
        }
    }
}
