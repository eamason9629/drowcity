package com.divisiblebyzero.drowcity.dto


import com.divisiblebyzero.drowcity.enums.CompoundFeatureType

class HouseCreationCompoundFeatureDTO {
    int quantity = 0
    CompoundFeatureType compoundFeature
}
