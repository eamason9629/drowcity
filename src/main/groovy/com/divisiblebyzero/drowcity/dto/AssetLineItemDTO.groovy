package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.AssetLineItem

class AssetLineItemDTO {
    String id
    int week
    int change
    String type

    static AssetLineItemDTO create(AssetLineItem lineItem) {
        new AssetLineItemDTO(
                id: lineItem.id.toString(),
                week: lineItem.week,
                change: lineItem.change,
                type: lineItem.type.prettyName
        )
    }

    static List<AssetLineItemDTO> create(List<AssetLineItem> lineItems) {
        lineItems.collect {
            create(it)
        }
    }
}
