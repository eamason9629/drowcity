package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.enums.EmploymentType
import com.divisiblebyzero.drowcity.enums.WorkerType

class NewWorkerGroupDTO {
    WorkerType workerType
    EmploymentType employmentType
    int amount
}
