package com.divisiblebyzero.drowcity.dto

import com.divisiblebyzero.drowcity.entity.City
import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.enums.OrganizationType
import groovy.transform.Canonical

@Canonical
class CityDTO {
    UUID id
    String name
    int playerCount
    int houseCount
    int compoundCount

    static CityDTO create(City city) {
        new CityDTO(id: city.id, name: city.name,
                playerCount: city.organizations.count { Organization it ->
                    it.player != null
                },
                houseCount: city.organizations.count { Organization it ->
                    it.type == OrganizationType.house
                },
                compoundCount: city.compounds.size()
        )
    }

    static List<CityDTO> create(List<City> cities) {
        cities.collect {
            create(it)
        }
    }
}