package com.divisiblebyzero.drowcity.config

import com.zaxxer.hikari.HikariDataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

import javax.sql.DataSource
import java.time.Duration

@Configuration
@EntityScan(basePackages = ['com.divisiblebyzero.drowcity.entity'])
@EnableJpaRepositories(basePackages = ['com.divisiblebyzero.drowcity.repository'])
class DatabaseConfig {
    @Autowired
    Environment env

    @Bean
    Properties envHibernateProperties() {
        Properties hibernateProperties = new Properties()
        ['hibernate.hbm2ddl.auto', 'hibernate.dialect'].each { key ->
            String value = env.getProperty(key)
            if (value) {
                hibernateProperties.put(key, value)
            }
        }

        hibernateProperties.setProperty('org.hibernate.envers.do_not_audit_optimistic_locking_field', true.toString())
        hibernateProperties.setProperty('org.hibernate.envers.revision_on_collection_change', false.toString())
        hibernateProperties.setProperty('org.hibernate.envers.store_data_at_delete', true.toString())
        hibernateProperties.setProperty('hibernate.create_empty_composites.enabled', true.toString())
        hibernateProperties.setProperty('hibernate.allow_refresh_detached_entity', true.toString())
        hibernateProperties.setProperty('hibernate.jdbc.fetch_size', '100')
        return hibernateProperties
    }

    @Bean
    DataSource dataSource() {
        HikariDataSource source = new HikariDataSource()
        source.setDriverClassName(env.getProperty('datasource.driver.class.name'))
        source.setJdbcUrl(env.getProperty('datasource.jdbc.url'))
        source.setMinimumIdle(env.getProperty('database.pool.size.min').toInteger())
        source.setMaximumPoolSize(env.getProperty('database.pool.size.max').toInteger())
        source.registerMbeans = true
        source.setPoolName('DrowCity-Hikari')
        boolean databaseKeepAliveEnabled = env.getProperty('database.keep.alive.enable').toBoolean()
        if (databaseKeepAliveEnabled) {
            int databaseKeepAlivePeriodMinutes = env.getProperty('database.keep.alive.period.minutes').toInteger()
            source.connectionTestQuery = 'select 1 from dual'
            source.idleTimeout = Duration.ofMinutes(databaseKeepAlivePeriodMinutes).toMillis()
        }
        return source
    }
}
