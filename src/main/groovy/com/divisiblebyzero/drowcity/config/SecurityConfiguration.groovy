package com.divisiblebyzero.drowcity.config

import com.divisiblebyzero.drowcity.security.AuthenticationSuccessHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

import javax.sql.DataSource

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = ['com.divisiblebyzero.drowcity.security'])
class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    DataSource dataSource

    @Autowired
    AuthenticationSuccessHandler authenticationSuccessHandler

    @Bean
    BCryptPasswordEncoder passwordEncoder() {
        new BCryptPasswordEncoder()
    }

    @Autowired
    void configAuthentication(AuthenticationManagerBuilder authBuilder) throws Exception {
        authBuilder.jdbcAuthentication().passwordEncoder(passwordEncoder())
                .dataSource(dataSource)
                .usersByUsernameQuery('select username, password, enabled from player where username=?')
                .authoritiesByUsernameQuery('select username, role from user_role where username=?')
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers('/**').permitAll()
            .and()
                .formLogin()
                .loginPage('/')
                .loginProcessingUrl('/login')
                .defaultSuccessUrl('/')
                .successHandler(authenticationSuccessHandler)
            .and()
                .logout()
                .logoutUrl('/logout')
                .logoutSuccessUrl('/')
                .deleteCookies('JSESSIONID')
    }
}
