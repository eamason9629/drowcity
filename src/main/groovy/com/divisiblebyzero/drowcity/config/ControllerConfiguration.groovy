package com.divisiblebyzero.drowcity.config


import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL
import static org.springframework.http.MediaType.APPLICATION_JSON

@Configuration
@ComponentScan(basePackages = ['com.divisiblebyzero.drowcity.controller', 'com.divisiblebyzero.drowcity.interceptor'])
class ControllerConfiguration implements WebMvcConfigurer {
    @Bean
    ObjectMapper objectMapper() {
        return new ObjectMapper(new JsonFactory()).setSerializationInclusion(NON_NULL)
    }

    @Override
    void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter()
        converter.setSupportedMediaTypes([APPLICATION_JSON])
        converter.objectMapper = objectMapper()
        converters.add(converter)
        converters.add(new StringHttpMessageConverter())
    }
}
