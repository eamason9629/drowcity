package com.divisiblebyzero.drowcity.config

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ['com.divisiblebyzero.drowcity.service'])
class ServiceConfiguration {
}
