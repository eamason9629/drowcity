package com.divisiblebyzero.drowcity.config


import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DrowCityApplication {
	static void main(String[] args) {
		SpringApplication.run(DrowCityApplication, args)
	}
}
