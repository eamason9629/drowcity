package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.entity.Marketplace
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'marketplace', path = 'marketplace')
interface MarketplaceRepository extends PagingAndSortingRepository<Marketplace, UUID> {}