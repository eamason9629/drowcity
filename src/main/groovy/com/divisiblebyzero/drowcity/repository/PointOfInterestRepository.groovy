package com.divisiblebyzero.drowcity.repository


import com.divisiblebyzero.drowcity.entity.PointOfInterest
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'pointOfInterest', path = 'pointOfInterest')
interface PointOfInterestRepository extends PagingAndSortingRepository<PointOfInterest, UUID> {
    PointOfInterest findByName(String name)
}