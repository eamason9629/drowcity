package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.entity.Player
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'players', path = 'players')
interface PlayerRepository extends PagingAndSortingRepository<Player, UUID> {
    Player findByUsername(String username)
}
