package com.divisiblebyzero.drowcity.repository


import com.divisiblebyzero.drowcity.entity.Loot
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'loot', path = 'loot')
interface LootRepository extends PagingAndSortingRepository<Loot, UUID> {}
