package com.divisiblebyzero.drowcity.repository


import com.divisiblebyzero.drowcity.entity.WorkerGroupLineItem
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'workerGroupLineItems', path = 'workerGroupLineItems')
interface WorkerGroupLineItemRepository extends PagingAndSortingRepository<WorkerGroupLineItem, UUID> {}