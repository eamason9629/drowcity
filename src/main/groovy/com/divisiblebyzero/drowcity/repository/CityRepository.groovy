package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.entity.City
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'cities', path = 'cities')
interface CityRepository extends PagingAndSortingRepository<City, UUID> {
    City findByName(String name)
}
