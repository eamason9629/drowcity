package com.divisiblebyzero.drowcity.repository


import com.divisiblebyzero.drowcity.entity.PotentialOperation
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'potentialOperation', path = 'potentialOperation')
interface PotentialOperationRepository extends PagingAndSortingRepository<PotentialOperation, UUID> {}