package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.entity.City
import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.enums.OrganizationType
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'organizations', path = 'organizations')
interface OrganizationRepository extends PagingAndSortingRepository<Organization, UUID> {
    List<Organization> findByPlayerAndType(Player player, OrganizationType organizationType)
    List<Organization> findByType(OrganizationType organizationType)
    List<Organization> findByCity(City city)
}