package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.entity.Individual
import com.divisiblebyzero.drowcity.entity.Message
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'messages', path = 'messages')
interface MessageRepository extends PagingAndSortingRepository<Message, UUID> {
    List<Message> findByTo(Individual to)
    List<Message> findByToAndReadByToTrue(Individual to)
}
