package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.entity.UserRole
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'roles', path = 'roles')
interface UserRoleRepository extends PagingAndSortingRepository<UserRole, UUID> {
    List<UserRole> findAllByUsername(String username)
}