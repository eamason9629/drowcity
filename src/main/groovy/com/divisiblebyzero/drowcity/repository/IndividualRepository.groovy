package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.entity.Individual
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'individuals', path = 'individuals')
interface IndividualRepository extends PagingAndSortingRepository<Individual, UUID> {}