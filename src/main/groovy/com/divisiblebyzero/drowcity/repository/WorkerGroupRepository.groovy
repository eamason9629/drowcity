package com.divisiblebyzero.drowcity.repository


import com.divisiblebyzero.drowcity.entity.WorkerGroup
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'workerGroups', path = 'workerGroups')
interface WorkerGroupRepository extends PagingAndSortingRepository<WorkerGroup, UUID> {}