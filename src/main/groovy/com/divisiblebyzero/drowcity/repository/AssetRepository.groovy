package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.entity.Asset
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'assets', path = 'assets')
interface AssetRepository extends PagingAndSortingRepository<Asset, UUID> {}