package com.divisiblebyzero.drowcity.repository

import com.divisiblebyzero.drowcity.entity.AssetLineItem
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = 'assetLineItems', path = 'assetLineItems')
interface AssetLineItemRepository extends PagingAndSortingRepository<AssetLineItem, UUID> {}
