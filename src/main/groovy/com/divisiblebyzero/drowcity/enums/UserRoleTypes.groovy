package com.divisiblebyzero.drowcity.enums

enum UserRoleTypes {
    generic,
    notLoggedIn,
    userAdmin
}
