package com.divisiblebyzero.drowcity.enums

enum AssetType {
    gold('Gold', 'Gold Pieces, commonly used as currency across the Forgotten Realms and beyond.'),
    rothe('Rothe', 'A form of large subterranean cattle.'),
    mushrooms('Mushrooms', 'An edible form of mushroom.'),
    fish('Fish', 'A variety of subterranean fish.'),
    moss('Moss', 'An edible form of moss.'),
    water('Water', "It's water."),
    weapons('Weapons', "A weapon of some sort. Who cares, it's for a commoner to use."),
    armor('Armor', "Armor of some sort. Who cares, it's for a commoner to use."),
    lizards('Lizards', 'A lizard mount.'),
    potions('Potions', 'A potion of some sort, normally just sold off to the market for cash.'),
    adamantium('Adamantium', 'Raw Adamantium Ore.'),
    mithril('Mithril', 'Raw Mithril Ore.'),

    final String prettyName
    final String description

    AssetType(String prettyName, String description) {
        this.prettyName = prettyName
        this.description = description
    }
}