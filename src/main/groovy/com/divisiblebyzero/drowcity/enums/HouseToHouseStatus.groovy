package com.divisiblebyzero.drowcity.enums

enum HouseToHouseStatus {
    alliance('Alliance'),
    nonAggression('Non-aggression Agreement'),
    neutral('Neutral'),
    coldWar('Cold War'),
    openWar('Open War')

    final String prettyName

    HouseToHouseStatus(String prettyName) {
        this.prettyName = prettyName
    }
}