package com.divisiblebyzero.drowcity.enums

enum WorkerGroupLineItemType {
    hire('Hired', 'Hired to work.'),
    capture('Captured', 'Captured prisoners force to work.'),
    birth('Born', 'Born into servitude.'),
    sacrifice('Sacrificed', 'Sacrificed.'),
    escape('Escaped', 'Escaped from our house.'),
    quit('Quit', 'Quit their position from our employment.'),
    death('Died', 'Died during service.')

    final String prettyName
    final String description

    WorkerGroupLineItemType(String prettyName, String description) {
        this.prettyName = prettyName
        this.description = description
    }
}