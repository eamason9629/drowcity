package com.divisiblebyzero.drowcity.enums

enum DieType {
    d1,
    d2,
    d3,
    d4,
    d6,
    d8,
    d10,
    d12,
    d18,
    d20,
    d30,
    d50,
    d100

    public int maxValue() {
        switch (this) {
            case d1: return 1
            case d2: return 2
            case d3: return 3
            case d4: return 4
            case d6: return 6
            case d8: return 8
            case d10: return 10
            case d12: return 12
            case d18: return 18
            case d20: return 20
            case d30: return 30
            case d50: return 50
            case d100: return 100
            default: 6
        }
    }
}