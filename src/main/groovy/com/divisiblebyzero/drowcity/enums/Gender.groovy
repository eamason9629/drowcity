package com.divisiblebyzero.drowcity.enums

import com.divisiblebyzero.drowcity.util.DieRoller

enum Gender {
    male('Male', 'The lesser gender of the species.', 0),
    female('Female', 'The supreme gender of the species.', 100)

    final String prettyName
    final String description
    final int creationCost

    Gender(String prettyName, String description, int creationCost) {
        this.prettyName = prettyName
        this.description = description
        this.creationCost = creationCost
    }

    static Gender random() {
        int roll = DieRoller.rollDice(1, DieType.d100)
        //90% chance of male
        return (roll <= 75) ? male : female
    }
}