package com.divisiblebyzero.drowcity.enums

enum FamilyTrait {
    heretical('Heretical', 'Your family does not worship Lloth, though you hide that fact from the rest of the city very well.', -1000),
    industrious('Industrious', 'Your family likes building stuff.', 250),
    infernalTies('Infernal Ties', 'Your family has ties with creatures of the Abyss.', 100),
    llothFanatic('Lloth Fanatic', 'Your family fervently worships Lloth, even the most holy of houses admire your tenacity.', 500),
    lucky('Lucky', 'Your family has always been luckier than most.', 1000),
    lycanthrope('Lycanthrope', 'Your family is cursed with some type of Lycanthropy.', -400),
    magicResistant('Magic Resistant', 'Your family is somehow resistant to most magics.', 200),
    medium('Medium', 'Your family comes from a long line of people who can speak with the dead.', -100),
    necromantic('Necromantic', 'Your family dabbles in the necromantic arts. Fortunately, Lloth does not ban this practice. Unfortunately, other houses do not look favorably on this trait.', -100),
    none('None', 'Your family is not special in any way.', 0),
    overconfident('Overconfident', 'The confidence your family possesses is second to none.  Whether this confidence is earned or not remains to be seen.', -150),
    paranoid('Paranoid', 'Paranoia runs rampant in your family. Some other house must have placed a curse on your family.', -150),
    psionicists('Psionicists', 'Your family is able to use the psionic arts and does not have to rely on magic. Other houses, in their jealousy, distrust this ability and fear it.', 2500),
    secretive('Secretive', 'Your family is very secretive, especially for drow. You all keep many secrets, even from each other.', -250),
    surfaceTies('Surface Ties', 'Your family has ties or connections with the surface world.', 100),
    vengeful('Vengeful', 'Your family vows vengeance against others for even the slightest sleights.', -150)

    final String prettyName
    final String description
    final int creationCost

    FamilyTrait(String prettyName, String description, int creationCost) {
        this.prettyName = prettyName
        this.description = description
        this.creationCost = creationCost
    }
}

