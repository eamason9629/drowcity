package com.divisiblebyzero.drowcity.enums

enum EmploymentType {
    forced('Forced Labor', 'Workers are forced to work without pay.'),
    paid('Paid Labor', 'Workers are paid for their time.'),
    volunteer('Volunteer Labor', 'Workers volunteer to work without pay.')

    final String prettyName
    final String description

    EmploymentType(String prettyName, String description) {
        this.prettyName = prettyName
        this.description = description
    }
}