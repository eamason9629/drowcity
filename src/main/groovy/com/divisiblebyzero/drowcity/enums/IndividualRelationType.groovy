package com.divisiblebyzero.drowcity.enums

enum IndividualRelationType {
    acquaintance('Acquaintance', 'Just an acquaintance.'),
    child('Child', 'One of the children.'),
    cousin('Cousin', 'A cousin.'),
    father('Father', 'The father.'),
    lover('Lover', 'Just two lovers, lovin\' all over each other.'),
    mother('Mother', 'The mother.'),
    none('None', 'No relationship.'),
    sibling('Sibling', 'A sibling.'),
    schoolmate('Schoolmate', 'Schoolmate.'),

    final String prettyName
    final String description

    IndividualRelationType(String prettyName, String description) {
        this.prettyName = prettyName
        this.description = description
    }
}