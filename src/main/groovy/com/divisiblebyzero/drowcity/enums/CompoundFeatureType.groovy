package com.divisiblebyzero.drowcity.enums

enum CompoundFeatureType {
    alchemyLab('Alchemy Lab', 'Doubles the amount of potions generated each week.', 0, 24, 2500, 25, 10, 0, 0, 1, 3, 5, 1, 1, 2, 1250),
    blacksmith('Blacksmith', 'Can turn adamantite into weapons or armor. Can fix weapons and armor damaged in battle.', 5, 4, 200, 2, 0, 0, 0, 0, 0, 0, 0, 1, 10, 100),
    library('Library', 'Improves investigations, potion craft, and magical defenses.', -10, 4, 1000, 10, 15, 2, 2, -5, 5, 15, 25, 2, 5, 1000),
    lizardStable('Lizard Stable', 'Allows the purchase of lizard mounts. Can hold 100 mounts.', 0, 12, 1500, 25, 0, 0, 0, 0, 0, 0, 0, 0, 5, 750),
    scryingMirror('Scrying Mirror', 'Improves investigations, though many houses are guarded against scrying.', 5, 4, 4000, 5, 5, 5, 5, 2, 20, 30, 0, 0, 0, 2000),
    vault('Vault', 'A secure place to store gold, treasures, and other baubles.', 0, 24, 2000, 10, 0, 0, 0, 5, 0, 0, 0, 3, 0, 1000),
    //only purchasable at house creation
    cave('Cave', 'A cave that is attached to your compound. Can be used for storage, or living quarters, or other uses, though no one really wants to live in a cave.', 10, -1, 0, -5, 0, 0, 20, 5, 0, 0, 0, 2, 5, 200),
    stalactite('Stalactite', 'A tapering structure hanging from the roof of a cave. Can contain rooms and other compound features.', 10, -1, 0, 5, 0, 0, 10, 0, 0, 0, 0, 0, 0, 150),
    stalactitePillar('Stalactite Pillar', 'Pillars are a stalactite and a stalagmite grown together. Can contain rooms and other compound features.', 50, -1, 0, 25, 0, 0, 15, 0, 0, 0, 0, 0, 0, 500),
    stalagmite('Stalagmite', 'A mound or tapering column rising from the floor of a cave. Can contain rooms and other compound features.', 5, -1, 0, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0, 100),
    //art
    faerieFireArt('Faerie Fire Art', 'Art dedicated to Lloth that is visible in the normal light spectrum.', 2, 4, 10, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5),
    infraredArt('Infrared Art', 'Art dedicated to Lloth that is visible in the infrared spectrum.', 1, 2, 10, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5),
    //chapels
    giganticChapel('Gigantic Chapel', 'A chapel designed to hold 1000 worshippers.', 100, 32, 10000, 40, 0, 0, 0, -10, 20, 0, 30, 0, 0, 1500),
    largeChapel('Large Chapel', 'A chapel designed to hold 500 worshippers.', 40, 16, 1000, 10, 0, 0, 0, -2, 15, 0, 20, 0, 0, 500),
    mediumChapel('Medium Chapel', 'A chapel designed to hold 250 worshippers.', 20, 12, 500, 0, 0, 0, 0, -1, 10, 0, 10, 0, 0, 250),
    privateChapel('Private Chapel', 'A chapel designed for the private services for a nobel family.', 25, 12, 500, 0, 0, 0, 5, 25, 5, 3, 10, 5, 0, 250),
    smallChapel('Small Chapel', 'A chapel designed to hold 25 worshippers.', 10, 4, 200, -5, 0, 0, 0, 0, 5, 1, 5, 0, 0, 100),
    //food storage
    foodStorage('Food Storage', 'Stores 1000 units of food.', 0, 2, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15),
    protectedFoodStorage('Protected Food Storage', 'Stores 1000 units of food. Immune from enemy tampering.', 0, 4, 100, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 50),
    //gargoyles
    animatedGargoyle('Animated Gargoyle', 'A statue of a gargoyle. This one can animate to defend the compound.', 10, 6, 250, -2, 0, 0, 15, 1, 0, 0, 0, 0, 0, 125),
    gargoyle('Gargoyle', 'A statue of a gargoyle for decorative purposes.', 5, 2, 15, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10),
    //gates
    adamantiteGate('Adamantite Gate', 'An extremely strong gate made from Adamantite. Allows traffic in and out of your compound.', 3, 8, 500, 5, 0, 0, -2, 0, 0, 0, 0, 0, 0, 150),
    ironGate('Iron Gate', 'A gate made from iron. Allows traffic in and out of your compound.', 0, 6, 250, 0, 0, 0, -5, 0, 0, 0, 0, 0, 0, 125),
    mithrilGate('Mithril Gate', 'A gate made from mithril. Allows traffic in and out of your compound.', 2, 8, 750, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 175),
    stoneGate('Stone Gate', 'A plain stone gate. Allows traffic in and out of your compound.', 0, 2, 100, -5, 0, 0, -10, -1, 0, 0, 0, 0, 0, 50),
    //golems
    adamantiteGolem('Adamantite Golem', 'A golem made from Adamantite.', 10, 24, 1500, 20, 5, 10, 20, 0, 0, 0, 0, 0, 0, 750),
    boneGolem('Bone Golem', 'A golem made from bone.', 10, 16, 200, 10, 0, 7, 10, 0, 0, 0, 0, 0, 0, 100),
    brainGolem('Brain Golem', 'A golem made from brain matter.', 15, 24, 1000, 15, 2, 4, 10, 0, 1, 1, 0, 0, 0, 500),
    clayGolem('Clay Golem', 'A golem made from clay.', 0, 16, 100, 2, 0, 4, 5, 0, 0, 0, 0, 0, 0, 50),
    crystalGolem('Crystal Golem', 'A golem made from crystals or gemstones.', 15, 24, 3500, 25, 1, 4, 10, 0, 0, 0, 0, 0, 0, 1750),
    fleshGolem('Flesh Golem', 'A golem made from flesh.', 5, 16, 100, 5, 0, 5, 15, 0, 0, 0, 0, 0, 0, 50),
    goldGolem('Gold Golem', 'A golem made from gold.', 10, 24, 3000, 30, 0, 6, 20, 0, 0, 0, 0, 0, 0, 1500),
    ironGolem('Iron Golem', 'A golem made from iron.', 0, 16, 250, 5, 0, 5, 15, 0, 0, 0, 0, 0, 0, 125),
    mithrilGolem('Mithril Golem', 'A golem made from mithril.', 20, 36, 2500, 30, 3, 8, 25, 0, 0, 0, 0, 0, 0, 1250),
    stoneGolem('Stone Golem', 'A golem made from stone.', 0, 16, 100, 2, 0, 3, 5, 0, 0, 0, 0, 0, 0, 50),
    webGolem('Web Golem', 'A golem made from spider webbing.', 50, 16, 100, 15, 0, 10, 10, 0, 0, 0, 1, 0, 0, 300),
    //guard houses
    guardHouse('Guard House', 'A place for your soldiers to congregate and guard your compound.', 0, 2, 50, 2, 0, 0, 5, -1, 0, 0, 0, 0, 0, 25),
    hiddenGuardHouse('Hidden Guard House', 'A hidden place for your soldiers to congregate and guard your compound.', 0, 2, 50, 0, 0, 0, 7, 2, 0, 0, 0, 0, 0, 125),
    //commoner housing
    comfortableHousing('Comfortable Housing', 'Small apartments designed for two roommates each. Each room has a private lavatory, but shared facilities otherwise. Can hold 20 occupants', 2, 4, 800, 10, 0, 0, 0, -2, 0, 0, 0, 0, 0, 300),
    crampedHousing('Cramped Housing', 'Barracks-style housing with no privacy and shared facilities. Can hold 100 occupants.', 0, 2, 250, 2, 0, 0, -5, -10, 0, 0, 0, 0, 0, 100),
    luxuriousHousing('Luxurious Housing', 'Small apartments for individuals with lavatories per room. Still has shared facilities otherwise. Can hold 10 occupants.', 4, 6, 1600, 20, 0, 0, 1, 0, 0, 0, 0, 0, 0, 400),
    standardHousing('Standard Housing', 'Small, separate rooms that hold two pairs of bunks each, offering little privacy. Has shared facilities. Can hold 50 occupants.', 1, 3, 400, 5, 0, 0, -2, -5, 0, 0, 0, 0, 0, 200),
    housingDefenses('Housing Defenses', 'Defenses built into your housing.', 0, 6, 500, 2, 0, 0, 10, -5, 0, 0, 0, 0, 5, 100),
    //traps
    magicalTraps('Magical Traps', 'A set of magical traps to aid in the defense of your compound.', 0, 2, 300, 0, 1, 0, 20, 2, 0, 0, 0, 0, 0, 150),
    mechanicalTraps('Mechanical Traps', 'A set of mechanical traps to aid in the defense of your compound.', 0, 1, 50, 0, 0, 0, 10, 1, 0, 0, 0, 0, 0, 25),
    //noble housing
    comfortableNobleHousing('Comfortable Noble Housing', 'Each noble has their own small room with some shared amenities. Can house up to 12 nobles in this highly configurable space.', 10, 8, 150, 2, 0, 0, 2, 0, 3, 2, 0, 0, 0, 75),
    crampedNobleHousing('Cramped Noble Housing', 'Each noble has their own minimalistic room with completely shared amenities. Can house up to 12 nobles in this highly configurable space.', 5, 4, 50, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 25),
    luxuriousNobleHousing('Luxurious Noble Housing', 'Each noble has their own spacious room with their own individual amenities. Can house up to 12 nobles in this highly configurable space.', 50, 32, 1500, 10, 0, 0, 7, 0, 7, 4, 0, 0, 0, 750),
    standardNobleHousing('Standard Noble Housing', 'Each noble has their own room with their own individual amenities. Can house up to 12 nobles in this highly configurable space.', 25, 16, 500, 5, 0, 0, 5, 0, 5, 3, 0, 0, 0, 250),
    nobleHousingDefenses('Noble Housing Defenses', 'Defenses built into your noble housing.', 0, 8, 1000, 2, 0, 0, 20, -5, 0, 0, 0, 0, 5, 150),
    //other defenses
    parapet('Parapet', 'Allows defense of your compound from above.', 0, 4, 250, 2, 0, 0, 5, -1, 1, 1, 0, 0, 0, 125),
    shrieker('Shrieker', 'A shrieker is a large mushroom that shrieks when enemies are near.', -5, 12, 250, 2, 0, 0, 5, 5, 0, 0, 0, 0, 0, 400),
    //spider statues
    adnimatedSpiderStatue('Animated Spider Statue', 'A statue of a spider, one of Lloth\'s favorite creatures. This one can animate to defend the compound.', 25, 8, 500, 1, 0, 0, 10, 1, 0, 0, 1, 0, 0, 250),
    spiderStatue('Spider Statue', 'A statue of a spider, one of Lloth\'s favorite creatures.', 10, 4, 50, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25),
    //water storage
    waterStorage('Water Storage', 'Stores 1000 units of water.', 0, 2, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15),
    protectedWaterStorage('Protected Water Storage', 'Stores 1000 units of water. Immune from enemy tampering.', 0, 4, 100, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 50),
    //walls
    adamantiteWall('Adamantite Wall', 'A wall made of Adamantite. The sign of a wealthy house.', 5, 16, 1500, 25, 0, 0, 20, 0, 0, 0, 0, 0, 0, 750),
    ironWall('Iron Wall', 'A wall made of iron. Not flashy or pretty, but they get the job done.', 0, 14, 750, 5, 0, 0, 15, 0, 0, 0, 0, 0, 0, 400),
    mithrilWall('Mithril Wall', 'A wall made from mithril.', 5, 18, 2500, 15, 0, 0, 25, 0, 0, 0, 0, 0, 0, 900),
    stoneWall('Stone Walls', 'Basic stone wall. Not flashy or pretty, but they get the job done.', 0, 12, 250, -5, 0, 0, 10, 0, 0, 0, 0, 0, 0, 125),

    //template('', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),

    final String prettyName
    final String description
    final int llothsFavor
    final int buildTimeInWeeks
    final int costInGold
    final int prestige
    final int arcana
    final int attack
    final int defense
    final int deception
    final int insight
    final int investigation
    final int religion
    final int stealth
    final int survival
    final int creationCost

    CompoundFeatureType(String prettyName, String description, int llothsFavor, int buildTimeInWeeks, int costInGold, int prestige, int arcana, int attack, int defense, int deception, int insight, int investigation, int religion, int stealth, int survival, int creationCost) {
        this.prettyName = prettyName
        this.description = description
        this.llothsFavor = llothsFavor
        this.buildTimeInWeeks = buildTimeInWeeks
        this.costInGold = costInGold
        this.prestige = prestige
        this.arcana = arcana
        this.attack = attack
        this.defense = defense
        this.deception = deception
        this.insight = insight
        this.investigation = investigation
        this.religion = religion
        this.stealth = stealth
        this.survival = survival
        this.creationCost = creationCost
    }
}