package com.divisiblebyzero.drowcity.enums

enum OrganizationalRelationshipStatus {
    owner('Owner', 'The owner of the organization.'),
    matron('Matron', 'The Matron Mother of the organization.'),
    noble('Noble', 'A Noble member of the organization.'),
    leader('Leader', 'A leader within the organization.'),
    weaponMaster('Weapon Master', 'The Weapon Master of the organization.'),
    spyMaster('Spy Master', 'The Spy Master of the organization.'),
    headPriestess('Head Priestess', 'The Head Priestess of the organization.'),
    headWizard('Head Wizard', 'The Head Wizard of the organization.'),
    manager('Manager', 'A manager of people, processes, or inventory of the organization.'),
    member('Member', 'A member of the organization.')

    final String prettyName
    final String description

    OrganizationalRelationshipStatus(String prettyName, String description) {
        this.prettyName = prettyName
        this.description = description
    }
}
