package com.divisiblebyzero.drowcity.enums

enum IndividualToIndividualStatus {
    alliance('Alliance'),
    amiable('Amiable'),
    neutral('Neutral'),
    rival('Rival'),
    hatred('Hatred')

    final String prettyName

    IndividualToIndividualStatus(String prettyName) {
        this.prettyName = prettyName
    }
}