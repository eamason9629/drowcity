package com.divisiblebyzero.drowcity.enums

enum AssetLineItemType {
    consumed('Consumed', 'Items were consumed.'),
    crafted('Crafted', 'Items were crafted.'),
    found('Found', 'Items were found and unclaimed by others.'),
    liberated('Liberated', 'Items were liberated from their previous owner.'),
    lost('Lost', 'Items were lost.'),
    purchased('Purchased', 'Purchased items.'),
    stolen('Stolen', 'Items were stolen from us.')

    final String prettyName
    final String description

    AssetLineItemType(String prettyName, String description) {
        this.prettyName = prettyName
        this.description = description
    }
}