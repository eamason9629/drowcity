package com.divisiblebyzero.drowcity.enums

enum Archetype {
    matron('Matron', 'Matron mother of a house.', 0),
    arachTinilithMatron('Arach-Tinilith Matron', 'The matron mother of the School of Lloth in the Academy.', 5000),
    academyMistress('Academy Mistress', 'A mistress or teacher of Arach-Tinilith.', 3000),
    headPriestess('Head Priestess', 'The lead priestess for a house.', 100),
    highPriestess('High Priestess', 'A priestess of Lloth who has risen to the top eschelon of priestesses in the eyes of Lloth.', 2000),
    priestess('Priestess', 'A priestess of Lloth who has graduated from Arach-Tinilith.', 400),
    acolyte('Acolyte', 'An initiate priestess learning her craft in Arach-Tinilith.', 200),
    archmage('Archmage', 'The Archmage of the City, the highest rank a male can achieve.', 7000),
    sorcereHeadMaster('Sorcere Head Master', 'The Head Master of the Academy school of magic.', 5000),
    sorcereMaster('Sorcere Master', 'A Master in the Academy school of magic.', 3000),
    sorcereProfessor('Sorcere Professor', 'A teacher of magic in the Acadmy school of magic.', 2000),
    headWizard('Head Wizard', 'The Head Wizard of a house.', 100),
    journeymanWizard('Journeyman Wizard', 'A learned and powerful wizard to be sure, but still just a male.', 400),
    wizard('Wizard', 'One who has graduated from Sorcere, but has not accomplished much.', 200),
    apprenticeWizard('Apprentice Wizard', 'One who has finished the base work required in Sorcere, but has not finished their apprenticeship to a Master.', 100),
    neophyte('Neophyte', 'Someone wishing to become a wizard and is attending Sorcere.', 50),
    warMaster('War Master','The "general" of the city. Reports directly to the Ruling Council.', 7000),
    meleeMagthereHeadMaster('Melee-Magthere Head Master','The Head Master of the warrior school in the Academy.', 5000),
    meleeMagthereMaster('Melee-Magthere Master','A Master of the warrior school in the Academy.', 3000),
    meleeMagthereInstructor('Melee-Magthere Instructor','A teacher in the warrior school of the Academy.', 2000),
    weaponMaster('Weapon Master', 'The Weapon Master of a house.', 100),
    championWarrior('Champion Warrior', 'A warrior of some reknown, still just a male.', 400),
    soldier('Soldier', 'An elite warrior with much experience in the field.', 200),
    warrior('Warrior', 'A graduate of Melee-Magthere that has not proven himself in the eyes of Lloth as of yet.', 100),
    conscript('Conscript', 'A student of Melee-Magthere, the school of warriors.', 50),
    inquisitor('Inquisitor', 'The lead spy for the city. Reports directly to the Ruling Council.', 7000),
    spyMaster('Spy Master', 'The Spy Master for a house.', 100),
    blackguard('Blackguard', 'An elite rogue or assassin.', 400),
    scoundrel('Scoundrel', 'A well-known and dangerous rogue.', 200),
    hooligan('Hooligan', 'A recent graduate of Melee-Magthere that has not proven themselves.', 100),
    lowLife('Low-life', 'A student of Melee-Magthere.', 50),
    child('Child', 'Just a child.', 25)

    final String prettyName
    final String description
    final int creationCost

    Archetype(String prettyName, String description, int creationCost) {
        this.prettyName = prettyName
        this.description = description
        this.creationCost = creationCost
    }

    final static List<Archetype> ALL_PRIESTESSES = [acolyte, priestess, highPriestess, headPriestess, academyMistress, arachTinilithMatron, matron]
    final static List<Archetype> ALL_WIZARDS = [neophyte, apprenticeWizard, wizard, journeymanWizard, headWizard, sorcereProfessor, sorcereMaster, sorcereHeadMaster, archmage]
    final static List<Archetype> ALL_SOLDIERS = [conscript, warrior, soldier, championWarrior, weaponMaster, meleeMagthereInstructor, meleeMagthereMaster, meleeMagthereHeadMaster]
    final static List<Archetype> ALL_ROGUES = [lowLife, hooligan, scoundrel, blackguard, spyMaster, inquisitor]
    final static List<Archetype> ALL_HOUSE_LEADERS = [matron, headPriestess, headWizard, weaponMaster, spyMaster]
    final static List<Archetype> ALL_ADULTS = values() - child
}