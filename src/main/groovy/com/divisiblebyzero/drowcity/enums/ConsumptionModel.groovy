package com.divisiblebyzero.drowcity.enums

enum ConsumptionModel {
    feasting('Feasting', 'Each meal is a feast.'),
    normal('Normal', 'Each meal is a healthy and normal amount of food.'),
    light('Light', 'Each meal is a little on the light side.'),
    rationed('Rationed', 'Each meal is the bare minimum to survive long term.'),
    starvation('Starvation', 'Each meal is very slim, the consumers will not survive for an extended period with this amount of food.'),
    none('None', 'Meals are not provided at all.')

    final String prettyName
    final String description

    ConsumptionModel(String prettyName, String description) {
        this.prettyName = prettyName
        this.description = description
    }
}