package com.divisiblebyzero.drowcity.enums

enum Region {
    city('City', 'Within the city limits.'),
    northernFrontier('Northern Frontier', 'The region of the Underdark near the city on the northern frontier.'),
    northEasternFrontier('North Eastern Frontier', 'The region of the Underdark near the city on the northern eastern frontier.'),
    easternFrontier('Eastern Frontier', 'The region of the Underdark near the city on the eastern frontier.'),
    southEasternFrontier('South Eastern Frontier', 'The region of the Underdark near the city on the south eastern frontier.'),
    southernFrontier('Southern Frontier', 'The region of the Underdark near the city on the southern frontier.'),
    southWesternFrontier('South Western Frontier', 'The region of the Underdark near the city on the south western frontier.'),
    westernFrontier('Western Frontier', 'The region of the Underdark near the city on the western frontier.'),
    northWesternFrontier('North Western Frontier', 'The region of the Underdark near the city on the north western frontier.'),
    northUnderdark('North Underdark', 'The region of the Underdark away from the city on the northern frontier.'),
    eastUnderdark('East Underdark', 'The region of the Underdark away from the city on the eastern frontier.'),
    southUnderdark('South Underdark', 'The region of the Underdark away from the city on the southern frontier.'),
    westUnderdark('West Underdark', 'The region of the Underdark away from the city on the western frontier.'),

    final String prettyName
    final String description

    Region(String prettyName, String description) {
        this.prettyName = prettyName
        this.description = description
    }
}