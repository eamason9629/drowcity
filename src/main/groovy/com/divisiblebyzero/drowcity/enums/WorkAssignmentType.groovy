package com.divisiblebyzero.drowcity.enums

enum WorkAssignmentType {
    runTheBusiness('Run The Business', 'These workers will run the business at this location.', 90),
    showForce('Show Force', 'These workers will publicly display their house emblems and make their presence known in the area.', 100),
    gatherIntel('Gather Intel', 'These workers will lay low and gather information in the area.', 25),
    quietGarrison('Quietly Garrison', 'These workers will lay low, but defend the area if necessary.', 10),

    final String prettyName
    final String description
    final int visibility

    WorkAssignmentType(String prettyName, String description, int visibility) {
        this.prettyName = prettyName
        this.description = description
        this.visibility = visibility
    }
}