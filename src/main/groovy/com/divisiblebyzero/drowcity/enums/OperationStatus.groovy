package com.divisiblebyzero.drowcity.enums

enum OperationStatus {
    planned('Planned', 'This operation is planned to begin next week.'),
    inProgress('In Progress', 'This operation is currently being executed.'),
    onHold('On Hold', 'This operation is currently on hold.'),
    cancelled('Cancelled', 'This operation has been cancelled before completion.'),
    failed('Failed', 'This operation failed.'),
    success('Success', 'This operation completed successfully.')

    final String prettyName
    final String description

    OperationStatus(String prettyName, String description) {
        this.prettyName = prettyName
        this.description = description
    }
}