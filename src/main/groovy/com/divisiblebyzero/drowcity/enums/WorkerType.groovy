package com.divisiblebyzero.drowcity.enums

enum WorkerType {
    soldier('Soldier', 'A common drow soldier in the employ of an organization.', 1),
    priestess('Priestess', 'A common drow priestess in the employ of an organization.', 25),
    wizard('Wizard', 'A common drow wizard in the employ of an organization.', 50),
    staff('Staff', 'A commoner with no special skills in the employ of an organization.', 0.2),
    orc('Orc', 'An orc. Ugly, brutish creatures. Decent at fighting and hard labor, not much else.', 1),
    goblin('Goblin', 'Goblins are basically smaller, scrawnier, more cowardly orcs.', 0.2),
    kobold('Kobold','Kobolds are small, cowardly creatures that are not good for much except battle fodder. They claim ancestry from dragons, though I, for one, do not see how that is possible.', 0.1),
    duergar('Duergar', 'The Grey Ones are adept warriors and laborers.', 2),
    dwarf('Dwarf', 'Dwarves are too stubborn and proud to make good slaves. However, they are excellent warriors and laborers if you can keep them.', 5),
    gnome('Gnome', 'The Svnirfneblin are excellent laborers and warriors, though they are quite difficult to keep as slaves.', 10),
    minotaur('Minotaur', 'Tall and brutish, they make excellent guardians and elite units in combat.', 25),
    beholder('Beholder', 'The Eye Tyrants are powerful allies and frightening foes.', 250),
    illithid('Illithid', 'Mind Flayers are powerful allies, though secretive and untrustworthy.', 500),
    kuoToa('Kuo-toa', 'The insane fanatics of make-believe gods will work hard and fight hard if they believe in your cause. They will fight harder against you if they believe you are against their gods.', 2),
    bugbear('Bugbear', 'These massive goblinoids make for great shock troops. They are often stupid enough to work just for the opportunity to fight with drow weapons.', 3),
    lamia('Lamia', 'Beautiful and elegant, drow share a lot of common traits with these creatures. They make powerful, but dangerous allies.', 10),
    nightmare('Nightmare', 'The Demon Horses are frightening to even the most stalwart Drow Matron. If they can be tamed, they make excellent allies, or mounts if they will allow it.', 200),
    zombie('Zombie', 'The reanimated corpse of a drow commoner.', 1),
    skeleton('Skeleton', 'The reanimated corpse of a drow commoner.', 0.5),
    drider('Drider', 'A drow transformed by the powers of Lloth into a hybrid of drow and spider.', 100)

    final String prettyName
    final String description
    final double creationCost

    WorkerType(String prettyName, String description, double creationCost) {
        this.prettyName = prettyName
        this.description = description
        this.creationCost = creationCost
    }
}