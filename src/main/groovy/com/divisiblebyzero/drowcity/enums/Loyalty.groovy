package com.divisiblebyzero.drowcity.enums

enum Loyalty {
    fanatic, entusiastic, loyal, positive, indifferent, negative, apathetic, opponent
}