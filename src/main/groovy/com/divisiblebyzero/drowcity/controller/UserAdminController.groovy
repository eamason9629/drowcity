package com.divisiblebyzero.drowcity.controller

import com.divisiblebyzero.drowcity.dto.PlayerAndRolesDTO
import com.divisiblebyzero.drowcity.dto.UserAdminDTO
import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.entity.UserRole
import com.divisiblebyzero.drowcity.repository.PlayerRepository
import com.divisiblebyzero.drowcity.repository.UserRoleRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*

@Slf4j
@RestController
@Secured(['userAdmin']) //TODO: this is not working
class UserAdminController {
    @Autowired
    PlayerRepository playerRepository

    @Autowired
    UserRoleRepository userRoleRepository

    @GetMapping('/player/manage')
    UserAdminDTO manageAllPlayers() {
        UserAdminDTO result = new UserAdminDTO()
        playerRepository.findAll().each { Player player ->
            List<UserRole> userRoles = userRoleRepository.findAllByUsername(player.username)
            result.players << PlayerAndRolesDTO.create(player, userRoles)
        }
        return result
    }

    @PostMapping('/player/{playerId}/updateRoles')
    void updatePlayerRoles(@PathVariable UUID playerId, @RequestBody List<String> roles) {
        Player player = playerRepository.findById(playerId).get()
        log.error(player)
        List<UserRole> currentRoles = userRoleService.findRolesForPlayer(player)
        log.error(currentRoles)
        log.error(roles)
    }
}
