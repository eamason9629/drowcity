package com.divisiblebyzero.drowcity.controller

import com.divisiblebyzero.drowcity.dto.HouseCreationProcessDTO
import com.divisiblebyzero.drowcity.service.HouseCreationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class HouseCreationController {
    @Autowired
    HouseCreationService houseCreationService

    @GetMapping('/house/create')
    HouseCreationProcessDTO createHouse() {
        return houseCreationService.createHouse()
    }

    @PostMapping('/house/create')
    HouseCreationProcessDTO createHouse(@RequestBody HouseCreationProcessDTO newHouse) {
        return houseCreationService.calculateRemainingPoints(newHouse)
    }
}
