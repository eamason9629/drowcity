package com.divisiblebyzero.drowcity.controller

import com.divisiblebyzero.drowcity.dto.AssetSummaryDTO
import com.divisiblebyzero.drowcity.dto.HouseDashboardDTO
import com.divisiblebyzero.drowcity.dto.MessageDTO
import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.enums.OrganizationalRelationshipStatus
import com.divisiblebyzero.drowcity.service.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

import static com.divisiblebyzero.drowcity.dto.HouseDashboardDTO.create

@RestController
class HouseDashboardController {
    @Autowired
    HouseService houseService

//    @Autowired
//    WeeklyReportService weeklyReportService

    @Autowired
    MessageService messageService

//    @Autowired
//    OperationService operationService

//    @Autowired
//    EventService eventService

    @GetMapping('/houseDashboard/{id}')
    HouseDashboardDTO findHouseById(@PathVariable UUID id) {
        Organization house = houseService.findHouseById(id)
        HouseDashboardDTO result = create(house)
        result.messages = MessageDTO.create(messageService.findMessagesTo(house.members.find { it.status == OrganizationalRelationshipStatus.matron }.to))
        return result
    }
}
