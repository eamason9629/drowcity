package com.divisiblebyzero.drowcity.controller

import com.divisiblebyzero.drowcity.dto.PlayerDTO
import com.divisiblebyzero.drowcity.dto.UserRoleDTO
import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.enums.UserRoleTypes
import com.divisiblebyzero.drowcity.repository.PlayerRepository
import com.divisiblebyzero.drowcity.service.PlayerService
import com.divisiblebyzero.drowcity.service.UserRoleService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

import java.security.Principal

import static com.divisiblebyzero.drowcity.dto.UserRoleDTO.create

@RestController
class PlayerController {
    @Autowired
    PlayerRepository playerRepository

    @Autowired
    PlayerService playerService

    @Autowired
    UserRoleService userRoleService

    @PostMapping('/createPlayer')
    PlayerDTO processCreatePlayer(Player player) {
        PlayerDTO.create(playerService.createPlayer(player))
    }

    @GetMapping('/loggedUser')
    PlayerDTO playerLoggedIn(Principal principal) {
        Player result = playerRepository.findByUsername(principal?.name)
        if (result) {
            return PlayerDTO.create(result)
        }
        return new PlayerDTO()
    }

    @GetMapping('/player/roles')
    List<UserRoleDTO> playerRoles(Principal principal) {
        if(!principal) {
            return [new UserRoleDTO(role: UserRoleTypes.notLoggedIn)]
        }
        create(userRoleService.findRolesForPlayer(playerRepository.findByUsername(principal?.name)))
    }
}
