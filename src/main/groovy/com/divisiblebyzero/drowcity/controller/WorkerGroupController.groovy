package com.divisiblebyzero.drowcity.controller


import com.divisiblebyzero.drowcity.dto.WorkerGroupDTO
import com.divisiblebyzero.drowcity.service.WorkerGroupService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class WorkerGroupController {
    @Autowired
    WorkerGroupService workerGroupService

    @GetMapping('/workerGroup/{id}')
    WorkerGroupDTO findCityById(@PathVariable UUID id) {
        WorkerGroupDTO.create(workerGroupService.findById(id))
    }
}
