package com.divisiblebyzero.drowcity.controller

import com.divisiblebyzero.drowcity.dto.AssetDTO
import com.divisiblebyzero.drowcity.entity.Organization
import com.divisiblebyzero.drowcity.service.AssetService
import com.divisiblebyzero.drowcity.service.OrganizationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class AssetController {
    @Autowired
    AssetService assetService

    @GetMapping('/asset/{id}')
    AssetDTO findAssetByOrgIdAndType(@PathVariable UUID id) {
        AssetDTO.create(assetService.findById(id))
    }
}
