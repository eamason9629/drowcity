package com.divisiblebyzero.drowcity.controller

import com.divisiblebyzero.drowcity.dto.UserDashboardDTO
import com.divisiblebyzero.drowcity.entity.Player
import com.divisiblebyzero.drowcity.service.PlayerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

import java.security.Principal

@RestController
class UserDashboardController {
    @Autowired
    PlayerService playerService

    @GetMapping(path = '/userDashboard')
    UserDashboardDTO fetchUserDashboard(Principal principal) {
        Player player = playerService.findPlayerForUsername(principal?.name)
        if(player) {
            return UserDashboardDTO.create(player)
        } else {
            return new UserDashboardDTO()
        }
    }
}
