package com.divisiblebyzero.drowcity.controller

import com.divisiblebyzero.drowcity.dto.CityDTO
import com.divisiblebyzero.drowcity.service.CityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

import static com.divisiblebyzero.drowcity.dto.CityDTO.create
import static com.divisiblebyzero.drowcity.util.NameGenerator.cityName

@RestController
class CityController {
    @Autowired
    CityService cityService

    @GetMapping('/city/{id}')
    CityDTO findCityById(@PathVariable UUID id) {
        create(cityService.findCityById(id))
    }

    @GetMapping('/city/list')
    List<CityDTO> listCities() {
        create(cityService.listCities())
    }

    @GetMapping('/city/create')
    CityDTO createCity() {
        new CityDTO(name: cityName())
    }

    @PostMapping('/city/create')
    CityDTO createCity(CityDTO city) {
        create(cityService.createCity(city))
    }
}
