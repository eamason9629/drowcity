package com.divisiblebyzero.drowcity.controller

import com.divisiblebyzero.drowcity.dto.MessageDTO
import com.divisiblebyzero.drowcity.service.IndividualService
import com.divisiblebyzero.drowcity.service.MessageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

import static com.divisiblebyzero.drowcity.dto.MessageDTO.create

@RestController
class MessageController {
    @Autowired
    MessageService messageService

    @Autowired
    IndividualService individualService

    @GetMapping('/message/for/{individualId}/unread')
    List<MessageDTO> findUnreadMessagesFor(@PathVariable UUID individualId) {
        create(messageService.findUnreadMessagesTo(individualService.findById(individualId)))
    }

    @GetMapping('/message/for/{individualId}')
    List<MessageDTO> findMessagesFor(@PathVariable UUID individualId) {
        create(messageService.findMessagesTo(individualService.findById(individualId)))
    }

    @GetMapping('/message/{id}')
    MessageDTO findMessageById(@PathVariable UUID id) {
        create(messageService.findMessageById(id))
    }

    @PostMapping('/message/send')
    MessageDTO createMessage(MessageDTO messageDTO) {
        create(messageService.sendMessage(create(messageDTO, individualService.findById(messageDTO.from), individualService.findById(messageDTO.to))))
    }
}
