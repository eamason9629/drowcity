import SCREENS from '../constants/Screens';
import loggedUserService from './LoggedUserService'

class SecurityService {
    canUserViewScreen(screen) {
        if(screen.roles.length === 0) {
            return new Promise((resolve, reject) => {
                resolve(true);
            });
        }
        return loggedUserService.fetchUserRoles().then((userRoles) => {
            let found = false;
            userRoles.forEach(userRole => {
                if(screen.roles.includes(userRole.role)) {
                    found = true;
                }
            });
            return found;
        });
    }
};

export default new SecurityService();