let loggedUser = {};
let userRoles = [];

class LoggedUserService {
    fetchLoggedUser(callback) {
        const that = this;
        return new Promise((resolve, reject) => {
            if(loggedUser.username) {
                resolve(loggedUser);
            } else {
                fetch('/loggedUser').then(response => {
                    response.json().then(json => {
                        loggedUser = json;
                        resolve(loggedUser);
                    });
                });
            }
        });
    }

    fetchUserRoles(callback) {
        const that = this;
        return new Promise((resolve, reject) => {
            if(userRoles.length > 0) {
                resolve(userRoles);
            } else {
                fetch('/player/roles').then(response => {
                    response.json().then(json => {
                        userRoles = json;
                        resolve(userRoles);
                    });
                });
            }
        });
    }
};

export default new LoggedUserService();