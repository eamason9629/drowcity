import React, { Component } from 'react';
import ReadOnlyField from './ReadOnlyField';
import DropdownInput from './DropdownInput';
import TextInput from './TextInput';

class GenericForm extends Component {
    renderField(field, value) {
        switch(field.type) {
            case 'readonly':
            case 'readOnly':
                return <ReadOnlyField key={field.key ? field.key : field.name} fieldName={field.name} type='text' value={value} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
            case 'text':
                return <TextInput key={field.key ? field.key : field.name} fieldName={field.name} type='text' value={value} onChange={this.props.onChange} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
            case 'int':
                return <TextInput key={field.key ? field.key : field.name} fieldName={field.name} type='number' value={value} onChange={this.props.onChange} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
            case 'enum':
                return <DropdownInput key={field.key ? field.key : field.name} fieldName={field.name} value={value} values={field.values} onChange={this.props.onChange} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
        }
    }

    render() {
        return (
            <form style={{display: this.props.display ? this.props.display : 'inline-block'}}>
                {this.props.fields.map(field => {
                    return this.renderField(field, this.props.data[field.name]);
                })}
            </form>
        );
    }
}

export default GenericForm;