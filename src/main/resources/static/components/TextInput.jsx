import React, { Component } from 'react';

class TextInput extends Component {
    constructor(options) {
        super();

        this.state = {
            value: options.value
        };

        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        this.props.onChange(this.props.fieldName, event.target.value);
        this.setState({value: event.target.value});
    }

    render() {
        return (
            <div className={'mb-3 form-floating tool-tip ' + (this.props.fieldClass ? this.props.fieldClass : '')}>
                <input name={this.props.fieldName} id={this.props.fieldName} className='form-control' type={this.props.type} value={this.state.value} placeholder={this.props.prettyName} aria-label={this.props.prettyName} aria-describedby={this.props.fieldName + '-label'} onChange={this.onChange} />
                <label id={this.props.fieldName + '-label'} htmlFor={this.props.fieldName}>{this.props.prettyName}</label>
                <span className='tool-tip-text'>{this.props.tooltip}</span>
            </div>
        );
    }
}

export default TextInput;