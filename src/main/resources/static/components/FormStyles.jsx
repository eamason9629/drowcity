import React, { Component } from 'react';

class FormStyles extends Component {
    render() {
        return (
            <style>
                {`
                .tool-tip {
                  position: relative;
                  display: inline-block;
                }

                .tool-tip .tool-tip-text {
                  visibility: hidden;
                  background-color: black;
                  color: #fff;
                  text-align: center;
                  padding: 5px;
                  border-radius: 6px;

                  position: absolute;
                  z-index: 1;
                }

                .tool-tip:hover .tool-tip-text {
                  visibility: visible;
                }`}
            </style>
        );
    }
}

export default FormStyles;