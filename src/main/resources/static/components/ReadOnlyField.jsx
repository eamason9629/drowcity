import React, { Component } from 'react';

class ReadOnlyField extends Component {
    constructor(options) {
        super();

        this.state = {
            value: options.value
        };
    }

    render() {
        return (
            <div className={'mb-3 form-floating tool-tip ' + (this.props.fieldClass ? this.props.fieldClass : '')}>
                <input name={this.props.fieldName} id={this.props.fieldName} className='form-control' type={this.props.type} value={this.state.value} placeholder={this.props.prettyName} aria-label={this.props.prettyName} aria-describedby={this.props.fieldName + '-label'} readOnly />
                <label id={this.props.fieldName + '-label'} htmlFor={this.props.fieldName}>{this.props.prettyName}</label>
                <span className='tool-tip-text'>{this.props.tooltip}</span>
            </div>
        );
    }
}

export default ReadOnlyField;