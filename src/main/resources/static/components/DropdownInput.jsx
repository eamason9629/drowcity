import React, { Component } from 'react';

class DropdownInput extends Component {
    constructor(options) {
        super();

        this.state = {
            value: options.value
        };

        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        this.props.onChange(event.target.name, event.target.value);
        this.setState({value: event.target.value});
    }

    render() {
        return (
            <div className={'mb-3 form-floating tool-tip ' + (this.props.fieldClass ? this.props.fieldClass : '')}>
                <select className='form-select' name={this.props.fieldName} id={this.props.fieldName} value={this.state.value} onChange={this.onChange} aria-label={this.props.prettyName}>
                    {this.props.values.map(it => {
                        return (
                            <option key={this.props.fieldName + '-' + it.name} value={it.name}>{it.prettyName}</option>
                        );
                    })}
                </select>
                <label htmlFor={this.props.fieldName}>{this.props.prettyName}</label>
                <span className='tool-tip-text'>{this.props.tooltip}</span>
            </div>
        );
    }
}

export default DropdownInput;