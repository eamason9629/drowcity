import React, { Component } from 'react';
import SCREENS from '../constants/Screens';
import loggedUserService from '../services/LoggedUserService';
import AssetView from '../views/AssetView';
import CityAdminView from '../views/CityAdminView';
import CreateHouseView from '../views/CreateHouseView';
import CreateUserView from '../views/CreateUserView';
import HeaderView from '../views/HeaderView';
import HouseDashboardView from '../views/HouseDashboardView';
import LoginView from '../views/LoginView';
import MessageView from '../views/MessageView';
import UserAdminView from '../views/UserAdminView';
import UserDashboardView from '../views/UserDashboardView';
import WorkerGroupView from '../views/WorkerGroupView';

class MainController extends Component {
    constructor() {
        super();

        this.state = {
            currentScreen: SCREENS.userDashboard.name,
            loggedIn: false,
            context: '',
            history: [{currentScreen: SCREENS.userDashboard.name, context: ''}]
        };

        this.setScreen = this.setScreen.bind(this);
        this.renderBody = this.renderBody.bind(this);
    }

    componentDidMount() {
        let that = this;
        loggedUserService.fetchLoggedUser().then((loggedUser) => {
            if(loggedUser.username) {
                that.setState({loggedIn: true});
            } else {
                that.setState({loggedIn: false});
            }
        });
    }

    setScreen(screen, context) {
        let history = [...this.state.history];
        let nextScreen = {currentScreen: screen, context: context};
        history.push(nextScreen);
        nextScreen.history = history;
        this.setState(nextScreen);
    }

    goBack() {
        this.state.history.pop();
        let screen = this.state.history[this.state.history.length - 1];
        this.setState(screen);
    }

    renderBody() {
        if(this.state.loggedIn) {
            switch(this.state.currentScreen) {
                case SCREENS.asset.name:
                    return (
                        <AssetView assetId={this.state.context} />
                    );
                case SCREENS.cityAdmin.name:
                    return (
                        <CityAdminView />
                    );
                case SCREENS.createHouse.name:
                    return (
                        <CreateHouseView />
                    );
                case SCREENS.createUser.name:
                    return (
                        <CreateUserView />
                    );
                case SCREENS.houseView.name:
                    return (
                        <HouseDashboardView houseId={this.state.context} changeScreen={this.setScreen.bind(this)} />
                    );
                case SCREENS.login.name:
                    return (
                        <LoginView />
                    );
                case SCREENS.message.name:
                    return (
                        <MessageView messageId={this.state.context} />
                    );
                case SCREENS.userAdmin.name:
                    return (
                        <UserAdminView />
                    );
                case SCREENS.workerGroup.name:
                    return (
                        <WorkerGroupView groupId={this.state.context} />
                    );
                case SCREENS.userDashboard.name:
                default:
                    return (
                        <UserDashboardView changeScreen={this.setScreen.bind(this)} />
                    );
            }
        } else {
            return (
                <LoginView />
            );
        }
    }

    render() {
        return (
            <div id='drowCity'>
                <HeaderView changeScreen={this.setScreen.bind(this)} goBack={this.goBack.bind(this)} />
                {this.renderBody()}
            </div>
        );
    }
}

export default MainController;
