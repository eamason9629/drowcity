import React, { Component } from 'react';

class UserAdminView extends Component {
    constructor() {
        super();

        this.state = {
            players: [],
            allRoles: []
        };
    }

    componentDidMount() {
        const that = this;
        fetch('/player/manage').then(response => {
            response.json().then(json => {
                that.setState(json);
            });
        });
    }

    renderPlayer(player) {
        return (
            <tr key={player.id}>
                <th scope='col'>{player.username}</th>
                <td scope='col'>{player.firstName}</td>
                <td scope='col'>{player.lastName}</td>
                <td scope='col'>{`${player.enabled}`}</td>
                {this.state.allRoles.map(role => {
                    return <td scope='col' key={player.id + '-' + role}>{`${player.roles[role]}`}</td>
                })}
            </tr>
        );
    }

    render() {
        return (
            <table className='table'>
                <thead>
                    <tr>
                        <th scope='col'>Username</th>
                        <th scope='col'>First Name</th>
                        <th scope='col'>Last Name</th>
                        <th scope='col'>Enabled?</th>
                        {this.state.allRoles.map(role => {
                            return <th scope='col' key={role}>{role}</th>
                        })}
                    </tr>
                </thead>
                <tbody>
                    {this.state.players.map(player => {
                        return this.renderPlayer(player)
                    })}
                </tbody>
            </table>
        );
    }
}

export default UserAdminView;