import React, { Component } from 'react';

class WorkerGroupView extends Component {
    constructor() {
        super();

        this.state = {
            lineItems: []
        };
    }

    componentDidMount() {
        const that = this;
        fetch('/workerGroup/' + this.props.groupId).then(response => {
            response.json().then(json => {
                that.setState(json);
            });
        });
    }

    render() {
        return (
            <div className='row'>
                <div className='column'>
                    <div>House {this.state.employerName}'s {this.state.workerType}(s)</div>
                    <div>Employed by {this.state.employmentType}</div>
                    <div>Currently fed on a {this.state.consumptionModel} consumption model</div>
                    <div>Currently employing {this.state.currentCount} {this.state.workerType}(s)</div>
                </div>
                <div className='column'>
                    <span>Worker History</span>
                    {this.state.lineItems.map(item => {
                        return (
                            <div key={item.id}>
                                Week {item.week}: {item.change} {item.type}
                            </div>
                        )
                    })}
                </div>
            </div>
        );
    }
}

export default WorkerGroupView;