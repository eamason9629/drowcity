import '../css/houseDashboardView.css';
import React, { Component } from 'react';
import SCREENS from '../constants/Screens';

class HouseDashboardView extends Component {
    constructor() {
        super();

        this.state = {
            reports: [],
            messages: [],
            assets: [],
            staff: [],
            nobles: [],
            operations: [],
            events: [],
        };
    }

    componentDidMount() {
        const that = this;
        fetch('/houseDashboard/' + this.props.houseId).then(response => {
            response.json().then(json => {
                that.setState(json);
            });
        });
    }

    renderJumpLinks() {
        return (
            <div className='jumpLinks'>
                <div><a href='#contain'>Top</a></div>
                <div><a href='#reports'>Reports</a></div>
                <div><a href='#messages'>Messages</a></div>
                <div><a href='#assets'>Assets</a></div>
                <div><a href='#staff'>Staff</a></div>
                <div><a href='#nobles'>Nobles</a></div>
                <div><a href='#operations'>Operations</a></div>
                <div><a href='#events'>Events</a></div>
            </div>
        );
    }

    renderFloatingContext() {
        return (
            <div className='floatingContext'>
                <div className='houseContext'>
                    <div>House {this.state.name}</div>
                    <div>City of {this.state.cityName}</div>
                    <div>Week {this.state.week}</div>
                </div>
                <div className='actionContext'>
                    <div>Send New Message</div>
                    <div>Browse Open Market</div>
                    <div>Browse Black Market</div>
                    <div>Hire Staff</div>
                    <div>Manage Nobles</div>
                    <div>Launch Operation</div>
                </div>
            </div>
        );
    }

    renderReports() {
        return (<h3 id='reports' className='row'>No reports</h3>);
    }

    renderMessages() {
        if(this.state.messages.length === 0) {
            return (<h3 id='messages' className='row'>No messages</h3>);
        } else {
            return (
                <div id='messages'>
                    <div className='row'><h3 style={{marginBottom: 0}}>Messages</h3></div>
                    {this.state.messages.map(message => {
                        return (
                            <div className='row' key={message.id} onClick={function () {this.props.changeScreen(SCREENS.message.name, message.id)}.bind(this)}>
                                {message.readByTo ? (<div className='column'>read</div>) : (<div className='column'>unread</div>)}
                                <div className='column'>From: {message.fromName}</div>
                                <div className='column'>Week: {message.week}</div>
                                <div className='column'>Subject: {message.subject}</div>
                            </div>
                        )
                    })}
                </div>
            );
        }
    }

    renderAssets() {
        if(this.state.assets.length === 0) {
            return (<h3 id='assets' className='row'>No assets</h3>);
        } else {
            return (
                <div id='assets'>
                    <div className='row'><h3 style={{marginBottom: 0}}>Assets</h3></div>
                    {this.state.assets.map(asset => {
                        return (
                            <div className='row' key={asset.id} onClick={function () {this.props.changeScreen(SCREENS.asset.name, asset.id)}.bind(this)}>
                                <div className='column'>{asset.typePrettyName}</div>
                                <div className='column'>Count: {asset.currentCount}</div>
                                <div className='column'>Last Change: {asset.lastChange}</div>
                            </div>
                        )
                    })}
                </div>
            );
        }
    }

    renderStaff() {
        if(this.state.staff.length === 0) {
            return (<h3 id='staff' className='row'>No staff</h3>);
        } else {
            return (
                <div id='staff'>
                    <div className='row'><h3 style={{marginBottom: 0}}>Staff</h3></div>
                    {this.state.staff.map(staff => {
                        return (
                            <div className='row' key={staff.id} onClick={function () {this.props.changeScreen(SCREENS.workerGroup.name, staff.id)}.bind(this)}>
                                <div className='column'>{staff.workerType} ({staff.employmentType})</div>
                                <div className='column'>Rations: {staff.consumptionModel}</div>
                                <div className='column'>Count: {staff.currentCount}</div>
                                <div className='column'>Last Change: {staff.lastChangeAmount}</div>
                            </div>
                        )
                    })}
                </div>
            );
        }
    }

    renderNobles() {
        if(this.state.nobles.length === 0) {
            return (<h3 id='nobles' className='row'>No nobles</h3>);
        } else {
            return (
                <div id='nobles'>
                    <div className='row'><h3 style={{marginBottom: 0}}>Nobles</h3></div>
                    {this.state.nobles.map(noble => {
                        return (
                            <div className='row' key={noble.id}>
                                <div className='column'>{noble.status} {noble.name}</div>
                                <div className='column'>Gender: {noble.gender}</div>
                            </div>
                        )
                    })}
                </div>
            );
        }
    }

    renderOperations() {
        return (<h3 id='operations' className='row'>No operations</h3>);
    }

    renderEvents() {
        return (<h3 id='events' className='row'>No events</h3>);
    }

    renderHouse() {
        return (
            <div id='contain'>
                {this.renderJumpLinks()}
                {this.renderFloatingContext()}
                <div className='inkTitle'>From the desk of Matron {this.state.matronName}...</div>
                {this.renderReports()}
                {this.renderMessages()}
                {this.renderAssets()}
                {this.renderStaff()}
                {this.renderNobles()}
                {this.renderOperations()}
                {this.renderEvents()}
            </div>
        );
    }

    render() {
        if(this.state.id) {
            return this.renderHouse();
        } else {
            return (<div></div>);
        }
    }
}

export default HouseDashboardView;