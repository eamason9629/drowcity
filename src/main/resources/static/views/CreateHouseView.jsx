import React, { Component } from 'react';
import FormStyles from '../components/FormStyles';
import TextInput from '../components/TextInput';
import DropdownInput from '../components/DropdownInput';

class CreateHouseView extends Component {
    constructor() {
        super();

        this.state = {
            newHouse: {},
            compoundFeatures: [],
            nobles: [],
            enums: {}
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onFieldChange = this.onFieldChange.bind(this);
        this.onAddCompoundFeature = this.onAddCompoundFeature.bind(this);
        this.onRemoveCompoundFeature = this.onRemoveCompoundFeature.bind(this);
        this.onNobleFieldChange = this.onNobleFieldChange.bind(this);
        this.onAddNoble = this.onAddNoble.bind(this);
    }

    componentDidMount() {
        const that = this;
        fetch('/house/create').then(response => {
            response.json().then(json => {
                that.setState(json);
            });
        });
    }

    onSubmit(newState) {
        const that = this;
        fetch('/house/create', {
            method: 'POST',
            body: JSON.stringify(newState),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        }).then(response => response.json())
        .then(json => that.setState(json));
    }

    onFieldChange(fieldName, newValue) {
        console.log(fieldName + ' ' + newValue);
        let newState = {...this.state};
        let newHouse = {...this.state.newHouse};
        newState.newHouse = newHouse;
        newHouse[fieldName] = newValue;
        this.onSubmit(newState);
    }

    fields() {
        return {
            name: {name: 'name', prettyName: 'Name', type: 'text', tooltip: 'The name of your house.', fieldClass: 'col'},
            familyTrait: {name: 'familyTrait', prettyName: 'Family Trait', type: 'enum', values: Object.values(this.state.enums['FamilyTrait'].values), tooltip: 'The trait the nobles of your house exhibit.', fieldClass: 'col'},
            gold: {name: 'gold', prettyName: 'Gold', type: 'int', tooltip: 'How much gold is in your reserves.', fieldClass: 'col'},
            rotheFarmBid: {name: 'rotheFarmBid', prettyName: 'Rothe Farm Bid', type: 'int', tooltip: 'Your bid on the Rothe Farming monopoly.', fieldClass: 'col'},
            adamantiumMineBid: {name: 'adamantiumMineBid', prettyName: 'Adamantium Farm Bid', type: 'int', tooltip: 'Your bid on the Adamantium Mining monopoly.', fieldClass: 'col'},
            waterSupplyBid: {name: 'waterSupplyBid', prettyName: 'Water Supply Bid', type: 'int', tooltip: 'Your bid on the Water Supply monopoly.', fieldClass: 'col'},
            fishmongeringBid: {name: 'fishmongeringBid', prettyName: 'Fishmongering Bid', type: 'int', tooltip: 'Your bid on the Fishmongering monopoly.', fieldClass: 'col'},
            lizardFarmingBid: {name: 'lizardFarmingBid', prettyName: 'Lizard Farming Bid', type: 'int', tooltip: 'Your bid on the Lizard Farming monopoly.', fieldClass: 'col'},
            mossFarmingBid: {name: 'mossFarmingBid', prettyName: 'Moss Farming Bid', type: 'int', tooltip: 'Your bid on the Moss Farming monopoly.', fieldClass: 'col'},
            mushroomFarmingBid: {name: 'mushroomFarmingBid', prettyName: 'Mushroom Farming Bid', type: 'int', tooltip: 'Your bid on the Mushroom Farming monopoly.', fieldClass: 'col'},
            mortuaryBid: {name: 'mortuaryBid', prettyName: 'Mortuary Bid', type: 'int', tooltip: 'Your bid on the Mortuary monopoly.', fieldClass: 'col'},
            garbageDumpBid: {name: 'garbageDumpBid', prettyName: 'Garbage Dump Bid', type: 'int', tooltip: 'Your bid on the Garbage Dump monopoly.', fieldClass: 'col'},
            arenaBid: {name: 'arenaBid', prettyName: 'Arena Bid', type: 'int', tooltip: 'Your bid on the Arena monopoly.', fieldClass: 'col'},
            soldierCount: {name: 'soldierCount', prettyName: 'Drow Soldiers', type: 'int', tooltip: 'The number of commoner soldiers your house employs.', fieldClass: 'col'},
            priestessCount: {name: 'priestessCount', prettyName: 'Drow Priestesses', type: 'int', tooltip: 'The number of commoner priestesses your house employs.', fieldClass: 'col'},
            wizardCount: {name: 'wizardCount', prettyName: 'Drow Wizards', type: 'int', tooltip: 'The number of commoner wizards your house employs.', fieldClass: 'col'},
            staffCount: {name: 'staffCount', prettyName: 'Drow Staff', type: 'int', tooltip: 'The number of commoners your house employs to manage house affairs and serve your nobles.', fieldClass: 'col'},
            orcCount: {name: 'orcCount', prettyName: 'Orcs', type: 'int', tooltip: 'The number of Orcs your house owns.', fieldClass: 'col'},
            goblinCount: {name: 'goblinCount', prettyName: 'Goblins', type: 'int', tooltip: 'The number of Goblins your house owns.', fieldClass: 'col'},
            koboldCount: {name: 'koboldCount', prettyName: 'Kobolds', type: 'int', tooltip: 'The number of Goblins your house owns.', fieldClass: 'col'},
            duergarCount: {name: 'duergarCount', prettyName: 'Duergar', type: 'int', tooltip: 'The number of Goblins your house owns.', fieldClass: 'col'},
            dwarfCount: {name: 'dwarfCount', prettyName: 'Dwarves', type: 'int', tooltip: 'The number of Goblins your house owns.', fieldClass: 'col'},
            gnomeCount: {name: 'gnomeCount', prettyName: 'Gnomes', type: 'int', tooltip: 'The number of Goblins your house owns.', fieldClass: 'col'},
            minotaurCount: {name: 'minotaurCount', prettyName: 'Minotaurs', type: 'int', tooltip: 'The number of Minotaurs your house employs.', fieldClass: 'col'},
            beholderCount: {name: 'beholderCount', prettyName: 'Beholders', type: 'int', tooltip: 'The number of Beholders your house employs.', fieldClass: 'col'},
            illithidCount: {name: 'illithidCount', prettyName: 'Illithid', type: 'int', tooltip: 'The number of Illithid your house employs.', fieldClass: 'col'},
            kuoToaCount: {name: 'kuoToaCount', prettyName: 'Kuo-Toa', type: 'int', tooltip: 'The number of Kuo-Toa your house owns.', fieldClass: 'col'},
            bugbearCount: {name: 'bugbearCount', prettyName: 'Bugbears', type: 'int', tooltip: 'The number of Bugbears your house employs.', fieldClass: 'col'},
            lamiaCount: {name: 'lamiaCount', prettyName: 'Lamia', type: 'int', tooltip: 'The number of Lamia your house employs.', fieldClass: 'col'},
            nightmareCount: {name: 'nightmareCount', prettyName: 'Nightmares', type: 'int', tooltip: 'The number of Nightmares your house employs.', fieldClass: 'col'},
            zombieCount: {name: 'zombieCount', prettyName: 'Zombies', type: 'int', tooltip: 'The number of Zombies your house owns.', fieldClass: 'col'},
            skeletonCount: {name: 'skeletonCount', prettyName: 'Skeletons', type: 'int', tooltip: 'The number of Skeletons your house owns.', fieldClass: 'col'},
            driderCount: {name: 'driderCount', prettyName: 'Driders', type: 'int', tooltip: 'The number of Driders your house owns.', fieldClass: 'col'}
        };
    }

    renderField(fieldName) {
        let field = this.fields()[fieldName];
        let value = this.state.newHouse[fieldName];
        switch(field.type) {
            case 'readonly':
            case 'readOnly':
               return <ReadOnlyField key={field.key ? field.key : field.name} fieldName={field.name} type='text' value={value} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
            case 'text':
               return <TextInput key={field.key ? field.key : field.name} fieldName={field.name} type='text' value={value} onChange={this.onFieldChange} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
            case 'int':
               return <TextInput key={field.key ? field.key : field.name} fieldName={field.name} type='number' value={value} onChange={this.onFieldChange} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
            case 'enum':
               return <DropdownInput key={field.key ? field.key : field.name} fieldName={field.name} value={value} values={field.values} onChange={this.onFieldChange} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
        }
    }

    renderHouseInfo() {
        return (
            <div className='accordion open' id='houseAccordion'>
                <div className='accordion-item'>
                    <h2 className='accordion-header' id='houseInfoHeading'>
                        <button className='accordion-button' type='button' data-bs-toggle='collapse' data-bs-target='#houseInfo' aria-expanded='true' aria-controls='#houseInfo'>
                            House Information
                        </button>
                    </h2>
                    <div id='houseInfo' className='accordion-collapse collapse show' aria-labelledby='houseInfoHeading' data-bs-parent='#houseAccordion'>
                        <div className='row accordion-body'>
                            {this.renderField('name')}
                            {this.renderField('familyTrait')}
                            {this.renderField('gold')}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderContractBids() {
        return (
            <div className='accordion open' id='contractBidsAccordion'>
                <div className='accordion-item'>
                    <h2 className='accordion-header' id='contractBidsHeading'>
                        <button className='accordion-button' type='button' data-bs-toggle='collapse' data-bs-target='#contractBids' aria-expanded='true' aria-controls='#contractBids'>
                            Monopoly Contract Bids
                        </button>
                    </h2>
                    <div id='contractBids' className='accordion-collapse collapse show' aria-labelledby='contractBidsHeading' data-bs-parent='#contractBidsAccordion'>
                        <div className='row'>
                            {this.renderField('rotheFarmBid')}
                            {this.renderField('adamantiumMineBid')}
                            {this.renderField('fishmongeringBid')}
                        </div>
                        <div className='row'>
                            {this.renderField('lizardFarmingBid')}
                            {this.renderField('mossFarmingBid')}
                            {this.renderField('mushroomFarmingBid')}
                        </div>
                        <div className='row'>
                            {this.renderField('mortuaryBid')}
                            {this.renderField('garbageDumpBid')}
                            {this.renderField('arenaBid')}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderStaffing() {
        return (
            <div className='accordion open' id='staffingAccordion'>
                <div className='accordion-item'>
                    <h2 className='accordion-header' id='staffHeading'>
                        <button className='accordion-button' type='button' data-bs-toggle='collapse' data-bs-target='#staff' aria-expanded='true' aria-controls='#staff'>
                            Staffing
                        </button>
                    </h2>
                    <div id='staff' className='accordion-collapse collapse show' aria-labelledby='staffHeading' data-bs-parent='#staffingAccordion'>
                        <div className='row'>
                            {this.renderField('soldierCount')}
                            {this.renderField('priestessCount')}
                            {this.renderField('wizardCount')}
                            {this.renderField('staffCount')}
                        </div>
                        <div className='row'>
                            {this.renderField('orcCount')}
                            {this.renderField('goblinCount')}
                            {this.renderField('koboldCount')}
                            {this.renderField('duergarCount')}
                        </div>
                        <div className='row'>
                            {this.renderField('dwarfCount')}
                            {this.renderField('gnomeCount')}
                            {this.renderField('minotaurCount')}
                            {this.renderField('beholderCount')}
                        </div>
                        <div className='row'>
                            {this.renderField('illithidCount')}
                            {this.renderField('kuoToaCount')}
                            {this.renderField('bugbearCount')}
                            {this.renderField('lamiaCount')}
                        </div>
                        <div className='row'>
                            {this.renderField('nightmareCount')}
                            {this.renderField('zombieCount')}
                            {this.renderField('skeletonCount')}
                            {this.renderField('driderCount')}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderCompoundFeature(feature) {
        let compoundFeature = this.state.enums['CompoundFeatureType'].values[feature.compoundFeature];
        return (
            <div className='row' key={compoundFeature.name}>
                <div className='col-2'>
                    <button type='button' className='btn' onClick={() => {this.onAddCompoundFeature(compoundFeature.name)}}>+</button>
                    {feature.quantity}
                    <button type='button' className='btn' onClick={() => {this.onRemoveCompoundFeature(compoundFeature.name)}}>-</button>
                    {compoundFeature.prettyName}(s)
                </div>
                <div className='col-8'>
                    {compoundFeature.description}
                </div>
                <div className='col-2'>
                    {compoundFeature.creationCost} points each
                </div>
            </div>
        );
    }

    onAddCompoundFeature(feature) {
        let newState = {...this.state};
        let existingFeatures = [...newState.compoundFeatures];
        let thisFeature = existingFeatures.find(curFeature => {
            return curFeature.compoundFeature == feature;
        });
        if(!thisFeature) {
            thisFeature = {quantity: 1, compoundFeature: feature}
            existingFeatures.push(thisFeature);
        } else {
            thisFeature.quantity++
        }
        newState.compoundFeatures = existingFeatures;
        this.onSubmit(newState);
    }

    onRemoveCompoundFeature(feature) {
        let newState = {...this.state};
        let existingFeatures = [...newState.compoundFeatures];
        let thisFeature = existingFeatures.find(curFeature => {
            return curFeature.compoundFeature == feature;
        });
        if(!thisFeature) {
            //do nothing
        } else {
            thisFeature.quantity--;
        }
        if(thisFeature.quantity == 0) {
            existingFeatures = existingFeatures.filter(curFeature => {
                return curFeature.compoundFeature !== feature;
            });
        }
        newState.compoundFeatures = existingFeatures;
        this.onSubmit(newState);
    }

    renderCompoundFeatures() {
        return (
            <div className='accordion open' id='compoundFeaturesAccordion'>
                <div className='accordion-item'>
                    <h2 className='accordion-header' id='compoundFeaturesHeader'>
                        <button className='accordion-button' type='button' data-bs-toggle='collapse' data-bs-target='#compoundFeatures' aria-expanded='true' aria-controls='#compoundFeatures'>
                            Compound Features
                        </button>
                    </h2>
                    <div id='compoundFeatures' className='accordion-collapse collapse show' aria-labelledby='compoundFeaturesHeader' data-bs-parent='#compoundFeaturesAccordion'>
                        <div className='row'>
                            <button type='button' className='btn btn-primary col-6' data-bs-toggle='modal' data-bs-target='#compoundFeaturesModal'>Add Feature</button>
                        </div>
                        {this.state.compoundFeatures.map(feature => {
                            return this.renderCompoundFeature(feature);
                        })}
                    </div>
                </div>
                <div className='modal fade' tabIndex='-1' id='compoundFeaturesModal'>
                    <div className='modal-dialog modal-xl'>
                        <div className='modal-content'>
                            <div className='modal-header'>
                                <h5 className='modal-title'>Choose a compound feature to add.</h5>
                                <button type='button' className='btn-close' data-bs-dismiss='modal' aria-label='close'></button>
                            </div>
                            <div className='modal-body container'>
                                {Object.values(this.state.enums['CompoundFeatureType'].values).map(feature => {
                                    return (
                                        <div key={feature.name} className='row'>
                                            <button className='col-3 btn-primary' onClick={() => {this.onAddCompoundFeature(feature.name)}}>{feature.prettyName}</button>
                                            <span className='col-3'>{feature.creationCost} points</span>
                                            <span className='col-6'>{feature.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderNobleField(field, noble, index) {
        let value = noble[field.name];
        switch(field.type) {
            case 'readonly':
            case 'readOnly':
               return <ReadOnlyField key={field.key ? field.key : field.name + index} fieldName={field.name} type='text' value={value} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
            case 'text':
               return <TextInput key={field.key ? field.key : field.name + index} fieldName={field.name} type='text' value={value} onChange={(fieldName, value) => {this.onNobleFieldChange(fieldName, value, index)}} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
            case 'int':
            case 'number':
               return <TextInput key={field.key ? field.key : field.name + index} fieldName={field.name} type='number' value={value} onChange={(fieldName, value) => {this.onNobleFieldChange(fieldName, value, index)}} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
            case 'enum':
               return <DropdownInput key={field.key ? field.key : field.name + index} fieldName={field.name} value={value} values={field.values} onChange={(fieldName, value) => {this.onNobleFieldChange(fieldName, value, index)}} prettyName={field.prettyName} tooltip={field.tooltip} fieldClass={field.fieldClass} />;
        }
    }

    onNobleFieldChange(fieldName, value, index) {
        let newState = {...this.state};
        let nobles = [...newState.nobles];
        let ourNoble = {...nobles[index]};
        ourNoble[fieldName] = value;
        nobles[index] = ourNoble;
        newState.nobles = nobles;
        this.onSubmit(newState);
    }

    renderNoble(noble, index) {
        let fields = {
            name: {name: 'name', prettyName: 'Name', type: 'text', tooltip: 'The name of this noble.', fieldClass: 'col-4'},
            gender: {name: 'gender', prettyName: 'Gender', type: 'enum', values: Object.values(this.state.enums['Gender'].values), tooltip: 'The gender of this noble.', fieldClass: 'col-2'},
            archetype: {name: 'archetype', prettyName: 'Archetype', type: 'enum', values: Object.values(this.state.enums['Archetype'].values), tooltip: 'The archetype (or class, if you prefer) of this noble.', fieldClass: 'col-3'},
            relationshipToMatron: {name: 'relationshipToMatron', prettyName: 'Matron Relationship', type: 'enum', values: Object.values(this.state.enums['IndividualRelationType'].values), tooltip: 'How this noble is related to the matron.', fieldClass: 'col-3'},
            llothsFavor: {name: 'llothsFavor', prettyName: 'Lloth\'s Favor', type: 'number', tooltip: 'The modification you wish for the favor Lloth shows towards this noble.', fieldClass: 'col-1'},
            arcana: {name: 'arcana', prettyName: 'Arcana', type: 'number', tooltip: 'The modification you wish for this noble\'s Arcana skill.', fieldClass: 'col-1'},
            attack: {name: 'attack', prettyName: 'Attack', type: 'number', tooltip: 'The modification you wish for this noble\'s Attack skill.', fieldClass: 'col-1'},
            defense: {name: 'defense', prettyName: 'Defense', type: 'number', tooltip: 'The modification you wish for this noble\'s Defense skill.', fieldClass: 'col-1'},
            deception: {name: 'deception', prettyName: 'Deception', type: 'number', tooltip: 'The modification you wish for this noble\'s Deception skill.', fieldClass: 'col-1'},
            insight: {name: 'insight', prettyName: 'Insight', type: 'number', tooltip: 'The modification you wish for this noble\'s Insight skill.', fieldClass: 'col-1'},
            investigation: {name: 'investigation', prettyName: 'Investigation', type: 'number', tooltip: 'The modification you wish for this noble\'s Investigation skill.', fieldClass: 'col-1'},
            religion: {name: 'religion', prettyName: 'Religion', type: 'number', tooltip: 'The modification you wish for this noble\'s Religion skill.', fieldClass: 'col-1'},
            stealth: {name: 'stealth', prettyName: 'Stealth', type: 'number', tooltip: 'The modification you wish for this noble\'s Stealth skill.', fieldClass: 'col-1'},
            survival: {name: 'survival', prettyName: 'Survival', type: 'number', tooltip: 'The modification you wish for this noble\'s Survival skill.', fieldClass: 'col-1'},
        };
        return (
            <div key={'noble-' + index}>
                <div className='row'>
                    {this.renderNobleField(fields.name, noble, index)}
                    {this.renderNobleField(fields.gender, noble, index)}
                    {this.renderNobleField(fields.archetype, noble, index)}
                    {this.renderNobleField(fields.relationshipToMatron, noble, index)}
                </div>
                <div className='row'>
                    {this.renderNobleField(fields.llothsFavor, noble, index)}
                    {this.renderNobleField(fields.arcana, noble, index)}
                    {this.renderNobleField(fields.attack, noble, index)}
                    {this.renderNobleField(fields.defense, noble, index)}
                    {this.renderNobleField(fields.deception, noble, index)}
                    {this.renderNobleField(fields.insight, noble, index)}
                    {this.renderNobleField(fields.investigation, noble, index)}
                    {this.renderNobleField(fields.religion, noble, index)}
                    {this.renderNobleField(fields.stealth, noble, index)}
                    {this.renderNobleField(fields.survival, noble, index)}
                </div>
            </div>
        );
    }

    onAddNoble() {
        let newNoble = {
            name: '', gender: 'female', archetype: 'child', relationshipToMatron: 'none',
            llothsFavor: 0, arcana: 0, attack: 0, defense: 0, deception: 0, insight: 0, investigation: 0, religion: 0, stealth: 0, survival: 0
        };
        let newState = {...this.state};
        newState.nobles.push(newNoble);
        this.onSubmit(newState);
    }

    renderNobles() {
        return (
            <div className='accordion open' id='noblesAccordion'>
                <div className='accordion-item'>
                    <h2 className='accordion-header' id='noblesHeader'>
                        <button className='accordion-button' type='button' data-bs-toggle='collapse' data-bs-target='#nobles' aria-expanded='true' aria-controls='#nobles'>
                            Nobles
                        </button>
                    </h2>
                    <div id='nobles' className='accordion-collapse collapse show' aria-labelledby='noblesHeader' data-bs-parent='#noblesAccordion'>
                        <div className='row'>
                            <button type='button' className='btn btn-primary col-6' onClick={this.onAddNoble}>Add Noble</button>
                        </div>
                        {this.state.nobles.map((noble, index) => {
                            return this.renderNoble(noble, index);
                        })}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        if(!this.state.newHouse.name) {
            return (<div></div>);
        }
        return (
            <div>
                <FormStyles />
                Points remaining: {this.state.remainingPoints}
                {this.renderHouseInfo()}
                {this.renderContractBids()}
                {this.renderStaffing()}
                {this.renderCompoundFeatures()}
                {this.renderNobles()}
            </div>
        );
    }
}

export default CreateHouseView;