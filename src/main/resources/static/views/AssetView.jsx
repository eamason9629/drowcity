import React, { Component } from 'react';

class AssetView extends Component {
    constructor() {
        super();

        this.state = {
            lines: []
        };
    }

    componentDidMount() {
        const that = this;
        fetch('/asset/' + this.props.assetId).then(response => {
            response.json().then(json => {
                that.setState(json);
            });
        });
    }

    render() {
        return (
            <div className='row'>
                <div className='column'>
                    <div>House {this.state.ownerName}'s {this.state.type}(s)</div>
                    <div>Currently holding {this.state.currentCount} {this.state.type}(s)</div>
                </div>
                <div className='column'>
                    <span>Inventory History</span>
                    {this.state.lines.map(item => {
                        return (
                            <div key={item.id}>
                                Week {item.week}: {item.change} {item.type}
                            </div>
                        )
                    })}
                </div>
            </div>
        );
    }
}

export default AssetView;