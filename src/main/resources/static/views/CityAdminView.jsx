import React, { Component } from 'react';

class CityAdminView extends Component {
    constructor() {
        super();

        this.state = {
            cities: []
        };
    }

    componentDidMount() {
        const that = this;
        fetch('/city/list').then(response => {
            response.json().then(json => {
                that.setState({cities: json});
            });
        });
    }

    renderCity(city) {
        return (
            <tr key={city.id}>
                <th scope='col'>{city.name}</th>
                <td scope='col'>{city.playerCount}</td>
                <td scope='col'>{city.houseCount}</td>
                <td scope='col'>{city.compoundCount}</td>
            </tr>
        );
    }

    render() {
        if(this.state.cities.length === 0) {
            return (<div></div>);
        }
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th scope='col'>Name</th>
                            <th scope='col'># of Players</th>
                            <th scope='col'># of Houses</th>
                            <th scope='col'># of Compounds</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.cities.map(city => {
                            return this.renderCity(city);
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default CityAdminView;