import '../css/userDashboard.css';
import React, { Component } from 'react';
import SCREENS from '../constants/Screens';

class UserDashboardView extends Component {
    constructor(options) {
        super();

        this.state = {
            houses: []
        };
    }

    componentDidMount() {
        const that = this;
        fetch('/userDashboard').then(response => {
            response.json().then(json => {
                that.setState(json);
            });
        });
    }

    renderUser() {
        return (
            <div>
                <div>Hi, {this.state.firstName} ({this.state.username})!</div>
                <div>This is the email we have on file: {this.state.email}</div>
                <div>You last logged in: {this.state.lastLogin}</div>
            </div>
        );
    }

    renderHouse(house) {
        return (
            <tr key={house.id} onClick={function () {this.props.changeScreen(SCREENS.houseView.name, house.id)}.bind(this)}>
                <th scope='col'>{house.cityName}</th>
                <td scope='col'>{house.week}</td>
                <td scope='col'>{house.name}</td>
                <td scope='col'>{house.matronName}</td>
            </tr>
        );
    }

    render() {
        return (
            <div>
                {this.renderUser()}
                <div>These are the houses you are currently running...</div>
                <table className='table' border='1' frame='hsides' rules='rows'>
                    <thead>
                        <tr>
                            <th scope='col'>City</th>
                            <th scope='col'>Week</th>
                            <th scope='col'>House</th>
                            <th scope='col'>Matron</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.houses.map(house => {
                            return this.renderHouse(house)
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default UserDashboardView;