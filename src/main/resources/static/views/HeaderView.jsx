import '../css/header.css';
import React, { Component } from 'react';
import SCREENS from '../constants/Screens';
import loggedUserService from '../services/LoggedUserService';
import securityService from '../services/SecurityService';

class HeaderView extends Component {
    constructor() {
        super();

        this.state = {
            loggedUser: {},
            screens: []
        }

        this.renderLoggedIn = this.renderLoggedIn.bind(this);
        this.renderNotLoggedIn = this.renderNotLoggedIn.bind(this);
    }

    componentDidMount() {
        const that = this;
        loggedUserService.fetchLoggedUser().then((loggedUser) => {
            that.setState({loggedUser: loggedUser});
        });
        this.checkScreens();
    }

    checkScreens() {
        let that = this;
        Object.values(SCREENS).forEach(screen => {
            if(screen.navBar) {
                securityService.canUserViewScreen(screen).then(theyCan => {
                    if(theyCan) {
                        let result = [...this.state.screens];
                        if(!result.includes(screen)) {
                            result.push(screen);
                            that.setState({screens: result});
                        }
                    }
                });
            }
        });
    }

    renderCopyright() {
        return (
            <span className="copyright">
                Drow City Game, © 2022, Erik Mason
            </span>
        );
    }

    renderLoggedIn() {
        return (
            <header>
                <span className='welcome'>Welcome {this.state.loggedUser.firstName}!</span>
                <nav>
                    {this.state.screens.map(screen => {
                        return (<span key={screen.name}><a className='nav-link' onClick={function () {this.props.changeScreen(screen.name)}.bind(this)}>{screen.prettyName}</a> | </span>);
                    })}
                    <a className='nav-link' href='/logout'>Logout</a>
                    <span> | <a className='nav-link' onClick={function () {this.props.goBack()}.bind(this)}>&lt;&lt; Back</a></span>
                </nav>
                {this.renderCopyright()}
            </header>
        );
    }

    renderNotLoggedIn() {
        return (
            <header>
                <nav>
                    <a className='nav-link' onClick={function() {this.props.changeScreen(SCREENS.login.name)}.bind(this)}>{SCREENS.login.prettyName}</a> | <a className='nav-link' onClick={function() {this.props.changeScreen(SCREENS.createUser.name)}.bind(this)}>{SCREENS.createUser.prettyName}</a>
                </nav>
                {this.renderCopyright()}
            </header>
        );
    }

    render() {
        if(this.state.loggedUser && this.state.loggedUser.username) {
            if(this.state.screens.length === 0) {
                this.checkScreens();
                return (<nav></nav>);
            } else {
                return this.renderLoggedIn();
            }
        } else {
            return this.renderNotLoggedIn();
        }
    }
}

export default HeaderView;