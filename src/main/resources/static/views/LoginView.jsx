import '../css/login.css';
import React, { Component } from 'react';

class LoginView extends Component {
    render() {
        return (
            <form name='loginForm' action='login' method='POST'>
                <div><label>Username:</label><input name='username' className='username' type='text' /></div>
                <div><label>Password:</label><input name='password' className='password' type='password' /></div>
                <button name='submit' type='submit'>Login</button>
            </form>
        );
    }
}

export default LoginView;