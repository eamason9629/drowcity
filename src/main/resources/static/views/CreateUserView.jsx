import React, { Component } from 'react';

class CreatePlayerView extends Component {
    render() {
        return (
            <form name='createPlayer' action='createPlayer' method='POST'>
                <div><label>Username</label><input className='username' name='username' type='text' /></div>
                <div><label>Password</label><input className='password' name='password' type='password' /></div>
                <div><label>First Name</label><input className='firstName' name='firstName' type='text' /></div>
                <div><label>Last Name</label><input className='lastName' name='lastName' type='text' /></div>
                <button className='submit' type='submit'>Create Player</button>
            </form>
        );
    }
}

export default CreatePlayerView;