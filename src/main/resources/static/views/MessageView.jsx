import React, { Component } from 'react';

class AssetView extends Component {
    constructor() {
        super();

        this.state = {};
    }

    componentDidMount() {
        const that = this;
        fetch('/message/' + this.props.messageId).then(response => {
            response.json().then(json => {
                that.setState(json);
            });
        });
    }

    render() {
        return (
            <div>
                {this.state.sentAnonymously ? (
                    <h2>Message sent Anonymously</h2>
                ) : (
                    <h2>Message from {this.state.fromName}</h2>
                )}
                <div className='row'>To: {this.state.toName}</div>
                {this.state.sentPublicly ? (
                    <div className='row'>Sent via public messenger</div>
                ) : (
                    <div className='row'>Sent via private messenger</div>
                )}
                <div className='row'>Subject: {this.state.subject}</div>
                <p>
                    {this.state.message}
                </p>
            </div>
        );
    }
}

export default AssetView;