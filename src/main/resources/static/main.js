import React from 'react';
import ReactDOM from 'react-dom';
import MainController from './controllers/MainController';

ReactDOM.render(<MainController />, document.getElementById('contain'));

//this little gem prevents the back button from working. yay? lol
history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function (event) {
    history.pushState(null, document.title, location.href);
});