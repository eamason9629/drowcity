const SCREENS = {
    asset: {name: 'asset', prettyName: 'Asset', navBar: false, roles: ['generic']},
    cityAdmin: {name: 'cityAdmin', prettyName: 'City Administration', navBar: true, roles: ['generic']},
    createHouse: {name: 'createHouse', prettyName: 'Create House', navBar: true, roles: ['generic']},
    createUser: {name: 'createUser', prettyName: 'Sign Up', navBar: true, roles: ['notLoggedIn']},
    houseView: {name: 'houseView', prettyName: 'House', navBar: false, roles: ['generic']},
    login: {name: 'login', prettyName: 'Login', navBar: true, roles: ['notLoggedIn']},
    message: {name: 'message', prettyName: 'Message', navBar: false, roles: ['notLoggedIn']},
    userAdmin: {name: 'userAdmin', prettyName: 'User Administration', navBar: true, roles: ['userAdmin']},
    userDashboard: {name: 'userDashboard', prettyName: 'User Dashboard', navBar: true, roles: ['generic']},
    workerGroup: {name: 'workerGroup', prettyName: 'Worker Group', navBar: false, roles: ['generic']},
};

export default SCREENS;